param(
    # Use custom path
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [string]
    $SarRootFolder,
    # run service under different credentials
    [Parameter(Mandatory = $false)]
    [pscredential]
    $ServiceCredentials,
    # do not ask for confirmations
    [Parameter(Mandatory=$false)]
    [switch]
    $Force
)
$ErrorActionPreference = 'stop'
$InformationPreference = 'continue'

$serviceParams = @{
    Name = 'SAR'
    DisplayName = 'Script At Rest'
    Description = 'Hosts Script At Rest server'
    StartupType = 'Automatic'
}

if ($PSBoundParameters.ContainsKey('SarRootFolder')) {
    Write-Information "Custom root folder specified : $SarRootFolder" 
    $appPath = Join-Path -Path $SarRootFolder -ChildPath 'ScriptAtRestServer.exe'
} else {
    $appPath = Join-Path -Path . -ChildPath 'ScriptAtRestServer.exe'
}

$serviceParams.Add('BinaryPathName' , $appPath)

if (!(Test-Path -Path $appPath)) {
    throw 'Unable to find SAR application in current folder, please specify the path using "SarRootFolder" parameter'
}

if ($PSBoundParameters.ContainsKey('ServiceCredentials')) {
    Write-Information 'Service will log on under specified credentials'
    $serviceParams.Add('Credential' , $ServiceCredentials)
} else {
    Write-Information 'Service will run under "Local System" account'
}

$currentService = Get-Service -Name 'SAR' -ErrorAction SilentlyContinue
if($currentService)
{
    Write-Warning 'Service is already present and will be reinstalled'
    if (!$force.IsPresent) {
        $answer = Read-Host 'Do you want to continue y/n ?'
        if ($answer -ne 'y') {
            Write-Warning 'Aborting installation...'
            return
        }
    }
    Write-Information 'Removing old service'
    $currentService | Remove-Service
}

Write-Information 'Installing new service'
New-Service @serviceParams