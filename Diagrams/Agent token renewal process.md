# Agent token renewal process
```mermaid
sequenceDiagram
autonumber
    Agent->>Agent:Check if token renew neeed
    Agent ->> Server: I will suspend
    Server ->> Server: Suspend Agent
    Note right of Server: New jobs will not be scheduled for suspended agent
    Agent->>Agent: Wait until no running jobs
    Agent->>Server: I need new tokens
    Server->>Agent: New tokens
    Agent->>Agent: Disconnect from server
    Agent->>Server: Reconnect
    Server->>Server: Resume Agent
    Note right of Server: Upon connection Agent status is automatically updated
```