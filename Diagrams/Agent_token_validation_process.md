```mermaid
flowchart TD
    Loop-->IsAgentConnected[Agent connected?]
    IsAgentConnected-->|Yes| IsTimeToCheckTokenLifetime
    IsAgentConnected-->|No| IsTokenExpiredAgentNotConnected
    IsTokenExpiredAgentNotConnected --> |Yes| Terminate
    IsTokenExpiredAgentNotConnected --> |No| Reconnect
    Reconnect --> Loop
    IsTimeToCheckTokenLifetime -->|No| Loop
    IsTimeToCheckTokenLifetime -->|Yes| IsTokenRefreshNeeded?
    IsTokenRefreshNeeded? --> |Yes| Refresh
    IsTokenRefreshNeeded? --> |No| Loop
    Refresh --> Reconnect
```