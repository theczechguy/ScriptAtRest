```mermaid
sequenceDiagram
    autonumber
    participant User
    participant Server
    participant Agent
    User->>Server: Schedule script run
    Server->>Server: New job in queue
    Server->>Server: Identify suitable agent
    Server->>User: Script job scheduled
    Server->>Server: Find agent for job
    Server->>Agent: Execute new scriptjob
    Agent->>Agent: Run script
    Agent->>Server: Scripjob finished
    User->>Server: Get job status & wait for finish
```