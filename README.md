[![pipeline status](https://gitlab.com/theczechguy/ScriptAtRest/badges/master/pipeline.svg)](https://gitlab.com/theczechguy/ScriptAtRest/-/pipelines)
# ScriptAtRest
https://gitlab.com/theczechguy/ScriptAtRest/-/wikis/Script-At-Rest

WARNING
! Master branch can be at all times under construction !
If you want stable version of the code base please visit the release page.
https://gitlab.com/theczechguy/ScriptAtRest/-/releases
