# How to run SAR locally ?
- install .net core 6
- install PGSQL on your machine or use Docker (version 15 is tested)
  - in case of Docker you can use ```docker-compose.yml``` file which will bring up pgsql with open port *5432* and automatically performs DB init by using ```db_init.sql```.
- start SAR manually, by default it will automatically connect to this DB