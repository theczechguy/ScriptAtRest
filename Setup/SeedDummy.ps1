$InformationPreference = 'continue'
$ErrorActionPreference = 'stop'
$apiUrl = 'http://localhost:5000/api'


Write-Information 'Login'
    #get token for default admin user
$body = @{
    Username = "admin"
    Password = "ChangeMeNow1!"
}

$adminToken = Invoke-RestMethod -Uri "$apiUrl/authentication/login" `
                                    -Method Post `
                                    -Body ($body | ConvertTo-Json) `
                                    -ContentType 'application/json'
$adminToken = $adminToken.token | ConvertTo-SecureString -AsPlainText -Force

Write-Information 'Script type'
1..5 | % {

    $body = @{
        "Name" = "type $_"
        "Runner" = 'pwsh.exe'
        "FileExtension" = '.ps1'
        "ScriptArgument" = '-f'
    } | ConvertTo-Json
    
    Invoke-RestMethod `
        -Method Post `
        -Uri "$apiUrl/script/type" `
        -Body $body `
        -ContentType "application/json" `
        -Authentication Bearer `
        -Token $adminToken `
        -AllowUnencryptedAuthentication
}

Write-Information 'Script'
1..5 | % {
    $script = Get-Content .\TestScript.ps1 -Encoding utf8 -Raw
    $Bytes = [System.Text.Encoding]::UTF8.GetBytes($script)
    $encoded =[Convert]::ToBase64String($Bytes)
    $body = @{
        "Name" = "script $_"
        "EncodedContent" = $encoded
        "Type" = $_
        "Timeout" = 30
    } | ConvertTo-Json

    Invoke-RestMethod `
        -Method Post `
        -Uri "$apiUrl/script/register" `
        -Body $body `
        -ContentType "application/json" `
        -Authentication Bearer `
        -Token $adminToken `
        -AllowUnencryptedAuthentication
}


Write-Information 'Script history'
1..5 | % {
    $scriptName = "script $_"
    $body = @{
        'Timeout' = 35
    } | ConvertTo-Json


    $nameBody = @{
        name = $scriptName
    } | ConvertTo-Json
    $script = Invoke-RestMethod `
        -Method Get `
        -Uri "$apiUrl/script/byname" `
        -Authentication Bearer `
        -Token $adminToken `
        -AllowUnencryptedAuthentication `
        -Body $nameBody `
        -ContentType "application/json"

    Invoke-RestMethod `
        -Method Put `
        -Uri "$apiUrl/script/$($script.id)" `
        -Body $body `
        -ContentType "application/json" `
        -Authentication Bearer `
        -Token $adminToken `
        -AllowUnencryptedAuthentication
}


Write-Information 'Script job'
# get all scripts
$allScripts = Invoke-RestMethod `
    -Method Get `
    -Uri "$apiUrl/script" `
    -Authentication Bearer `
    -Token $adminToken `
    -AllowUnencryptedAuthentication
$allScripts = $allScripts | where name -like "script *"

$allScripts | % {
    $currentscript = $_
    1..3 | % {
        $param1 = "prvni"
        $Bytes = [System.Text.Encoding]::UTF8.GetBytes($param1)
        $param1Encoded =[Convert]::ToBase64String($Bytes)
    
        $param2 = "druhy"
        $Bytes = [System.Text.Encoding]::UTF8.GetBytes($param2)
        $param2Encoded =[Convert]::ToBase64String($Bytes)
    
        $body = @{
            "Parameters" = @(
                @{
                    "Name"= 'prvni'
                    'EncodedValue' = $param1Encoded
    
                },
                @{
                    "Name"= 'druhy'
                    'EncodedValue' = $param2Encoded
                }
            )
        } | ConvertTo-Json
    
        Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/scriptjob/$($currentscript.id)" `
            -ContentType "application/json" `
            -Body $body `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
    }
}