CREATE DATABASE "SAR-Server"
    WITH 
    OWNER = sar
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;