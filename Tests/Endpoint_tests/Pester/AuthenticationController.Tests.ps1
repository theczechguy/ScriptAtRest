param (
    $Username,
    $UserPassword,
    $apiUrl
)

BeforeAll {
    $InformationPreference = 'continue'
    #create test account
    $body = @{
        Username = $Username
        Password = $UserPassword
    }

    $null = Invoke-WebRequest -Uri "$apiUrl/api/users/register" `
                        -Method Post `
                        -Body ($body | ConvertTo-Json) `
                        -ContentType 'application/json'

    #get token for default admin user
    $body = @{
        Username = "admin"
        Password = "ChangeMeNow1!"
    }

    $adminToken = Invoke-RestMethod -Uri "$apiUrl/api/authentication/login" `
        -Method Post `
        -Body ($body | ConvertTo-Json) `
        -ContentType 'application/json'
    
    $shared = @{
        "AdminToken" = $adminToken.token
        "TestUserRefreshToken" = ''
    }
    
    $adminToken = $adminToken.token | ConvertTo-SecureString -AsPlainText -Force
}

Describe "User authentication tests" {
    It "Obtains access token" {
        $body = @{
            Username = $Username
            Password = $UserPassword
        }
        $response = Invoke-RestMethod -Uri "$apiUrl/api/authentication/login" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json'


        $response | Should -Not -BeNullOrEmpty
        $response.token | should -Not -BeNullOrEmpty
        $response.RefreshToken | should -Not -BeNullOrEmpty
        $response.RefreshTokenExpiration | should -Not -BeNullOrEmpty
        $response.tokenExpiration | should -Not -BeNullOrEmpty

        $shared.TestUserRefreshToken = $response.RefreshToken
        Start-Sleep -Seconds 1
    }

    It "Fail authenticating with wrong credentials"{
        $body = @{
            username = $Username
            password = 'wrong password'
        }
        $err
        try {
            $response = Invoke-RestMethod -Uri "$apiUrl/api/authentication/login" `
                                            -Method Post -Body ($body | ConvertTo-Json) `
                                            -ContentType 'application/json'
        }
        catch {
            $err = $_
        }

        ($err.ErrorDetails.Message | ConvertFrom-Json).Status | Should -BeExactly 'Failed'
        ($err.ErrorDetails.Message | ConvertFrom-Json).Message | Should -BeLike "Please verify user and password*"
    }

    It "Get new access token using existing refresh token"{
        $body = @{
            Token = $shared.TestUserRefreshToken
        }

        $response = Invoke-RestMethod -Uri "$apiUrl/api/authentication/refreshtoken" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json'
        $response | Should -Not -BeNullOrEmpty
        $response.token | should -Not -BeNullOrEmpty
        $response.RefreshToken | should -Not -BeNullOrEmpty
        $shared.TestUserRefreshToken = $response.RefreshToken
    }

    It "Revoke existing refresh token" {
        $body = @{
            Token = $shared.TestUserRefreshToken
        }
        $response = Invoke-RestMethod -Uri "$apiUrl/api/authentication/revoketoken" `
            -Method Post `
            -Body ($body | ConvertTo-Json) `
            -ContentType 'application/json'

        $response.Status | should -be 'Successful'
        $response.Message | should -be 'Token revoked'
    }
}

Describe "Password reset tests" {
    It "Reset user password" {
        $body = @{
            Password = $UserPassword
        }
        $response = Invoke-WebRequest -Uri "$apiUrl/api/authentication/$Username/passwordreset" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json' `
                                        -Authentication Bearer -Token $adminToken -AllowUnencryptedAuthentication

        $response = $response.content | ConvertFrom-Json
        $response | Should -Not -BeNullOrEmpty
        $response.Status | should -BeExactly 'Successful'
        $response.Message | should -BeExactly 'New password set'
    }

    It "Fail reseting password for nonexisting user" {
        $body = @{
            Password = $UserPassword
        }
        $err
        try {
            $response = Invoke-WebRequest -Uri "$apiUrl/api/authentication/idontexist/passwordreset" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json' `
                                        -Authentication Bearer -Token $adminToken -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }

        ($err.ErrorDetails.Message | ConvertFrom-Json).Message | Should -BeExactly 'User not found'
    }
}