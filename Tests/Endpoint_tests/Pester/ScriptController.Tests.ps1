param (
    $apiUrl
)
BeforeAll {
    $InformationPreference = 'continue'
    #region script type tests
        $scriptTypeName = 'Test Type_{0}' -f (Get-Random -Minimum 1000 -Maximum 10000)
        $scriptTypeRunner = "pwsh.exe"
        $scriptTypeFileExtension = '.ps1'
        $scriptTypeArgument = '-f'
    #endregion

    #region script tests
        $ScriptName = "Pester Test_{0}" -f (Get-Random -Minimum 1000 -Maximum 10000)
    #endregion

    #region shared data
        $global:newScriptTypeId = $Null
        $global:newScriptId = $Null
    #endregion

    #get token for default admin user
    $body = @{
        Username = "admin"
        Password = "ChangeMeNow1!"
    }

    $adminToken = Invoke-WebRequest -Uri "$apiUrl/authentication/login" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json'
    $adminToken = $adminToken.content | ConvertFrom-Json | select -ExpandProperty token
    $adminToken = $adminToken | ConvertTo-SecureString -AsPlainText -Force
}

Describe "Script type registration tests" {
    It "Register a new script type with name: <scriptTypeName>" {
        $body = @{
            "Name" = $scriptTypeName
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/type" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | should -BeExactly $scriptTypeName
        $response.id | should -not -BeNullOrEmpty
        $response.FileExtension | should -be $scriptTypeFileExtension
        $response.ScriptArgument | should -be $scriptTypeArgument
        $global:newScriptTypeId = $response.id
    }

    It "Register another script type with different name : <scriptTypeName> -test" {
        $body = @{
            "Name" = "$scriptTypeName-test"
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/type" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | should -BeExactly "$scriptTypeName-test"
        $response.id | should -not -BeNullOrEmpty
        $response.FileExtension | should -be $scriptTypeFileExtension
        $response.ScriptArgument | should -be $scriptTypeArgument
    }

    It "Register script type for deletion test: <scriptTypeName> -todelete" {
        $body = @{
            "Name" = "$scriptTypeName-todelete"
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/type" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | should -BeExactly "$scriptTypeName-todelete"
        $response.id | should -not -BeNullOrEmpty
        $response.FileExtension | should -be $scriptTypeFileExtension
        $response.ScriptArgument | should -be $scriptTypeArgument
    }
}

Describe "Script type update tests" {
    It "Update script type name: <scriptTypeName> to <scriptTypeName> -updated"{
        $body = @{
            "Name" = "$scriptTypeName-updated"
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/type/$newScriptTypeId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | Should -be "$scriptTypeName-updated"
        $response.id | should -Be $newScriptTypeId
        $response.FileExtension | should -be $scriptTypeFileExtension
        $response.ScriptArgument | should -be $scriptTypeArgument
    }

    It "Update script type runner: <scriptTypeRunner> to <scriptTypeRunner> -updated" {
        $body = @{
            "Runner" = "$scriptTypeRunner-updated"
        } | ConvertTo-Json
    
        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/type/$newScriptTypeId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
    
        $response.Runner | should -be "$scriptTypeRunner-updated"
    }
    It "Update script type runner & name to: <scriptTypeName> & <scriptTypeRunner>" {
        $body = @{
            "Name" = $scriptTypeName
            "Runner" = $scriptTypeRunner
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/type/$newScriptTypeId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | Should -be "$scriptTypeName"
        $response.runner | should -be $scriptTypeRunner
    }
    It "Fail update request which has no real differences"{
        $body = @{
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json
    
        $err
        try {
            $response = Invoke-WebRequest `
                -Method Put `
                -Uri "$apiUrl/script/type/$newScriptTypeId" `
                -Body $body `
                -ContentType "application/json" `
                -Authentication Bearer `
                -Token $adminToken `
                -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }
        $err | should -not -BeNullOrEmpty
        $err.ErrorDetails.Message.Trim('"') | should -be "No changes detected"
    }
    It "Fail updating name which already exists" {
        $body = @{
            "Name" = "$scriptTypeName-test"
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json
    
        $err
        try {
            $response = Invoke-RestMethod `
                -Method Put `
                -Uri "$apiUrl/script/type/$newScriptTypeId" `
                -Body $body `
                -ContentType "application/json" `
                -Authentication Bearer `
                -Token $adminToken `
                -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }
        $err | should -not -BeNullOrEmpty
        $err.ErrorDetails.Message.Trim('"') | should -be "New script type name is already taken !"
    }
}

Describe "Script type get tests" {
    It "Get all script types - should be at least 2"{
        $response = Invoke-RestMethod `
            -Method Get `
            -Uri "$apiUrl/script/type" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.count | Should -BeGreaterOrEqual 2
    }
    It "Get script type by ID: <newScriptTypeId>" {
        $response = Invoke-RestMethod `
            -Method Get `
            -Uri "$apiUrl/script/type/$newScriptTypeId" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.id | should -BeExactly $newScriptTypeId
        $response.name | should -be "$scriptTypeName"
    }
}

Describe "Script type delete tests" {
    It "Delete script type with id <newScriptTypeId>" {
        $err
        try {
            $response = Invoke-RestMethod `
                -Method Delete `
                -Uri "$apiUrl/script/type/$newScriptTypeId" `
                -Authentication Bearer `
                -Token $adminToken `
                -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }
        $err | Should -BeNullOrEmpty
    }
}

Describe "Script operation tests" {
    It "Register a new script type with name: <scriptTypeName>" {
        $body = @{
            "Name" = $scriptTypeName
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/type" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | should -BeExactly $scriptTypeName
        $response.id | should -not -BeNullOrEmpty
        $response.FileExtension | should -be $scriptTypeFileExtension
        $response.ScriptArgument | should -be $scriptTypeArgument
        $global:newScriptTypeId = $response.id
    }

    It "Register a new script with name: <ScriptName>" {
        $script = Get-Content .\TestScript.ps1 -Encoding utf8 -Raw

        $Bytes = [System.Text.Encoding]::UTF8.GetBytes($script)
        $encoded =[Convert]::ToBase64String($Bytes)

        $body = @{
            "Name" = $ScriptName
            "EncodedContent" = $encoded
            "Type" = $newScriptTypeId
            "Timeout" = 30
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/register" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.name | should -BeExactly $ScriptName
        $global:newScriptId = $response.id
    }

    It "Get the script by id: <newScriptId>" {
        $response = Invoke-RestMethod `
            -Method Get `
            -Uri "$apiUrl/script/$newScriptId" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        $response.id | should -BeExactly $newScriptId
        $response.name | should -BeExactly $ScriptName
    }
    
    It "Get the script by name '$ScriptName'" {
        $body = @{
            name = $ScriptName
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Get `
            -Uri "$apiUrl/script/byname" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication `
            -Body $body `
            -ContentType "application/json"

        $response.name | should -BeExactly $ScriptName
    }

    It "Get all scripts - should retrieve at least 1" {
        $response = Invoke-RestMethod `
        -Method Get `
        -Uri "$apiUrl/script" `
        -Authentication Bearer `
        -Token $adminToken `
        -AllowUnencryptedAuthentication
        
        $response.count | Should -BeGreaterOrEqual 1
    }

    It "Update script name to <ScriptName> -updated"{
        $body = @{
            "Name" = "$ScriptName-updated"
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/$newScriptId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
            $response.name | should -Be "$ScriptName-updated"
    }
    It "Update script type of script id : <newScriptId>"{
        $body = @{
            "Name" = 'Test_type_{0}' -f (Get-Random -Minimum 1000 -Maximum 10000)
            "Runner" = $scriptTypeRunner
            "FileExtension" = $scriptTypeFileExtension
            "ScriptArgument" = $scriptTypeArgument
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$apiUrl/script/type" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
        
        $testingScriptType = $response.id


        $body = @{
            "Type" = $testingScriptType
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/$newScriptId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication

            $response.scriptType.id | should -be $testingScriptType
    }
    It "Update script content"{
        $body = @{
            "encodedContent" = "d3JpdGUtaG9zdCAic3VwZXIgdGVzdGluZyI="
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/$newScriptId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
            $response.encodedContent | should -Be "d3JpdGUtaG9zdCAic3VwZXIgdGVzdGluZyI="
    }
    It "Update script timeout"{
        $body = @{
            "Timeout" = 10
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/$newScriptId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
            $response.timeout | should -be 10
    }
    It "Update name & timeout together"{
        $body = @{
            "Name" = $ScriptName
            "Timeout" = 30
        } | ConvertTo-Json

        $response = Invoke-RestMethod `
            -Method Put `
            -Uri "$apiUrl/script/$newScriptId" `
            -Body $body `
            -ContentType "application/json" `
            -Authentication Bearer `
            -Token $adminToken `
            -AllowUnencryptedAuthentication
        
            $response.name | should -be $ScriptName
            $response.timeout | should -be 30
    }

    # It "Delete the script by id <newScriptId>" {
    #     $err
    #     try {
    #         $response = Invoke-RestMethod `
    #             -Method Delete `
    #             -Uri "$apiUrl/script/$newScriptId" `
    #             -Authentication Bearer `
    #             -Token $adminToken `
    #             -AllowUnencryptedAuthentication
    #     }
    #     catch {
    #         $err = $_
    #     }
    #     $err | should -BeNullOrEmpty
    # }
}

AfterAll {
}