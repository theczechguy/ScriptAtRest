$InformationPreference = 'continue'
$body = @{
    Username = "admin"
    Password = "ChangeMeNow1!"
}
$apiUrl = "http://127.0.0.1:5000"
$adminToken = Invoke-WebRequest -Uri "$apiUrl/api/authentication/login" `
                                            -Method Post `
                                            -Body ($body | ConvertTo-Json) `
                                            -ContentType 'application/json'
$adminToken = $adminToken.content | ConvertFrom-Json | select -ExpandProperty token
$secureAdminToken = $adminToken | ConvertTo-SecureString -AsPlainText -Force


1..100 | ForEach-Object -ThrottleLimit 5 -Parallel {
    $response = Invoke-RestMethod `
        -Method Post `
        -Uri "$($using:apiUrl)/api/scriptjobs/1" `
        -Authentication Bearer `
        -Token $using:secureAdminToken -AllowUnencryptedAuthentication -ContentType 'application/json'
    
    $jobid = $response.jobid
    Write-Information $jobid
    Start-Sleep -Seconds 5
    do {
        Start-Sleep -Seconds 5
        $status = Invoke-RestMethod `
            -Method Get `
            -Uri "$($using:apiUrl)/api/scriptjobs/$jobid" `
            -Authentication Bearer `
            -Token $using:secureAdminToken `
            -AllowUnencryptedAuthentication `
            -ContentType 'application/json'
        $done = $status.jobstatus
        Write-Information ("JOB: {0} | Status: {1}" -f $jobid,$done)
    } until ($done -eq 'Succeeded')
}