param (
    $Username,
    $UserPassword,
    $apiUrl
)
BeforeAll {
    #get token for default admin user
    $body = @{
        Username = "admin"
        Password = "ChangeMeNow1!"
    }

    $adminToken = Invoke-WebRequest -Uri "$apiUrl/api/authentication/login" `
                                        -Method Post `
                                        -Body ($body | ConvertTo-Json) `
                                        -ContentType 'application/json'
    $adminToken = $adminToken.content | ConvertFrom-Json | select -ExpandProperty token
    $adminToken = $adminToken | ConvertTo-SecureString -AsPlainText -Force
}
Describe "New user registration tests" {
    It "Register new user" {
        $body = @{
            Username = $Username
            Password = $UserPassword
        }

        $response = Invoke-WebRequest -Uri "$apiUrl/api/users/register" `
                        -Method Post `
                        -Body ($body | ConvertTo-Json) `
                        -ContentType 'application/json'

        $response = $response.content | ConvertFrom-Json
        $response.Id | should -not -BeNullOrEmpty
        $response.Username | Should -be $Username
        $response.Approved | should -BeFalse
    }

    It "Fail registering the same user again" {
        $err = $Null
        $body = @{
            Username = 'trololl'
            Password = 'Start12389!'
        }
        try {
            $response = Invoke-WebRequest -Uri "$apiUrl/api/users/register" `
                -Method Post `
                -Body ($body | ConvertTo-Json) `
                -ContentType 'application/json'
        }
        catch {
            $_.ErrorDetails.Message.trim('"') | Should -BeExactly ('User name is already taken')
        }
    }
}

Describe "Get users tests" {
    It "Get all users" {

        $response = Invoke-RestMethod `
            -Method Get `
            -Uri "$apiUrl/api/users" `
            -Authentication Bearer `
            -Token $adminToken -AllowUnencryptedAuthentication
        
        $response.Count | Should -BeGreaterOrEqual 2
        $response.username | should -Contain $Username
    }

    It "Get specific user with name $Username" {
        $response = Invoke-RestMethod `
                    -Method Get `
                    -Uri "$apiUrl/api/users/$Username" `
                    -Authentication Bearer `
                    -Token $adminToken -AllowUnencryptedAuthentication
        
        $response | should -Not -BeNullOrEmpty
        $response.Username | should -BeExactly $Username
    }

    It "Fail getting non existing user" {
        $err = $Null
        try {
            $response = Invoke-RestMethod `
                -Method Get `
                -Uri "$apiUrl/api/users/nonexisting" `
                -Authentication Bearer `
                -Token $adminToken -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }
        $err.ErrorDetails.Message.trim('"') | Should -BeExactly ("User not found")
    }
}

Describe "User roles tests" {
    It "Approve user: $Username" {
        $response = Invoke-RestMethod `
                        -Method Post `
                        -Uri "$apiUrl/api/users/$Username/approve" `
                        -Authentication Bearer `
                        -Token $adminToken -AllowUnencryptedAuthentication
        
        $response.username | should -BeExactly $Username
    }
    It "Add admin role to: $Username" {
        $body = @{
            RoleName = "Admin"
        } | ConvertTo-Json
        $response = Invoke-RestMethod `
                        -Method Post `
                        -Uri "$apiUrl/api/users/$Username/addrole" `
                        -Authentication Bearer `
                        -Body $body `
                        -Token $adminToken -AllowUnencryptedAuthentication `
                        -ContentType 'application/json'
        $response.status | should -BeExactly 'Successful'
        $response.Message | should -BeExactly 'Role granted'
    }

    It "Remove admin role from: $Username" {
        $body = @{
            RoleName = "Admin"
        } | ConvertTo-Json
        $response = Invoke-RestMethod `
                        -Method Post `
                        -Uri "$apiUrl/api/users/$Username/removerole" `
                        -Authentication Bearer `
                        -Body $body `
                        -Token $adminToken -AllowUnencryptedAuthentication `
                        -ContentType 'application/json'
        $response.status | should -BeExactly 'Successful'
        $response.Message | should -BeExactly 'Role membership removed'
    }

    It "List roles of user: $Username" {
        $response = Invoke-RestMethod `
                        -Method Get `
                        -Uri "$apiUrl/api/users/$Username/roles" `
                        -Authentication Bearer `
                        -Token $adminToken -AllowUnencryptedAuthentication
        
        $response.username | should -BeExactly $Username
        $response.roles | should -HaveCount 1
        $response.roles | should -Contain "Approved"
    }
}

Describe "Delete user tests" {
    It "Delete user: $Username" {
        $response = Invoke-RestMethod `
                        -Method Delete `
                        -Uri "$apiUrl/api/users/$Username" `
                        -Authentication Bearer `
                        -Token $adminToken `
                        -AllowUnencryptedAuthentication
        
        $response.status | should -BeExactly 'Successful'
        $response.message | should -Be "User: $Username deleted"
    }

    It "Fail deleting non-existing user" {
        $err = $Null
        try {
            $response = Invoke-RestMethod `
                -Method Get `
                -Uri "$apiUrl/api/users/nonexistinguser" `
                -Authentication Bearer `
                -Token $adminToken -AllowUnencryptedAuthentication
        }
        catch {
            $err = $_
        }
        $err.ErrorDetails.Message.trim('"') | Should -BeExactly ("User not found")
    }
}