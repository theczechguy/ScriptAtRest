$InformationPreference = 'continue'

$apiUrl = "http://127.0.0.1:5050"

$adminToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiQWRtaW4iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMSIsImp0aSI6IjAwYmYyZWQ5LTc5MWItNDQ3Ni05Nzc0LWJiZWM5ZDQyNDdjMyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6WyJBZG1pbiIsIkFwcHJvdmVkIl0sIm5iZiI6MTY5NTI5ODIyMiwiZXhwIjoxNzI1Mjk4MjIyLCJpc3MiOiJodHRwOi8vMTI3LjAuMC4xOjUwMDAiLCJhdWQiOiJodHRwOi8vMTI3LjAuMC4xOjUwMDAifQ.fLT90tI_nt8feRV3IDUaB9MthE0XVb96l2pG_rNVDog'
$secureAdminToken = $adminToken | ConvertTo-SecureString -AsPlainText -Force
$startTime = Get-Date
$endTime = $startTime.AddMinutes(60)
$requestbody = @{
    'scriptParamModel' = @()
    'requiredCapabilityName' = ''
    'scriptId' = 1
} | ConvertTo-Json

while ((Get-Date) -le $endTime)
{
    1..10 | ForEach-Object -ThrottleLimit 3 -Parallel {
        $response = Invoke-RestMethod `
            -Method Post `
            -Uri "$($using:apiUrl)/api/scriptjob/" `
            -Authentication Bearer `
            -Token $using:secureAdminToken `
            -AllowUnencryptedAuthentication `
            -ContentType 'application/json' `
            -Body $using:requestbody

        $jobid = $response.id
        if (!$jobid)
        {
            continue
        }
        do {
            Start-Sleep -Seconds 2
            $status = Invoke-RestMethod `
                -Method Get `
                -Uri "$($using:apiUrl)/api/scriptjob/$jobid" `
                -Authentication Bearer `
                -Token $using:secureAdminToken `
                -AllowUnencryptedAuthentication `
                -ContentType 'application/json'
            $done = $status.jobstatus
            Write-Information ("JOB: {0} | Status: {1}" -f $jobid, $done)
        } until ($done -eq 'Completed')
    }

    # Add a 1-minute pause every 10 minutes
    if ((Get-Date) -ge $startTime.AddMinutes(10))
    {
        Start-Sleep -Seconds 60
        $startTime = Get-Date
    }
}