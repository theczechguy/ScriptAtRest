$pesterContainer = New-PesterContainer -Path .\Pester\ScriptController.Tests.ps1 -Data @{
    ApiUrl = 'http://localhost:5050/api'
}

Invoke-Pester -Container $pesterContainer -Output Detailed