$pesterContainer = New-PesterContainer -Path .\Pester\UserController.Tests.ps1 -Data @{
    Username = "Test{0}" -f (Get-Random -Minimum 100 -Maximum 10000)
    UserPassword = 'Test123!980'
    ApiUrl = 'http://localhost:5050'
}

Invoke-Pester -Container $pesterContainer -Output Detailed