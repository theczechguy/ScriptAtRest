# SAR endpoint tests
## Description
- Suite of tests designed to verify functionality of each endpoint
- This test does not simulate actuall user behavior, actions do not follow each other in any logical way

## How to run ?
- Tests are written using Powershell ```Pester``` framework
- Make sure you have latest ```Powershell``` and ```Pester``` version instaled
  - Do not run using the old Powershell v5 version !
- Tests are separated into logical endpoint groupings. You can execute either each group separately or execute full suite of tests at once.
- Tests expect to have a running SAR environment

### Individual tests
- Execute any of the scripts which are named ```StartxyzControllerTest.ps1```

### All tests
- Execute ```StartAllTests.ps1``` script