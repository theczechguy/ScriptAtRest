#region setup
    $InformationPreference =  'continue'
#endregion

#region magic
    Write-Information "Doing some magic"
    Start-Sleep -Seconds (Get-Random -Minimum 1 -Maximum 15)
    Write-Information "Did some magic"
#endregion