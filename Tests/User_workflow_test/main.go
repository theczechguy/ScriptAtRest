package main

import (
	"SAR_User_Workflow_Test/pkg/sar/agent"
	"SAR_User_Workflow_Test/pkg/sar/authentication"
	"SAR_User_Workflow_Test/pkg/sar/scriptjob"
	"SAR_User_Workflow_Test/pkg/sar/scripts"
	"SAR_User_Workflow_Test/pkg/sar/users"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"
)

const url string = "http://127.0.0.1:5050"
const authentication_user string = "admin"
const authentication_pass string = "ChangeMeNow1!"
const adminRoleName string = "Admin"
const secondUserPass string = "Start123!"
const secondUserNewPass string = "Start321!"

func lg(initiator, message string) {
	fmt.Printf("[%s] %s  -> ", initiator, message)
}

func logOk() {
	fmt.Println("OK")
}

func generateRandomUsername(prefix string) string {
	rand.Seed(time.Now().UnixNano())
	randomNumber := rand.Intn(1000)
	username := fmt.Sprintf("%s%d", prefix, randomNumber)
	return username
}

func main() {
	log.Println("SAR E2E test v1")

	var err error

	lg("USER1", "log in")
	authResponse, err := authentication.Login(url, authentication_user, authentication_pass)
	if err != nil {
		log.Fatalf("Authentication failed: %s", err)
	}
	var user1AuthToken string = authResponse.Token
	logOk()

	lg("USER1", "create new user")
	registerUserResponse, err := users.RegisterUser(url, user1AuthToken, generateRandomUsername("e2e"), secondUserPass)
	if err != nil {
		log.Fatalf("User registration failed: %s", err)
	}
	fmt.Println(registerUserResponse.ID)

	lg("USER1", "approve second user")
	_, err = users.ApproveUser(url, user1AuthToken, registerUserResponse.Username)
	if err != nil {
		log.Fatalf("User approval failed: %s", err)
	}
	logOk()

	lg("USER1", "grant admin role to second user")
	_, err = users.AssignRoleToUser(url, user1AuthToken, registerUserResponse.Username, adminRoleName)
	if err != nil {
		log.Fatalf("Role add failed: %s", err)
	}
	logOk()

	lg("USER1", "register new agent")
	agentName := generateRandomUsername("agente2e")
	agentRegisterResponse, err := agent.RegisterAgent(url, user1AuthToken, agentName)
	if err != nil {
		log.Fatalf("Agent registration failed: %s", err)
	}
	logOk()
	fmt.Println("Use following init token to configure agent")
	fmt.Println("================================")
	fmt.Println(agentRegisterResponse.Token)
	fmt.Println("================================")
	fmt.Println("Press enter when agent is connected")
	fmt.Scanln()
	// var agentId string = agentRegisterResponse.AgentID

	lg("USER2", "log in")
	authResponseUser2, err := authentication.Login(url, registerUserResponse.Username, secondUserPass)
	if err != nil {
		log.Fatalf("Authentication failed: %s", err)
	}
	var user2AuthToken string = authResponseUser2.Token
	logOk()

	lg("USER2", "set new password for second user")
	_, err = authentication.SetUserPassword(url, user2AuthToken, registerUserResponse.Username, secondUserNewPass)
	if err != nil {
		log.Fatalf("Password reset failed: %s", err)
	}
	logOk()

	lg("USER2", "log in with new password")
	authResponseUser2, err = authentication.Login(url, registerUserResponse.Username, secondUserNewPass)
	if err != nil {
		log.Fatalf("Authentication failed: %s", err)
	}
	user2AuthToken = authResponseUser2.Token
	logOk()

	lg("USER2", "register script type")
	var typeName string = generateRandomUsername("ste2e")
	var typeRunner string = "pwsh.exe"
	var typeFileExtension string = ".ps1"
	var typeScriptArgument string = "-f"

	scriptTypeRegisterResponse, err := scripts.RegisterScriptType(url, user2AuthToken, typeName, typeRunner, typeFileExtension, typeScriptArgument)
	if err != nil {
		log.Fatalf("Script type registration failed: %s", err)
	}
	fmt.Println(scriptTypeRegisterResponse.ID)

	lg("USER2", "register script")
	scriptName := generateRandomUsername("sce2e")
	typeId := strconv.Itoa(scriptTypeRegisterResponse.ID)
	encodedContent := "I3JlZ2lvbiBzZXR1cAogICAgJEluZm9ybWF0aW9uUHJlZmVyZW5jZSA9ICAnY29udGludWUnCiNlbmRyZWdpb24KCiNyZWdpb24gbWFnaWMKICAgIFdyaXRlLUluZm9ybWF0aW9uICJEb2luZyBzb21lIG1hZ2ljIgogICAgU3RhcnQtU2xlZXAgLVNlY29uZHMgKEdldC1SYW5kb20gLU1pbmltdW0gMSAtTWF4aW11bSA1KQogICAgV3JpdGUtSW5mb3JtYXRpb24gIkRpZCBzb21lIG1hZ2ljIgojZW5kcmVnaW9u"
	timeout := "10"
	scriptRegisterResponse, err := scripts.RegisterScript(url, user2AuthToken, scriptName, typeId, encodedContent, timeout)
	if err != nil {
		log.Fatalf("Script registration failed: %s", err)
	}
	fmt.Println(scriptRegisterResponse.ID)

	lg("USER2", "create script job")
	request := scriptjob.ScriptJobRequest{
		ScriptParamModel:       []scriptjob.ScriptParamModel{},
		RequiredCapabilityName: "",
		ScriptID:               scriptRegisterResponse.ID,
	}
	scriptJobCreateResponse, err := scriptjob.CreateJob(url, user2AuthToken, request)
	if err != nil {
		log.Fatalf("Scriptjob creation failed: %s", err)
	}
	fmt.Println(string(scriptJobCreateResponse.ID))

	lg("USER1", "first user gets state of script job")
	scriptJobGetResponse, err := scriptjob.GetJob(url, user1AuthToken, scriptJobCreateResponse.ID)
	if err != nil {
		log.Fatalf("Scriptjob get failed: %s", err)
	}
	fmt.Println(scriptJobGetResponse.JobStatus)
}
