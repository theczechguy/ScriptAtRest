package agent

import (
	"SAR_User_Workflow_Test/pkg/sar/utils"
	"encoding/json"
)

type AgentRegisterResponse struct {
	AgentName string `json:"agentName"`
	AgentID   string `json:"agentId"`
	Token     string `json:"token"`
}

func RegisterAgent(url, token, name string) (*AgentRegisterResponse, error) {

	endpoint := "api/agent/register"
	requestBody := map[string]string{"agentName": name}

	response, err := utils.PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result AgentRegisterResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
