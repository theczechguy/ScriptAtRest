package scriptjob

import (
	"SAR_User_Workflow_Test/pkg/sar/utils"
	"encoding/json"
	"fmt"
	"time"
)

type ScriptJobRequest struct {
	ScriptParamModel       []ScriptParamModel `json:"scriptParamModel"`
	RequiredCapabilityName string             `json:"requiredCapabilityName"`
	ScriptID               int                `json:"scriptId"`
}
type ScriptParamModel struct {
	Name         string `json:"name"`
	EncodedValue string `json:"encodedValue"`
}

type ScriptJob struct {
	ID                       string    `json:"id"`
	CreatedOn                time.Time `json:"createdOn"`
	JobStatus                string    `json:"jobStatus"`
	EncodedJobStandardOutput any       `json:"encodedJobStandardOutput"`
	EncodedJobErrorOutput    any       `json:"encodedJobErrorOutput"`
	JobExitCode              int       `json:"jobExitCode"`
	FailureDetails           any       `json:"failureDetails"`
	AssignedAgentID          string    `json:"assignedAgentId"`
	ScriptID                 int       `json:"scriptId"`
}

func CreateJob(url, token string, JobDetails ScriptJobRequest) (*ScriptJob, error) {

	endpoint := "api/scriptjob"
	response, err := utils.PerformHTTPPost(url, endpoint, token, JobDetails)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptJob
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func GetJob(url, token, jobId string) (*ScriptJob, error) {

	endpoint := fmt.Sprintf("api/scriptjob/%s", jobId)

	response, err := utils.PerformHTTPGet(url, endpoint, token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptJob
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
