package scripts

import (
	"SAR_User_Workflow_Test/pkg/sar/utils"
	"encoding/json"
	"time"
)

type ScriptRegisterResponse struct {
	ID              int             `json:"id"`
	Name            string          `json:"name"`
	EncodedContent  string          `json:"encodedContent"`
	ScriptType      ScriptType      `json:"scriptType"`
	Timeout         int             `json:"timeout"`
	LastModifiedUtc time.Time       `json:"lastModifiedUtc"`
	ScriptHistory   []ScriptHistory `json:"scriptHistory"`
}
type ScriptType struct {
	ID             int    `json:"id"`
	Name           string `json:"name"`
	Runner         string `json:"runner"`
	FileExtension  string `json:"fileExtension"`
	ScriptArgument string `json:"scriptArgument"`
}
type ScriptHistory struct {
	ID             int        `json:"id"`
	ScriptEntityID int        `json:"scriptEntityId"`
	Name           string     `json:"name"`
	EncodedContent string     `json:"encodedContent"`
	ScriptType     ScriptType `json:"scriptType"`
	Timeout        int        `json:"timeout"`
	ModifiedUtc    time.Time  `json:"modifiedUtc"`
}

func RegisterScriptType(url, token, name, runner, fileExtension, scriptArgument string) (*ScriptType, error) {

	endpoint := "api/script/type"
	requestBody := map[string]string{
		"name":           name,
		"runner":         runner,
		"fileExtension":  fileExtension,
		"scriptArgument": scriptArgument,
	}

	response, err := utils.PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptType
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func RegisterScript(url, token, name, typeId, encodedContent, timeoutSeconds string) (*ScriptRegisterResponse, error) {

	endpoint := "api/script/register"
	requestBody := map[string]string{
		"name":           name,
		"type":           typeId,
		"encodedContent": encodedContent,
		"timeout":        timeoutSeconds,
	}

	response, err := utils.PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptRegisterResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
