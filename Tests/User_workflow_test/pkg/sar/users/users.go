package users

import (
	"SAR_User_Workflow_Test/pkg/sar/utils"
	"encoding/json"
	"fmt"
)

type RegisteredUserResponse struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Approved bool   `json:"approved"`
}

type GenericResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func RegisterUser(url, token, username, password string) (*RegisteredUserResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	if password == "" {
		return nil, fmt.Errorf("Password cannot be empty")
	}

	endpoint := "api/users/register"
	requestBody := map[string]string{
		"username": username,
		"password": password,
	}

	response, err := utils.PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func ApproveUser(url, token, username string) (*RegisteredUserResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s/approve", username)
	response, err := utils.PerformHTTPPost(url, endpoint, token, nil)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func AssignRoleToUser(url, token, username, rolename string) (*GenericResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	if rolename == "" {
		return nil, fmt.Errorf("Rolename cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s/addrole", username)
	requestBody := map[string]string{
		"RoleName": rolename,
	}

	response, err := utils.PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
