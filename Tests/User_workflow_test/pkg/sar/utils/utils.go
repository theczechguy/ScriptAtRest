package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func PerformHTTPPost(url, endpoint, token string, requestBody interface{}) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	requestBodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}
	// fmt.Println(string(requestBodyBytes))

	request, err := http.NewRequest("POST", fullEndpoint, bytes.NewBuffer(requestBodyBytes))
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		body, readErr := ioutil.ReadAll(response.Body)
		if readErr != nil {
			return response, fmt.Errorf("Failed to read response body: %v", readErr)
		}

		errMessage := fmt.Sprintf("Request failed with status code %d | Response Body: %s", response.StatusCode, string(body))
		return response, fmt.Errorf(errMessage)
	}

	return response, nil
}

func PerformHTTPGet(url, endpoint, token string) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	request, err := http.NewRequest("GET", fullEndpoint, nil)
	if err != nil {
		return nil, err
	}

	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}
	request.Header.Set("Accept", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		body, readErr := ioutil.ReadAll(response.Body)
		if readErr != nil {
			return response, fmt.Errorf("Failed to read response body: %v", readErr)
		}

		errMessage := fmt.Sprintf("Request failed with status code %d | Response Body: %s", response.StatusCode, string(body))
		return response, fmt.Errorf(errMessage)
	}

	return response, nil
}
