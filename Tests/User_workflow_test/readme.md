# User workflow test
## Description
- This test simulates standard end to end workflow that is usually performed by users

## Scenario
- log in with default account
- create new user
- approve new user
- grant admin role to new user
- register new agent
- new user log in
- new user change password
- new user logs in with new password
- new user register script type
- new user register script
- new user create script job
- default account get state of script job

## How to run ?
- test is written in Go
  - requires at least Go 1.19
  - to generate executable just run ```go build``` command
- it is expected you have running instance of SAR before executing this test
  - you can spin up test environment using the ```docker-compose``` file present in ***Docker*** folder