﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using ScriptAtRest.RemoteAgent.Database;

namespace RemoteAgent.Credentials
{
    /// <summary>
    /// Factory for creating and managing agent credentials.
    /// </summary>
    public interface IAgentCredentialsFactory
    {
        /// <summary>
        /// Protects the access and refresh tokens.
        /// </summary>
        /// <returns>The instance of the factory.</returns>
        public IAgentCredentialsFactory ProtectTokens();

        /// <summary>
        /// Unprotects and returns the access and refresh tokens.
        /// </summary>
        /// <returns>The un-protected tokens.</returns>
        public HubTokens UnProtectTokens();

        /// <summary>
        /// Stores the protected access and refresh tokens in the database.
        /// </summary>
        /// <returns>The instance of the factory.</returns>
        public Task<IAgentCredentialsFactory> StoreProtectedTokensAsync();

        /// <summary>
        /// Loads the protected access and refresh tokens from the database.
        /// </summary>
        /// <returns>The instance of the factory.</returns>
        public Task<IAgentCredentialsFactory> LoadProtectedTokensAsync();

        /// <summary>
        /// Sets new access and refresh tokens.
        /// </summary>
        /// <param name="AccessToken">The new access token.</param>
        /// <param name="RefreshToken">The new refresh token.</param>
        /// <returns>The instance of the factory.</returns>
        public IAgentCredentialsFactory SetNewTokens(string AccessToken, string RefreshToken);

        /// <summary>
        /// Gets the server URI as entered by user
        /// </summary>
        /// <returns>The server URI.</returns>
        public Task<string> GetServerUri();

        /// <summary>
        /// Gets the server URI for establishing connection.
        /// Use this to establish SignalR connectivity
        /// </summary>
        /// <returns>The server AGENTHUB URI.</returns>
        public Task<string> GetServerAgentHubUri();
    }

    public class AgentCredentialsFactory : IAgentCredentialsFactory
    {
        private readonly string _protectorName = "HubTokenProtection";
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private string AccessToken { get; set; }
        private string RefreshToken { get; set; }
        private string ProtectedAccessToken { get; set; }
        private string ProtectedRefreshToken { get; set; }

        public AgentCredentialsFactory(IDataProtectionProvider dataProtectionProvider, IServiceScopeFactory serviceScopeFactory)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public IAgentCredentialsFactory SetNewTokens(string AccessToken, string RefreshToken)
        {
            this.AccessToken = AccessToken;
            this.RefreshToken = RefreshToken;
            return this;
        }

        public IAgentCredentialsFactory ProtectTokens()
        {
            if (this.AccessToken is null || this.RefreshToken is null)
            {
                throw new Exception("Token or RefreshToken are empty");
            }

            var protector = this.CreateProtector();
            this.ProtectedAccessToken = protector.Protect(this.AccessToken);
            this.ProtectedRefreshToken = protector.Protect(this.RefreshToken);

            return this;
        }

        public async Task<IAgentCredentialsFactory> StoreProtectedTokensAsync()
        {
            if (this.ProtectedAccessToken is null || this.ProtectedRefreshToken is null)
            {
                throw new Exception("Protected token or protected refreshtoken are missing, run ProtectTokens() method first.");
            }
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();

                var existingAgentConfig = await dataContext.AgentConnectionConfig.FirstOrDefaultAsync();
                existingAgentConfig.ProtectedAccessToken = this.ProtectedAccessToken;
                existingAgentConfig.ProtectedRefreshToken = this.ProtectedRefreshToken;
                existingAgentConfig.ModifiedUtc = DateTime.UtcNow;
                await dataContext.SaveChangesAsync();
            }
            return this;
        }

        public async Task<IAgentCredentialsFactory> LoadProtectedTokensAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();

                var existingAgentConfig = await dataContext.AgentConnectionConfig.FirstOrDefaultAsync();
                this.ProtectedAccessToken = existingAgentConfig.ProtectedAccessToken;
                this.ProtectedRefreshToken = existingAgentConfig.ProtectedRefreshToken;
            }
            return this;
        }

        public HubTokens UnProtectTokens()
        {
            var protector = this.CreateProtector();
            if (!string.IsNullOrEmpty(this.ProtectedAccessToken))
            {
                this.AccessToken = protector.Unprotect(this.ProtectedAccessToken);
            }

            if (!string.IsNullOrEmpty(this.ProtectedRefreshToken))
            {
                this.RefreshToken = protector.Unprotect(this.ProtectedRefreshToken);
            }
            return new HubTokens
            {
                AccessToken = this.AccessToken,
                RefreshToken = this.RefreshToken
            };
        }

        private IDataProtector CreateProtector()
        {
            return _dataProtectionProvider.CreateProtector(this._protectorName);
        }

        public async Task<string> GetServerUri()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                var existingAgentConfig = await dataContext.AgentConnectionConfig.FirstOrDefaultAsync();
                return existingAgentConfig.ServerUri;
            }
        }

        public async Task<string> GetServerAgentHubUri()
        {
            var serverUri = await this.GetServerUri();
            return string.Format("{0}/agenthub", serverUri.TrimEnd('/'));
        }
    }
}