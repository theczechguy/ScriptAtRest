﻿namespace RemoteAgent.Credentials
{
    public class HubTokens
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}