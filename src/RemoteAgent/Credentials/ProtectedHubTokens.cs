﻿namespace RemoteAgent.Credentials
{
    public class ProtectedHubTokens
    {
        public string ProtectedAccessToken { get; set; }
        public string ProtectedRefreshToken { get; set; }
    }
}