﻿namespace ScriptAtRest.RemoteAgent.Database
{
    public class AgentConnectionConfigEntity
    {
        public Guid Id { get; set; }
        public DateTime ModifiedUtc { get; set; }
        public string? ProtectedAccessToken { get; set; }
        public string ProtectedRefreshToken { get; set; }
        public string ServerUri { get; set; }
    }
}