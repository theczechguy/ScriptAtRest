﻿using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ScriptAtRest.RemoteAgent.Database
{
    public class DataContext : DbContext, IDataProtectionKeyContext
    {
        public DataContext()
        { }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite($"Data Source={Path.GetDirectoryName(Environment.ProcessPath)}\\datastore.db");
        }

        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
        public DbSet<AgentConnectionConfigEntity> AgentConnectionConfig { get; set; }
    }
}