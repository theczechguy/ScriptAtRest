﻿using Microsoft.AspNetCore.DataProtection;
using ScriptAtRest.RemoteAgent.Database;

namespace RemoteAgent.Helpers
{
    public class AgentConfigurator
    {
        public static async void Configure(string RefreshToken, string ServerUri, DataContext dataContext, IDataProtectionProvider dataProtectionProvider)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" - Config argument detected, Agent is entering Configuration mode\n\n");

            if (string.IsNullOrWhiteSpace(RefreshToken))
            {
                throw new ArgumentException($"'{nameof(RefreshToken)}' cannot be null or empty.", nameof(RefreshToken));
            }

            if (string.IsNullOrWhiteSpace(ServerUri))
            {
                throw new ArgumentException($"'{nameof(ServerUri)}' cannot be null or empty.", nameof(ServerUri));
            }

            Console.Write($"Please verify token: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(RefreshToken);
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.Write("Please verify server uri: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(ServerUri);
            Console.ForegroundColor = ConsoleColor.Yellow;

            var allTokens = dataContext.AgentConnectionConfig.ToList();
            dataContext.AgentConnectionConfig.RemoveRange(allTokens);
            await dataContext.SaveChangesAsync();

            var protector = dataProtectionProvider.CreateProtector("HubTokenProtection");

            dataContext.AgentConnectionConfig.Add(new AgentConnectionConfigEntity
            {
                ModifiedUtc = DateTime.UtcNow,
                ProtectedRefreshToken = protector.Protect(RefreshToken),
                ServerUri = ServerUri
            });
            await dataContext.SaveChangesAsync();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Agent configured, you can restart agent now without -config option");
        }
    }
}