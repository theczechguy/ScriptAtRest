﻿namespace ScriptAtRest.RemoteAgent.Helpers
{
    public enum TokenRefreshState
    {
        NotNeeded,
        SoonExpiring,
        Expired,
        Missing,
    }
}