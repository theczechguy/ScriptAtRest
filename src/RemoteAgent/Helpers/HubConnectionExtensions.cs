﻿using Microsoft.AspNetCore.SignalR.Client;
using Quartz;
using RemoteAgent.ScriptExecution;
using ScriptAtRest.Shared.Scripts;

namespace RemoteAgent.Helpers
{
    public static class HubConnectionExtensions
    {
        public static void OnTerminateConnection(this HubConnection hubConnection, ILogger logger)
        {
            hubConnection.On("TerminateConnection", async () =>
            {
                logger.LogWarning("Received command from server to terminate active connection");
                await hubConnection.StopAsync();
            });
        }

        public static void OnRunScript(this HubConnection hubConnection,
                                       ILogger logger,
                                       IScheduler JobScheduler)
        {
            if (hubConnection == null)
            {
                throw new ArgumentNullException(nameof(hubConnection));
            }

            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            hubConnection.On<ScriptRequestData>("RunScript", async scriptRequestData =>
            {
                logger.LogInformation("Received new scriptjob to execute : {jobid}", scriptRequestData.ServerScriptJobId);
                IJobDetail job = JobBuilder.Create<ScriptJob>()
                .UsingJobData(new JobDataMap
                {
                    {"ScriptRequestData", scriptRequestData }
                })
                .Build();
                logger.LogDebug("New job: {@job}", job);

                ITrigger trigger = TriggerBuilder.Create()
                    .StartNow().Build();
                var scheduledJob = await JobScheduler.ScheduleJob(job, trigger);
                logger.LogInformation("Job scheduled to run: {job}", scheduledJob);

                await hubConnection.InvokeAsync(SignalRServerMethods.ScriptRunAck.ToString(), scriptRequestData.ServerScriptJobId.ToString());
            });
        }
    }
}