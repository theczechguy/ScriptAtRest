﻿using Quartz;
using RemoteAgent.Credentials;
using RemoteAgent.ScriptExecution;
using RemoteAgent.ServerConnectivity;

namespace RemoteAgent.Helpers
{
    public static class ServiceExtensions
    {
        public static void ConfigureAppServices(this IServiceCollection services)
        {
            services.AddHostedService<Worker>();
            services.AddSingleton<IAgentCredentialsFactory, AgentCredentialsFactory>();
            services.AddSingleton<ISignalRClientService, SignalRClientService>();
            services.AddScoped<IJob, ScriptJob>();
        }
    }
}