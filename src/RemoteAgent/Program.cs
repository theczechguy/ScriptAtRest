using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using Quartz;
using RemoteAgent.Helpers;
using ScriptAtRest.RemoteAgent.Database;
using Serilog;
using System.CommandLine;

try
{
    var pathToContentRoot = Path.GetDirectoryName(Environment.ProcessPath);
    Directory.SetCurrentDirectory(pathToContentRoot);

    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Script At Rest Remote Agent");
    Console.ForegroundColor = ConsoleColor.White;

    var configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json")
        .Build();

    var appExecutionMethod = configuration.GetValue<string>("General:AppExecutionMethod");

    var hostBuilder = Host.CreateDefaultBuilder(args)
        .ConfigureServices(services =>
        {
            services.AddDbContext<DataContext>();
            services.AddDataProtection()
                .SetApplicationName("SarAgent")
                .PersistKeysToDbContext<DataContext>();
            services.ConfigureAppServices();
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();
            });
            services.AddQuartzHostedService(opt =>
            {
                opt.WaitForJobsToComplete = true;
            });
        })
        .UseSerilog((hostingContext, services, loggerConfiguration) => loggerConfiguration
            .ReadFrom.Configuration(hostingContext.Configuration)
            .Enrich.WithProperty("AppPath", Environment.ProcessPath));

    switch (appExecutionMethod)
    {
        case "standalone":
            Console.WriteLine("Starting as standalone");
            break;

        case "winservice":
            Console.WriteLine("Starting as winservice");
            hostBuilder.UseWindowsService();
            break;

        case "systemd":
            Console.WriteLine("Starting as systemd service");
            hostBuilder.UseSystemd();
            break;

        default:
            throw new Exception(string.Format("Appexecutionmethod configuration item contains unknown value : {0}", appExecutionMethod));
    }

    var host = hostBuilder.Build();

    // create db if doesn't exist and apply migrations
    using (var scope = host.Services.CreateScope())
    {
        var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();
        dbContext.Database.Migrate();
    }

    // if argument config is used
    if (args.Length > 0)
    {
        var refreshTokenOption = new Option<string>(
            "-token",
            "Specify token generated during agent registration process");

        var serverUriOption = new Option<string>(
            "-serveruri",
            "Specify server uri");

        var rootCommand = new RootCommand("ScriptAtRest Agent");

        var configCommand = new Command("config", "Configure ScriptAtRest agent")
        {
            refreshTokenOption,
            serverUriOption
        };
        rootCommand.AddCommand(configCommand);

        using (var scope = host.Services.CreateScope())
        {
            var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
            var dataProtectionProvider = scope.ServiceProvider.GetRequiredService<IDataProtectionProvider>();

            configCommand.SetHandler((refreshToken, serverUri) =>
            {
                AgentConfigurator.Configure(refreshToken, serverUri, dataContext, dataProtectionProvider);
            },
                refreshTokenOption, serverUriOption
            );
            var result = rootCommand.Invoke(args);
        }
        Environment.Exit(0); //after agent is configured do not continue with execution
    }
    // start the app
    host.Run();
}
catch (Exception ex)
{
    Console.WriteLine(ex.ToString());
    Environment.Exit(1);
}