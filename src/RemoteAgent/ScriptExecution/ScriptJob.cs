﻿using Microsoft.AspNetCore.SignalR.Client;
using Quartz;
using RemoteAgent.ServerConnectivity;
using ScriptAtRest.Shared.Scripts;
using System.Diagnostics;
using System.Text;

namespace RemoteAgent.ScriptExecution
{
    public class ScriptJob : IJob
    {
        private readonly ILogger<ScriptJob> _logger;
        private readonly ISignalRClientService _signalRClientService;
        private readonly IConfiguration _configuration;

        public ScriptJob(ILogger<ScriptJob> logger, ISignalRClientService signalRClientService, IConfiguration configuration)
        {
            _logger = logger;
            _signalRClientService = signalRClientService;
            _configuration = configuration;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            string scriptFilePath = null;
            ScriptRequestData scriptRequestData = null;
            try
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                scriptRequestData = context.JobDetail.JobDataMap.Get("ScriptRequestData") as ScriptRequestData;
                _logger.LogInformation("Executing scriptjob : {job}", scriptRequestData.ServerScriptJobId);
                _logger.LogDebug("Scriptjob data: {@data}", scriptRequestData);

                //inform server about script job status change
                await _signalRClientService.Connection.InvokeAsync(SignalRServerMethods.ScriptRunUpdateState.ToString(), scriptRequestData.ServerScriptJobId, ScriptJobStates.Executing);
                var decodedScriptContent = Base64Helper.DecodeBase64(scriptRequestData.ScriptEncodedContent);
                scriptFilePath = this._createScriptFileWithContent(decodedScriptContent, scriptRequestData.ScriptFileExtension);
                string processArgs = this._prepareScriptArguments(scriptRequestData.ScriptArguments, scriptFilePath, scriptRequestData.ScriptParamModel);

                ProcessModel processModel = await this._runProcessAsync(processArgs, scriptRequestData.ScriptExecutable, scriptRequestData.ScriptTimeoutSeconds);
                stopwatch.Stop();
                _logger.LogDebug("Finished in : {time} seconds", stopwatch.Elapsed.TotalSeconds);
                _logger.LogDebug("Process result: {@result}", processModel);
                ScriptRunResultData scriptResultData = new ScriptRunResultData
                {
                    EncodedErrorOutput = Base64Helper.EncodeBase64(processModel.ErrorOutput),
                    EncodedStandardOutput = Base64Helper.EncodeBase64(processModel.Output),
                    FinishedOn = DateTime.UtcNow,
                    ScriptExitCode = processModel.ExitCode,
                    ServerScriptJobId = scriptRequestData.ServerScriptJobId,
                    ExecutionTimeSeconds = stopwatch.Elapsed.TotalSeconds,
                    Status = ScriptJobStates.Completed
                };

                _logger.LogInformation("Sending results to server for job: {id}", scriptRequestData.ServerScriptJobId);
                await _signalRClientService.Connection.InvokeAsync(SignalRServerMethods.ScriptRunFinished.ToString(), scriptResultData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fatal error during script execution");
                await _signalRClientService.Connection.InvokeAsync(SignalRServerMethods.ScriptRunFatalFailure.ToString(), scriptRequestData.ServerScriptJobId, ex.Message);
            }
            finally
            {
                this._deleteScriptFileAsync(scriptFilePath);
            }
        }

        private void _deleteScriptFileAsync(string FilePath)
        {
            bool deleteScript = _configuration.GetValue<bool>("ScriptExecution:DeleteScriptFilesAfterExecution");
            if (deleteScript)
            {
                _logger.LogDebug("Deleting file : {file}", FilePath);
                File.Delete(FilePath);
            }
            else
            {
                _logger.LogWarning("File will not be deleted as requested by configuration option 'DeleteScriptFilesAfterExecution'");
            }
        }

        private async Task<ProcessModel> _runProcessAsync(string processArgs, string scriptExecutable, int scriptTimeoutSeconds)
        {
            return await Task.Run(() =>
            {
                _logger.LogDebug("Process runner : {runner}", scriptExecutable);

                int timeoutMiliseconds;
                if (scriptTimeoutSeconds < 1)
                {
                    timeoutMiliseconds = 120000; //2 minutes
                }
                else
                {
                    timeoutMiliseconds = scriptTimeoutSeconds * 1000;
                }
                _logger.LogDebug("Process timeout (ms): {timeout}", timeoutMiliseconds);

                // it's important to process standard and error outputs asynchronously otherwise we would not be able to use timeout for the process
                // there is also risk of outputs and processs blocking each other & risk of output buffer exhaustion
                // https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.process.standardoutput?view=net-5.0
                // https://stackoverflow.com/questions/139593/processstartinfo-hanging-on-waitforexit-why

                using (Process process = new Process())
                {
                    process.StartInfo.FileName = scriptExecutable;
                    process.StartInfo.Arguments = processArgs;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;

                    StringBuilder output = new StringBuilder();
                    StringBuilder error = new StringBuilder();

                    using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                    using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                    {
                        process.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                outputWaitHandle.Set();
                            }
                            else
                            {
                                output.AppendLine(e.Data);
                            }
                        };
                        process.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                errorWaitHandle.Set();
                            }
                            else
                            {
                                error.AppendLine(e.Data);
                            }
                        };

                        process.Start();

                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();

                        if (
                            process.WaitForExit(timeoutMiliseconds)
                            && outputWaitHandle.WaitOne(timeoutMiliseconds)
                            && errorWaitHandle.WaitOne(timeoutMiliseconds)
                            )
                        {
                            return new ProcessModel
                            {
                                ExitCode = process.ExitCode,
                                Output = output.ToString(),
                                ErrorOutput = error.ToString()
                            };
                        }
                        else
                        {
                            _logger.LogWarning("Process timed out and will be forcefully terminated");
                            try
                            {
                                process.Kill();
                            }
                            catch (InvalidOperationException ex)
                            {
                                _logger.LogError(ex, "Failed to kill process as it probably is already stopped");
                            }
                            catch (System.Exception ex)
                            {
                                _logger.LogError(ex, "Failed to kill process");
                            }
                            return new ProcessModel
                            {
                                ExitCode = -1,
                                Output = output.ToString(),
                                ErrorOutput = error.ToString()
                            };
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Create temporary file with a specified content and extension
        /// </summary>
        /// <param name="ScriptContent"></param>
        /// <param name="ScriptExtension"></param>
        /// <returns></returns>
        private string _createScriptFileWithContent(string ScriptContent, string ScriptExtension)
        {
            string tempFilePath = Path.GetTempFileName();
            _logger.LogDebug("New temporary file : {file}", tempFilePath);

            tempFilePath = Path.ChangeExtension(tempFilePath, ScriptExtension);
            _logger.LogDebug("Temporary file extension set to : {extension}", ScriptExtension);

            File.WriteAllText(tempFilePath, ScriptContent);
            _logger.LogDebug("Script content stored in temp file");

            return tempFilePath;
        }

        private string _prepareScriptArguments(string ScriptArgument, string ScriptFilePath, List<ScriptParamModel> ScriptParamModels)
        {
            _logger.LogDebug("Preparing script arguments");
            StringBuilder stringBuilder = new StringBuilder();

            if (ScriptArgument != null)
            {
                stringBuilder.AppendFormat(" {0} {1}", ScriptArgument, ScriptFilePath);
            }
            else
            {
                stringBuilder.AppendFormat(" {0}", ScriptFilePath);
            }

            if (ScriptParamModels == null)
            {
                return stringBuilder.ToString();
            }

            if (ScriptParamModels.Count > 0)
            {
                foreach (ScriptParamModel paramModel in ScriptParamModels)
                {
                    string decodedValue = Base64Helper.DecodeBase64(paramModel.EncodedValue);
                    stringBuilder.Append(string.Format(" -{0} {1}", paramModel.Name, decodedValue));
                }
            }
            _logger.LogDebug("Final argument string: {str}", stringBuilder.ToString());
            return stringBuilder.ToString();
        }
    }
}