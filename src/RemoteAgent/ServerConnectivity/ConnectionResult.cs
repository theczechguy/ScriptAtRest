﻿namespace ScriptAtRest.RemoteAgent.ServerConnectivity
{
    public class ConnectionResult<T>
    {
        public bool IsSuccess { get; private set; }
        public bool IsFailure { get; private set; }
        public T Value { get; private set; }

        public static ConnectionResult<T> Success(T Value)
        {
            return new ConnectionResult<T> { IsSuccess = true, IsFailure = false, Value = Value };
        }

        public static ConnectionResult<T> Failure()
        {
            return new ConnectionResult<T> { IsSuccess = false, IsFailure = true };
        }
    }
}