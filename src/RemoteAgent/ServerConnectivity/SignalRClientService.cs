using Microsoft.AspNetCore.SignalR.Client;
using Polly;
using RemoteAgent.Credentials;
using RestSharp;
using ScriptAtRest.RemoteAgent.Helpers;
using ScriptAtRest.RemoteAgent.ServerConnectivity;
using ScriptAtRest.Shared.Scripts;
using Serilog;
using System.IdentityModel.Tokens.Jwt;
using System.Text.Json;

namespace RemoteAgent.ServerConnectivity
{
    public interface ISignalRClientService
    {
        /// <summary>
        /// Connects to the SignalR hub asynchronously. This method has to be called after LoadTokens and
        /// BuildConnection methods
        /// Connection is available via Connection property of the object.
        /// </summary>
        /// <returns>An instance of ISignalRClientService after a successful connection.</returns>
        Task<ConnectionResult<bool>> ConnectAsync();

        /// <summary>
        /// Loads tokens from the filesystem.
        /// </summary>
        /// <returns>An instance of ISignalRClientService.</returns>
        Task<ISignalRClientService> LoadTokensAsync();

        /// <summary>
        /// Builds the connection to the SignalR hub using the provided server URI and access token.
        /// </summary>
        /// <returns>An instance of ISignalRClientService.</returns>
        Task<ISignalRClientService> BuildConnectionAsync();

        Task<ISignalRClientService> ReconnectAsync();

        Task InvokeHubMethodAsync(SignalRServerMethods Method, params string[] args);

        /// <summary>
        /// Gets the HubConnection instance.
        /// </summary>
        HubConnection Connection { get; }

        Task<bool> RenewTokens();

        /// <summary>
        /// Determines if a refresh of the token is required by checking the expiration date
        /// and the percentage of the token's lifetime that has passed.
        /// </summary>
        /// <returns>
        /// A <see cref="TokenRefreshState"/> indicating whether the token needs to be refreshed.
        /// </returns>
        TokenRefreshState GetAccessTokenState();

        TokenRefreshState GetRefreshTokenState();

        Task<bool> ResetTokens();
    }

    public class SignalRClientService : ISignalRClientService
    {
        public HubConnection Connection { get; private set; }
        private readonly ILogger<SignalRClientService> _logger;
        private readonly IAgentCredentialsFactory _hubTokenFactory;

        //private string _serverUri = "http://localhost:5000/agenthub";
        private HubTokens _hubTokens;

        private const string _refreshTokenEndpoint = "/api/agent/refreshtoken";

        public SignalRClientService(ILogger<SignalRClientService> logger, IAgentCredentialsFactory hubTokenFactory)
        {
            _logger = logger;
            _hubTokenFactory = hubTokenFactory;
        }

        public async Task InvokeHubMethodAsync(SignalRServerMethods Method, params string[] args)
        {
            _logger.LogDebug("Invoking method: {method}", Method.ToString());
            await Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(
                    retryCount: 3,
                    sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    onRetry: (exception, _, retryAttempt, _) =>
                    {
                        _logger.LogWarning(exception, "Error invoking SignalR hub method {method}. Retrying ({retryAttempt}/{retryCount})...", Method, retryAttempt, 3);
                    })
                .ExecuteAsync(async () => await Connection.InvokeAsync(Method.ToString(), args));
        }

        public async Task<ISignalRClientService> ReconnectAsync()
        {
            _logger.LogInformation("Stopping hub connection");
            await this.Connection.StopAsync();
            await Task.Delay(5000);
            _logger.LogInformation("Starting hub connection");
            await this.ConnectAsync();
            return this;
        }

        private TokenRefreshState GetTokenRefreshState(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var decodedToken = handler.ReadJwtToken(token);
            var expiry = decodedToken.Payload.Exp;
            DateTime expiryDateTime = DateTimeOffset.FromUnixTimeSeconds((long)expiry).DateTime;
            var percentLifetimePassed = Math.Round((DateTime.UtcNow - decodedToken.ValidFrom).TotalSeconds / (expiryDateTime - decodedToken.ValidFrom).TotalSeconds * 100);
            _logger.LogDebug("Token lifetime passed : {life} %", percentLifetimePassed);
            if (expiryDateTime < DateTime.UtcNow)
            {
                _logger.LogWarning("Token has expired");
                return TokenRefreshState.Expired;
            }
            if (percentLifetimePassed > 80)
            {
                _logger.LogInformation("Token has less than 20% of it's lifetime");
                return TokenRefreshState.SoonExpiring;
            }
            return TokenRefreshState.NotNeeded;
        }

        public TokenRefreshState GetRefreshTokenState()
        {
            _logger.LogDebug("Checking refresh token state");
            if (_hubTokens.RefreshToken is null)
            {
                return TokenRefreshState.Missing;
            }
            return GetTokenRefreshState(_hubTokens.RefreshToken);
        }

        public TokenRefreshState GetAccessTokenState()
        {
            _logger.LogDebug("Checking access token state");
            if (_hubTokens.AccessToken is null)
            {
                return TokenRefreshState.Missing;
            }
            return GetTokenRefreshState(_hubTokens.AccessToken);
        }

        /// <summary>
        /// Checks if token renewal is needed, and renews the tokens if necessary.
        /// </summary>
        /// <returns>
        /// Returns true if tokens were renewed, false otherwise.
        /// </returns>
        /// <remarks>
        /// This method checks whether the access token needs to be renewed, and if so,
        /// requests a new set of tokens from the server. The new tokens are then encrypted,
        /// stored, and returned by the method. If the access token does not need to be renewed,
        /// the method returns false without making any changes.
        /// </remarks>
        public async Task<bool> RenewTokens()
        {
            _logger.LogDebug("Request new set of tokens from server");
            var newTokens = await Connection.InvokeAsync<NewTokensModel>(SignalRServerMethods.RenewTokens.ToString(), _hubTokens.RefreshToken);
            if (newTokens is null)
            {
                _logger.LogWarning("Server did not provide a set of new tokens, agent will stay suspended !");
                return false;
            }
            _logger.LogDebug("Retrieved a new set of tokens from server");
            await _hubTokenFactory
                .SetNewTokens(newTokens.Token, newTokens.RefreshToken)
                .ProtectTokens()
                .StoreProtectedTokensAsync();
            this.LoadTokensAsync();
            return true;
        }

        public async Task<ISignalRClientService> BuildConnectionAsync()
        {
            var serverUri = string.Format("{0}/agenthub", await _hubTokenFactory.GetServerUri());
            if (this._hubTokens is null)
            {
                await this.LoadTokensAsync();
            }
            _logger.LogDebug("Building connection to server: {serverurl}", serverUri);
            this.Connection = new HubConnectionBuilder()
                .WithUrl(serverUri, options =>
                {
                    options.AccessTokenProvider = () => Task.FromResult(_hubTokens.AccessToken);
                })
                .ConfigureLogging(loggin =>
                {
                    loggin.AddSerilog();
                })
                .WithAutomaticReconnect()
                .Build();

            this.Connection.Reconnecting += async (error) =>
            {
                if (error != null)
                {
                    _logger.LogWarning("Agent reconnecting: {e}", error.Message);
                }
                else
                {
                    _logger.LogWarning("Agent reconnecting");
                }
            };

            this.Connection.Closed += async (error) =>
            {
                if (error != null)
                {
                    _logger.LogWarning("Agent connection closed: {e}", error.Message);
                }
                else
                {
                    _logger.LogWarning("Agent connection closed");
                }
            };

            this.Connection.Reconnected += async (connectionId) =>
            {
                _logger.LogInformation("Agent reconnected with connection ID {id}.", connectionId);
            };

            return this;
        }

        public async Task<ConnectionResult<bool>> ConnectAsync()
        {
            _logger.LogInformation("Establishing Hub connection");
            if (!(this.Connection.State != HubConnectionState.Connected
                  || this.Connection.State != HubConnectionState.Connecting
                  || this.Connection.State != HubConnectionState.Reconnecting))
            {
                _logger.LogInformation("Connection is in state {state}. New connection will not be started", this.Connection.State);
                return ConnectionResult<bool>.Success(false);
            }
            try
            {
                await this.Connection.StartAsync();
                _logger.LogInformation("Established connection with hub under id: {id}", this.Connection.ConnectionId);
                await Task.Delay(2000); // give hub enough time to evaluate and potentially terminate the connection
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Failed to start connection with server: {m}", ex.Message);
                return ConnectionResult<bool>.Failure();
            }
            return ConnectionResult<bool>.Success(true);
        }

        public async Task<ISignalRClientService> LoadTokensAsync()
        {
            _logger.LogDebug("Loading tokens from protected storage");
            this._hubTokens = this._hubTokenFactory.LoadProtectedTokensAsync()
                .Result
                .UnProtectTokens();
            return this;
        }

        public async Task<bool> ResetTokens()
        {
            _logger.LogWarning("Reseting tokens via refresh token");
            var refreshTokenRequest = new RefreshTokenRequest
            {
                Token = _hubTokens.RefreshToken
            };
            RestClient client = new RestClient(await _hubTokenFactory.GetServerUri());
            RestRequest request = new RestRequest(_refreshTokenEndpoint, Method.Post).AddBody(refreshTokenRequest, ContentType.Json);
            var response = await client.ExecuteAsync(request);
            _logger.LogInformation("Refresh token response : {response}", response.StatusCode);

            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("Failed to renew tokens using refresh token - {message}", response.Content);
                return false;
            }
            var serializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var newTokens = JsonSerializer.Deserialize<NewTokensModel>(response.Content, serializerOptions);
            await _hubTokenFactory
                .SetNewTokens(newTokens.Token, newTokens.RefreshToken)
                .ProtectTokens()
                .StoreProtectedTokensAsync();
            await this.LoadTokensAsync();
            return true;
        }
    }
}