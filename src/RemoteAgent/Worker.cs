using Microsoft.AspNetCore.SignalR.Client;
using Quartz;
using RemoteAgent.Helpers;
using RemoteAgent.ServerConnectivity;
using ScriptAtRest.RemoteAgent.Helpers;
using ScriptAtRest.Shared.Scripts;
using System.CommandLine.Parsing;

namespace RemoteAgent
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly ISignalRClientService _signalRClientService;
        private HubConnection _hubConnection;
        private DateTime _lastTokenCheck;
        private readonly IConfiguration _configuration;
        private int _connectionRetryAttempt = 0;
        private TimeSpan _connectionDelayTimeSpan = TimeSpan.FromSeconds(5);

        public Worker(ILogger<Worker> logger, ISchedulerFactory schedulerFactory, ISignalRClientService signalRClientService, IConfiguration configuration)
        {
            _logger = logger;
            _schedulerFactory = schedulerFactory;
            _signalRClientService = signalRClientService;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(3000, stoppingToken); // wait for the rest of agent to initialize properly
            // start quartz scheduler
            IScheduler scheduler = await this.StartScheduler(stoppingToken);

            // load tokens and verify whether it's valid
            await _signalRClientService.LoadTokensAsync();

            // build connection
            await _signalRClientService.BuildConnectionAsync();

            this._hubConnection = _signalRClientService.Connection;
            _hubConnection.OnRunScript(_logger, scheduler);
            _hubConnection.OnTerminateConnection(_logger);

            while (!stoppingToken.IsCancellationRequested)
            {
                if (_hubConnection.State == HubConnectionState.Reconnecting)
                {
                    _logger.LogWarning("Agent is reconnecting");
                }
                else if (this._isAgentConnected())
                {
                    _logger.LogDebug("[Workerloop] Agent is connected");
                    if (this._isTimeToCheckTokens())
                    {
                        _logger.LogDebug("[Workerloop] Time to check token lifetime");
                        if (_signalRClientService.GetAccessTokenState() != TokenRefreshState.NotNeeded)
                        {
                            _logger.LogDebug("[Workerloop] Time to refresh token");
                            await this._renewTokens(scheduler);
                        }
                        this._lastTokenCheck = DateTime.UtcNow;
                    }
                }
                else
                { // agent not connected
                    _logger.LogDebug("[Workerloop] Agent is not connected");
                    //token not yet expired keep reconnecting
                    await this._reconnectAgentIfNeeded();
                }
                await Task.Delay(2000, stoppingToken); // Add a delay to avoid tight loop
            }
        }

        private async Task<bool> _ensureTokensAreValid()
        {
            var accessTokenState = _signalRClientService.GetAccessTokenState();
            if (accessTokenState == TokenRefreshState.Expired
                || accessTokenState == TokenRefreshState.Missing)
            {
                _logger.LogDebug("Acces token is expired or missing");
                var refreshTokenState = _signalRClientService.GetRefreshTokenState();
                if (refreshTokenState == TokenRefreshState.Expired
                    || refreshTokenState == TokenRefreshState.Missing)
                {
                    _logger.LogError("Both access and refresh tokens expired or missing");
                    throw new Exception("No available tokens");
                }
                // access token is missing or expired and refresh token is present and valid
                return await _signalRClientService.ResetTokens();
            }
            else
            {
                _logger.LogDebug("Access token is present and valid");
                return true;
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogWarning("Service is shutting down");
            var hubConnection = _signalRClientService.Connection;

            // Close and dispose the hub connection
            if (hubConnection != null)
            {
                await hubConnection.StopAsync(cancellationToken);
                await hubConnection.DisposeAsync();
            }

            // Call the base class StopAsync method
            await base.StopAsync(cancellationToken);
        }

        /// <summary>
        /// Starts the job scheduler and returns an <see cref="IScheduler"/> instance.
        /// </summary>
        /// <param name="stoppingToken">The <see cref="CancellationToken"/> to monitor for cancellation requests.</param>
        /// <returns>
        /// A <see cref="Task"/> that represents the asynchronous operation. The task result contains
        /// the <see cref="IScheduler"/> instance of the started job scheduler.
        /// </returns>
        private async Task<IScheduler> StartScheduler(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Obtaining job scheduler");
            IScheduler scheduler = await this._schedulerFactory.GetScheduler(stoppingToken);
            if (!scheduler.IsStarted)
            {
                _logger.LogInformation("Starting job scheduler");
                await scheduler.Start(stoppingToken);
            }
            _logger.LogInformation("Job scheduler running: {running}", scheduler.IsStarted);
            return scheduler;
        }

        /// <summary>
        /// Checks whether the agent is disconnected from the HubConnection.
        /// </summary>
        /// <returns>
        /// True if the agent is disconnected from the HubConnection, otherwise false.
        /// </returns>
        private bool _isAgentConnected()
        {
            if (_hubConnection.State is HubConnectionState.Connected
                or HubConnectionState.Reconnecting
                or HubConnectionState.Connecting)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if it is time to refresh the authentication tokens by comparing the time
        /// elapsed since the last token check to the renewal interval specified in the configuration.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> indicating whether it is time to refresh the authentication tokens.
        /// </returns>
        private bool _isTimeToCheckTokens()
        {
            var renewalIntervalMinutes = double.Parse(_configuration["Authentication:RenewalIntervalMinutes"]);
            if ((DateTime.UtcNow - _lastTokenCheck) >= TimeSpan.FromMinutes(renewalIntervalMinutes))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reconnects the agent to the SignalR hub if the connection is not in a connected,
        /// reconnecting or connecting state.
        /// </summary>
        /// <returns>
        /// A <see cref="Task"/> representing the asynchronous operation.
        /// </returns>
        private async Task _reconnectAgentIfNeeded()
        {
            // reconnect if not reconnecting,connecting or connected
            if (_hubConnection.State != HubConnectionState.Connected && _hubConnection.State != HubConnectionState.Reconnecting
                && _hubConnection.State != HubConnectionState.Connecting)
            {
                var tokensValid = await _ensureTokensAreValid();
                if (tokensValid)
                {
                    _logger.LogWarning("Agent is not connected, retrying...");
                    var result = await _signalRClientService.ConnectAsync();
                    // exponential backoff
                    if (result.IsFailure)
                    {
                        _connectionRetryAttempt++;
                        _connectionDelayTimeSpan = TimeSpan.FromSeconds(Math.Pow(2, _connectionRetryAttempt));
                        // Limit the backoff to 120 seconds
                        if (_connectionDelayTimeSpan.TotalSeconds > 120)
                        {
                            _connectionDelayTimeSpan = TimeSpan.FromSeconds(120);
                        }
                        _logger.LogInformation("Delay: {delay}", _connectionDelayTimeSpan);
                        await Task.Delay(_connectionDelayTimeSpan);
                    }
                    if (result.IsSuccess)
                    {
                        _connectionRetryAttempt = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Renews tokens and restarts the server connection if necessary.
        /// </summary>
        /// <param name="scheduler">The scheduler used to wait for running jobs to finish.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        private async Task _renewTokens(IScheduler scheduler)
        {
            _logger.LogInformation("Requesting agent suspension");
            await _hubConnection.InvokeAsync(SignalRServerMethods.SuspendAgent.ToString(), "Token refresh");
            await this._waitForRunningJobsToFinish(scheduler);

            var renewed = await _signalRClientService.RenewTokens();
            if (renewed)
            {
                _logger.LogInformation("Restarting server connection after token refresh");
                await _signalRClientService.ReconnectAsync();
            }
        }

        /// <summary>
        /// Waits for currently running jobs to finish by repeatedly checking the scheduler
        /// until there are no currently executing jobs.
        /// </summary>
        /// <param name="scheduler">The <see cref="IScheduler"/> instance to check for running jobs.</param>
        /// <returns>
        /// A <see cref="Task"/> representing the asynchronous operation.
        /// </returns>
        private async Task _waitForRunningJobsToFinish(IScheduler scheduler)
        {
            while (true)
            {
                var currentJobs = await scheduler.GetCurrentlyExecutingJobs();
                if (currentJobs.Count == 0)
                {
                    _logger.LogDebug("No jobs running");
                    break; // no jobs currently executing, exit the loop
                }
                else
                {
                    _logger.LogInformation("Waiting for {jobcount} currently executing job(s) to complete", currentJobs.Count);
                    await Task.Delay(TimeSpan.FromSeconds(10)); // wait for 30 seconds before trying again
                }
            }
        }
    }
}