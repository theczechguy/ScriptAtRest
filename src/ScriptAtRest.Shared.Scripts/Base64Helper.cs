﻿using System.Text;

namespace ScriptAtRest.Shared.Scripts
{
    public class Base64Helper
    {
        public static string DecodeBase64(String Encoded)
        {
            var encodedBytes = Convert.FromBase64String(Encoded);
            return Encoding.UTF8.GetString(encodedBytes);
        }

        public static string EncodeBase64(String ValueToEncode)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(ValueToEncode);
            return Convert.ToBase64String(bytes);
        }
    }
}