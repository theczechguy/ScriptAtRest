﻿namespace ScriptAtRest.Shared.Scripts
{
    public class RefreshTokenRequest
    {
        public string Token { get; set; }
    }
}