﻿namespace ScriptAtRest.Shared.Scripts
{
    public static class ScriptJobStates
    {
        /// <summary>
        /// Initial stage of script job where it's waiting to be picked up and sent to an agent
        /// </summary>
        public const string InServerQueue = "Queued";

        /// <summary>
        /// No available agent was found for scriptjob
        /// </summary>
        public const string NoAgentAvailable = "NoAgentAvailable";

        /// <summary>
        /// Scriptjob was sent for processing to agent
        /// </summary>
        public const string SentToAgent = "SentToAgent";

        /// <summary>
        /// Scriptjob was accepted by agent
        /// </summary>
        public const string Acknowledged = "Agent acknowledged";

        /// <summary>
        /// Scriptjob is being executed by agent
        /// </summary>
        public const string Executing = "Executing";

        /// <summary>
        /// Scripjob completed
        /// </summary>
        public const string Completed = "Completed";

        /// <summary>
        /// Scriptjob had fatal failure
        /// </summary>
        public const string Failed = "Failed";

        /// <summary>
        /// Script job had failed too many times
        /// </summary>
        public const string TooManyRetries = "TooManyRetries";
    }
}