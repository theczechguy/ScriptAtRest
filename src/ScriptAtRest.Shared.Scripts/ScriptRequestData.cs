﻿namespace ScriptAtRest.Shared.Scripts
{
    public class ScriptRequestData
    {
        public Guid ServerScriptJobId { get; set; }
        public string ScriptEncodedContent { get; set; }
        public string ScriptArguments { get; set; }
        public string ScriptExecutable { get; set; }
        public string ScriptFileExtension { get; set; }
        public List<ScriptParamModel> ScriptParamModel { get; set; }
        public int ScriptTimeoutSeconds { get; set; }
        public bool ShouldDeleteFileAfterExecution { get; set; }
    }
}