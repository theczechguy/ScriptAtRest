﻿namespace ScriptAtRest.Shared.Scripts
{
    public class ScriptRunResultData
    {
        public Guid ServerScriptJobId { get; set; }
        public DateTime FinishedOn { get; set; }
        public double ExecutionTimeSeconds { get; set; }
        public string Status { get; set; }
        public string EncodedStandardOutput { get; set; }
        public string EncodedErrorOutput { get; set; }
        public int ScriptExitCode { get; set; }
    }
}