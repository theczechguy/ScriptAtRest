﻿namespace ScriptAtRest.Shared.Scripts
{
    public static class SignalRAgentMethods
    {
        public const string RunScriptMethod = "RunScript";
        public const string TerminateConnectionMethod = "TerminateConnection";
    }
}