﻿namespace ScriptAtRest.Shared.Scripts
{
    public enum SignalRServerMethods
    {
        ScriptRunAck,
        ScriptRunUpdateState,
        ScriptRunFinished,
        ScriptRunFatalFailure,
        RenewTokens,
        SuspendAgent,
        ResumeAgent
    }
}