﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv.Models;
using ScriptAtRestServer.IdentityAuth;
using System;
using System.Threading.Tasks;

namespace ScriptAtRestServer.Controllers
{
    [Produces("application/json")]
    [Authorize(Roles = UserRoles.Approved)]
    [Authorize(Roles = UserRoles.Admin)]
    [ApiController]
    [Route("api/[controller]")]
    public class AgentCapabilityController : Controller
    {
        private readonly ILogger<AgentCapabilityController> _logger;
        private readonly IAgentCapabilityService _agentCapabilityService;

        public AgentCapabilityController(ILogger<AgentCapabilityController> logger, IAgentCapabilityService agentCapabilityService)
        {
            _logger = logger;
            _agentCapabilityService = agentCapabilityService;
        }

        [HttpPost()]
        public async Task<IActionResult> RegisterAgentCapability([FromBody] RegisterAgentCapabilityModel registerAgentCapabilityModel)
        {
            if (string.IsNullOrWhiteSpace(registerAgentCapabilityModel.Name))
            {
                return BadRequest("You must specify at least capability name !");
            }
            var newCapability = await _agentCapabilityService.NewAgentCapability(registerAgentCapabilityModel.Name, registerAgentCapabilityModel.Description);
            if (newCapability.IsFailure)
            {
                return BadRequest(newCapability.ErrorMessage);
            }
            return CreatedAtAction(nameof(GetAgentCapabilityByName), new { CapabilityName = newCapability.Value.Name }, newCapability.Value);
        }

        [HttpGet("name/{CapabilityName}")]
        public async Task<IActionResult> GetAgentCapabilityByName(string CapabilityName)
        {
            if (string.IsNullOrWhiteSpace(CapabilityName))
            {
                return BadRequest("You must specify capability name !");
            }
            var capability = await _agentCapabilityService.GetAgentCapabilityByName(CapabilityName);
            if (capability.IsFailure)
            {
                return BadRequest("Capability not found");
            }
            return Ok(capability.Value);
        }

        [HttpGet("id/{CapabilityId}")]
        public async Task<IActionResult> GetAgentCapabilityById(Guid CapabilityId)
        {
            if (CapabilityId == Guid.Empty)
            {
                return BadRequest("You must specify capability id !");
            }
            var capability = await _agentCapabilityService.GetAgentCapabilityById(CapabilityId);
            if (capability.IsFailure)
            {
                return BadRequest("Capability not found");
            }
            return Ok(capability.Value);
        }

        [HttpDelete("{CapabilityName}")]
        public async Task<IActionResult> RemoveAgentCapabilityByName(string CapabilityName)
        {
            _logger.LogInformation("Received request for {action} : {name}", nameof(RemoveAgentCapabilityByName), CapabilityName);
            var result = await _agentCapabilityService.RemoveAgentCapabilityByName(CapabilityName);
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok($"Capability deleted: {result.Value}");
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCapabilities()
        {
            _logger.LogInformation("Received request to list all registered capabilities");
            var result = await _agentCapabilityService.ListAllCapabilities();
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(result.Value);
        }
    }
}