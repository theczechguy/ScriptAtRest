﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Domains.AgentSrv;
using ScriptAtRest.Server.Domains.AgentSrv.Models;
using ScriptAtRest.Server.Domains.Token;
using ScriptAtRest.Shared.Scripts;
using ScriptAtRestServer.Helpers;
using ScriptAtRestServer.IdentityAuth;
using System;
using System.Threading.Tasks;

namespace ScriptAtRestServer.Controllers
{
    [Produces("application/json")]
    [Authorize(Roles = UserRoles.Approved)]
    [Authorize(Roles = UserRoles.Admin)]
    [ApiController]
    [Route("api/[controller]")]
    public class AgentController : Controller
    {
        private readonly ILogger<AgentController> _logger;
        private readonly IAgentService _agentService;
        private readonly ITokenService _tokenService;
        private readonly IConfiguration _configuration;

        public AgentController(ILogger<AgentController> logger, IAgentService agentService, ITokenService tokenService, IConfiguration configuration)
        {
            _logger = logger;
            _agentService = agentService;
            _tokenService = tokenService;
            _configuration = configuration;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAgent([FromBody] RegisterAgentModel registerAgentModel)
        {
            _logger.LogInformation("Received request to register a new agent : {@agent}", registerAgentModel);
            var newTokens = await _agentService.RegisterAgent(registerAgentModel.AgentName);
            if (!newTokens.IsSuccess)
            {
                return BadRequest(newTokens.ErrorMessage);
            }
            return Ok(new
            {
                AgentName = newTokens.Value.AgentName,
                AgentId = newTokens.Value.AgentId,
                Token = newTokens.Value.Token
            });
        }

        [HttpPost("{AgentId}/capability/{CapabilityId}")]
        public async Task<IActionResult> AssignCapabilityToAgent(Guid AgentId, Guid CapabilityId)
        {
            _logger.LogInformation("Received request to assign capability: {cap} to agent: {agent}", CapabilityId, AgentId);
            var result = await _agentService.AssignCapabilityToAgent(AgentId, CapabilityId);
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(new GenericResponse
            {
                Message = "Capability added to agent",
                Status = "success"
            });
        }

        [HttpDelete("{AgentId}/capability/{CapabilityId}")]
        public async Task<IActionResult> RemoveCapabilityFromAgent(Guid AgentId, Guid CapabilityId)
        {
            _logger.LogInformation("Received request to remove capability: {cap} from agent: {agent}", CapabilityId, AgentId);
            var agentModel = await _agentService.RemoveCapabilityFromAgent(AgentId, CapabilityId);
            return Ok(agentModel);
        }

        [HttpGet("{AgentId}")]
        public async Task<IActionResult> GetAgentById(Guid AgentId)
        {
            _logger.LogInformation("Received request to get agent with id : {@agent}", AgentId);
            var agentModel = await _agentService.GetAgentById(AgentId);
            if (agentModel.IsFailure)
            {
                return BadRequest("Agent not found");
            }
            return Ok(agentModel.Value);
        }

        /// <summary>
        /// Manually reset tokens for specified agent id
        /// </summary>
        [HttpPost("{AgentId}/resettoken")]
        public async Task<IActionResult> ResetAgentTokensById(Guid AgentId)
        {
            _logger.LogInformation("Received request to reset tokens for agent: {agent}", AgentId);
            var result = await _agentService.ResetTokensByAgentId(AgentId);
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(new
            {
                Token = result.Value
            });
        }

        [AllowAnonymous]
        [HttpPost("refreshtoken")]
        public async Task<IActionResult> RefreshToken(RefreshTokenRequest refreshTokenRequest)
        {
            if (refreshTokenRequest is null)
            {
                return Unauthorized("You must provide valid refresh token");
            }
            var refreshTokenValidationResult = await _tokenService.IsProvidedRefreshTokenValid(refreshTokenRequest.Token);
            if (refreshTokenValidationResult.IsFailure)
            {
                return Unauthorized("You must provide valid refresh token");
            }
            var agent = await _agentService.GetAgentByIdentityName(refreshTokenValidationResult.Value.UserName);
            if (agent.IsFailure)
            {
                return BadRequest(agent.ErrorMessage);
            }
            var tokenResult = await _tokenService.GenerateTokensAsync(refreshTokenValidationResult.Value.Id,
                                                                      HttpContext,
                                                                      int.Parse(_configuration["JWT:AgentTokenExpirationMinutes"]),
                                                                      int.Parse(_configuration["JWT:AgentRefreshTokenExpirationMinutes"]));
            if (tokenResult.IsFailure)
            {
                return BadRequest(tokenResult.ErrorMessage);
            }
            return Ok(new NewTokensModel
            {
                RefreshToken = tokenResult.Value.RefreshToken.Token,
                RefreshTokenExpiration = tokenResult.Value.RefreshToken.ExpiryOn,
                Token = tokenResult.Value.Token.Token,
                TokenExpiration = tokenResult.Value.Token.ExpiryOn
            });
        }

        /// <summary>
        /// List all registered agents
        /// </summary>
        [HttpGet()]
        public async Task<IActionResult> GetAllAgents()
        {
            _logger.LogInformation("Received request to list all agents");
            var listResult = await _agentService.GetAllAgents();
            if (listResult.IsFailure)
            {
                return BadRequest(listResult.ErrorMessage);
            }
            return Ok(listResult.Value);
        }

        [HttpDelete("{AgentId}")]
        public async Task<IActionResult> DeleteAgent(Guid AgentId)
        {
            _logger.LogInformation("Received request to delete agent with id: {agentid}", AgentId);
            var result = await _agentService.DeleteAgentById(AgentId);
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(new GenericResponse
            {
                Status = "Successfull",
                Message = "Agent deleted"
            });
        }
    }
}