﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Domains.Token;
using ScriptAtRest.Server.Domains.Token.Models;
using ScriptAtRest.Server.IdentityAuth.Models;
using ScriptAtRest.Shared.Scripts;
using ScriptAtRestServer.Helpers;
using ScriptAtRestServer.Helpers.Constants;
using ScriptAtRestServer.IdentityAuth;
using System.Linq;
using System.Threading.Tasks;

namespace JwtAuthentication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        private readonly DataContext _context;
        private readonly ILogger<AuthenticationController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        private readonly IConfiguration _configuration;

        public AuthenticationController(UserManager<ApplicationUser> UserManager,
                                        ILogger<AuthenticationController> Logger,
                                        IMapper Mapper,
                                        DataContext Context,
                                        ITokenService TokenService,
                                        IConfiguration configuration)
        {
            _userManager = UserManager;
            _logger = Logger;
            _mapper = Mapper;
            _context = Context;
            _tokenService = TokenService;
            _configuration = configuration;
        }

        /// <summary>
        /// Generate access token for user
        /// </summary>
        /// <param name="LoginModel"></param>
        /// <returns>Access token</returns>
        /// <response code="200">Returns new access token</response>
        /// <response code="400">Request fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel LoginModel)
        {
            _logger.LogInformation("Generate token for : {user}", LoginModel.Username);

            if (!ModelState.IsValid || LoginModel == null)
            {
                return BadRequest(new
                {
                    Status = ResponseConstants.StatusFail,
                    Message = "Please verify you entered inputs in correct format"
                });
            };

            var identityUserValidationResult = await _tokenService.ValidateUser(LoginModel.Username, LoginModel.Password);
            if (!identityUserValidationResult.IsSuccess)
            {
                return BadRequest(new
                {
                    Status = ResponseConstants.StatusFail,
                    Message = "Please verify user and password and try again"
                });
            }
            _ = await _tokenService.RevokeAllRefreshTokensAsync(identityUserValidationResult.Value, HttpContext);
            int accesstokenrefresh = int.Parse(_configuration["JWT:TokenExpirationMinutes"]);
            int refreshTokenRefresh = int.Parse(_configuration["JWT:RefreshTokenExpirationMinutes"]);
            var tokenResult = await _tokenService.GenerateTokensAsync(identityUserValidationResult.Value.Id, HttpContext, accesstokenrefresh, refreshTokenRefresh);
            if (!tokenResult.IsSuccess)
            {
                return BadRequest(tokenResult.ErrorMessage);
            }

            var newTokens = _mapper.Map<NewTokensModel>(tokenResult.Value);
            return Ok(newTokens);
        }

        /// <summary>
        /// Refresh access and refresh token
        /// </summary>
        /// <remarks>Refresh token can be provided via cookie or body</remarks>
        /// <returns>New refresh and access token</returns>
        /// <response code="200">Returns new tokens</response>
        /// <response code="400">Request fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [AllowAnonymous]
        [HttpPost]
        [Route("refreshtoken")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest RequestModel)
        {
            _logger.LogInformation("Refresh token request");
            _logger.LogDebug("Searching for token in body and cookies");
            var token = RequestModel.Token ?? Request.Cookies["refreshToken"];
            if (token == null)
            {
                _logger.LogDebug("Token not found");
                return BadRequest(new GenericResponse
                {
                    Status = ResponseConstants.StatusFail,
                    Message = ResponseConstants.TokenMissing
                });
            }

            var user = await _context.Users.Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(y => y.Token == token && y.UserId == x.Id));

            var existingRefreshTokenResult = await _tokenService.GetValidRefreshToken(token, user);
            if (!existingRefreshTokenResult.IsSuccess)
            {
                return BadRequest("No valid refresh token found. Use login endpoint to generate new one");
            }
            await _tokenService.RevokeRefreshTokenAsync(existingRefreshTokenResult.Value, user, HttpContext);

            var tokenResult = await _tokenService.GenerateTokensAsync(user.Id, HttpContext, int.Parse(_configuration["JWT:TokenExpirationMinutes"]), int.Parse(_configuration["JWT:RefreshTokenExpirationMinutes"]));
            if (!tokenResult.IsSuccess)
            {
                return BadRequest(tokenResult.ErrorMessage);
            }

            var newTokens = _mapper.Map<NewTokensModel>(tokenResult.Value);
            return Ok(newTokens);
        }

        /// <summary>
        /// Set new password for a user
        /// </summary>
        /// <remarks>Only users with Admin role can run this request</remarks>
        /// <param name="UserName"></param>
        /// <param name="NewPasswordModel"></param>
        /// <returns>Confirmation message</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        [Route("{UserName}/passwordreset")]
        public async Task<IActionResult> ResetUserPassword(string UserName, [FromBody] PasswordResetModel NewPasswordModel)
        {
            _logger.LogInformation("Reset password for user: {user}", UserName);
            var user = await _userManager.FindByNameAsync(UserName) ?? throw new AppException("User not found");
            string resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, resetToken, NewPasswordModel.Password);
            return Ok(new GenericResponse { Status = ResponseConstants.StatusOk, Message = "New password set" });
        }

        /// <summary>
        /// Revoke existing refresh token
        /// </summary>
        /// <param name="RequestModel"></param>
        /// <returns>Confirmation message</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        [Route("revoketoken")]
        public async Task<IActionResult> RevokeToken([FromBody] RevokeTokenRequest RequestModel)
        {
            _logger.LogInformation("Revoke token request");
            var token = RequestModel.Token ?? Request.Cookies["refreshToken"];
            if (token == null)
            {
                return BadRequest(new GenericResponse
                {
                    Status = ResponseConstants.StatusFail,
                    Message = ResponseConstants.TokenMissing
                });
            }

            var user = await _context.Users.Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(y => y.Token == token && y.UserId == x.Id));
            if (user == null)
            {
                return BadRequest(new GenericResponse { Status = ResponseConstants.StatusFail, Message = "User with specified token not found" });
            }

            var existingRefreshTokenResult = await _tokenService.GetValidRefreshToken(token, user);
            if (!existingRefreshTokenResult.IsSuccess)
            {
                return BadRequest(new GenericResponse { Status = ResponseConstants.StatusFail, Message = "No active refresh token found" });
            }
            await _tokenService.RevokeRefreshTokenAsync(existingRefreshTokenResult.Value, user, HttpContext);
            return Ok(new GenericResponse { Status = ResponseConstants.StatusOk, Message = "Token revoked" });
        }
    }
}