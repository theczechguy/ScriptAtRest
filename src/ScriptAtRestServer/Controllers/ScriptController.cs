﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.Script;
using ScriptAtRest.Server.Domains.Script.Models;
using ScriptAtRestServer.Helpers;
using ScriptAtRestServer.IdentityAuth;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScriptAtRestServer.Controllers
{
    [Produces("application/json")]
    [Authorize(Roles = UserRoles.Approved)]
    [ApiController]
    [Route("api/[controller]")]
    public class ScriptController : Controller
    {
        private readonly IScriptService _scriptService;
        private readonly ILogger<ScriptController> _logger;
        private readonly IMapper _mapper;

        public ScriptController(
            IScriptService ScriptService,
            ILogger<ScriptController> Logger,
            IMapper Mapper
        )
        {
            _scriptService = ScriptService;
            _logger = Logger;
            _mapper = Mapper;
        }

        /// <summary>
        /// Register new script
        /// </summary>
        /// <param name="Model"></param>
        /// <remarks>
        /// # Sample request
        /// ```
        ///     POST /scripts/register
        ///     {
        ///        "Name": "Script name",
        ///        "Type": 1,
        ///        "EncodedContent": "aGVsbG8=",
        ///        "Timeout": 60
        ///     }
        /// ```
        /// # Parameter requirements
        /// - Name must be unique
        /// - Script type must exist before script registration
        /// - Encoded content is BASE64 UTF8 encoded string
        /// - Timeout is in seconds
        ///
        /// # Encoding script content
        /// - ***Powershell***
        ///     ```
        ///     $Text = "script content"
        ///     [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($Text))
        ///     ```
        /// - ***Python***
        ///     ```
        ///     import base64
        ///     base64.b64encode(bytes('script content', 'utf-8'))
        ///     ```
        /// </remarks>
        /// <returns>A newly created script</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If registration fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterScriptModel Model)
        {
            _logger.LogInformation("Register new script");

            var newScriptResult = await _scriptService.CreateAsync(Model);
            if (!newScriptResult.IsSuccess)
            {
                return BadRequest(newScriptResult.ErrorMessage);
            }

            _logger.LogInformation("Script registered with id : {scriptId}", newScriptResult.Value.id);
            return CreatedAtAction(nameof(GetByIdAsync), new { Id = newScriptResult.Value.id }, newScriptResult.Value);
        }

        /// <summary>
        /// Get all registered scripts
        /// </summary>
        /// <returns>A list of all scripts</returns>
        /// <response code="200">Returns a list of all scripts</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAll()
        {
            _logger.LogInformation("Get all scripts");

            IEnumerable<ScriptEntity> scripts = _scriptService.GetAll();
            IList<ScriptSimplifiedModel> model = _mapper.Map<IList<ScriptSimplifiedModel>>(scripts);
            _logger.LogInformation("Scripts retrieved : {scriptCount}", model.Count);

            return Ok(model);
        }

        /// <summary>
        /// Get script by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Script</returns>
        /// <response code="200">Returns script</response>
        /// <response code="400">If script id cannot be found</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet("{Id}")]
        [ActionName("GetByIdAsync")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByIdAsync(int Id)
        {
            _logger.LogInformation("Get Script with id : {scriptid}", Id);

            var result = await _scriptService.GetByIdAsync(Id);
            if (result.IsSuccess)
            {
                var model = _mapper.Map<ScriptModel>(result.Value);
                _logger.LogInformation("Retrieved script : {scriptName}", model.Name);
                return Ok(model);
            }
            return BadRequest(result.ErrorMessage);
        }

        /// <summary>
        /// Get script by name
        /// </summary>
        /// <param name="RequestModel"></param>
        /// <response code="200">Returns script</response>
        /// <response code="400">If script name cannot be found</response>
        /// <response code="500">If application experiences fatal error</response>
        /// <returns></returns>
        [HttpGet("byname")]
        [ActionName("GetScriptByNameAsync")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetScriptByNameAsync([FromBody] ScriptByNameModel RequestModel)
        {
            _logger.LogInformation("Get script with name : {scriptname}", RequestModel.Name);

            if (string.IsNullOrWhiteSpace(RequestModel.Name))
            {
                throw new AppException("You must specify script name in request body !");
            }

            var scriptResult = await _scriptService.GetScriptByNameAsync(RequestModel.Name);
            if (!scriptResult.IsSuccess)
            {
                return BadRequest(scriptResult.ErrorMessage);
            }
            var model = _mapper.Map<ScriptModel>(scriptResult.Value);
            _logger.LogInformation("Retrieved script : {scriptName}", model.id);
            return Ok(model);
        }

        /// <summary>
        /// Delete script by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Script</returns>
        /// <response code="200">Returns empty response</response>
        /// <response code="400">If script id cannot be found</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpDelete("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            _logger.LogInformation("Delete script with ID : {scriptid}", Id);

            var result = await _scriptService.DeleteAsync(Id);
            if (result.IsSuccess)
            {
                _logger.LogInformation("Script deleted");
                return Ok();
            }
            return BadRequest(result.ErrorMessage);
        }

        /// <summary>
        /// Update script definition
        /// </summary>
        /// <remarks>
        /// # Sample request
        /// - this request will update script with id ***1*** with new script content and new timeout value
        ///     ```
        ///         PUT /scripts/1
        ///         {
        ///             "EncodedContent": "aGVsbG8=",
        ///             "Timeout": 59
        ///         }
        ///     ```
        /// # Remarks
        /// - there is no need to specify all fields, only put in the request things that you want to change
        /// - encodedContent is generated the same way as when you register new scripts. IF you need help read docu for the scripts/register action
        /// </remarks>
        /// <param name="Model"></param>
        /// <param name="Id"></param>
        /// <returns>Updated script</returns>
        /// <response code="200">Returns updated definition</response>
        /// <response code="400">If update fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpPut("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateScriptAsync([FromBody] UpdateScriptModel Model, int Id)
        {
            _logger.LogInformation("Update script with id : {id}", Id);

            var updatedScriptResult = await _scriptService.UpdateScriptByIdAsync(Id, Model);
            if (!updatedScriptResult.IsSuccess)
            {
                return BadRequest(updatedScriptResult.ErrorMessage);
            }
            return Ok(_mapper.Map<ScriptModel>(updatedScriptResult.Value));
        }

        /// <summary>
        /// Get list of registered script types
        /// </summary>
        /// <returns>A list of script types</returns>
        /// <response code="200">Returns a list of all script types</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet("type")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAllScriptTypes()
        {
            _logger.LogInformation("Get all script types");

            IEnumerable<ScriptTypeEntity> scriptTypes = _scriptService.GetAllTypes();
            IList<ScriptTypeModel> model = _mapper.Map<IList<ScriptTypeModel>>(scriptTypes);
            _logger.LogInformation("Script types retrieved : {typesCOunt}", model.Count);
            return Ok(model);
        }

        /// <summary>
        /// Get script type by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Script type</returns>
        /// <response code="200">Returns script type</response>
        /// <response code="400">If script type id not found</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet("type/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetScripTypeById(int Id)
        {
            _logger.LogInformation("Get script type with id : {id}", Id);

            var scriptTypeResult = await _scriptService.GetTypeByIdAsync(Id);
            if (!scriptTypeResult.IsSuccess)
            {
                return BadRequest(scriptTypeResult.ErrorMessage);
            }
            ScriptTypeModel model = _mapper.Map<ScriptTypeModel>(scriptTypeResult.Value);
            _logger.LogInformation("Retrieved script type : {scriptName}", model.Name);
            return Ok(model);
        }

        /// <summary>
        /// Register script type
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        /// <response code="200">Returns script type</response>
        /// <response code="400">If script type id not found</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpPost("type")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RegisterScriptType([FromBody] RegisterScriptTypeModel Model)
        {
            _logger.LogInformation("Register new script type");

            var newScriptTypeResult = await _scriptService.CreateTypeAsync(Model.Name, Model.Runner, Model.FileExtension, Model.ScriptArgument);
            if (newScriptTypeResult.IsFailure)
            {
                return BadRequest(newScriptTypeResult.ErrorMessage);
            }

            _logger.LogInformation("Registered new script type with id : {id}", newScriptTypeResult.Value.Id);
            _logger.LogDebug("New script type : {@scripttype}", newScriptTypeResult.Value);

            return CreatedAtAction(nameof(GetScripTypeById), new { newScriptTypeResult.Value.Id }, newScriptTypeResult.Value);
        }

        /// <summary>
        /// Delete script type by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="200">Empty response</response>
        /// <response code="400">If script type id not found</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpDelete("type/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteScriptTypeAsync(int Id)
        {
            _logger.LogInformation("Delete script type with id : {id}", Id);

            var result = await _scriptService.DeleteTypeAsync(Id);
            if (result.IsSuccess)
            {
                _logger.LogInformation("Script type deleted");
                return Ok();
            }
            return BadRequest(result.ErrorMessage);
        }

        /// <summary>
        /// Update script type definition
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="Id"></param>
        /// <returns>Updated script type</returns>
        /// <response code="200">Returns updated script type definition</response>
        /// <response code="400">If script type update fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpPut("type/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateScriptTypeAsync([FromBody] UpdateScriptTypeModel Model, int Id)
        {
            _logger.LogInformation("Update script type with id : {id}", Id);

            ScriptTypeEntity scriptType = _mapper.Map<ScriptTypeEntity>(Model);
            var updatedScriptTypeResult = await _scriptService.UpdateTypeById(Id, scriptType);
            if (updatedScriptTypeResult.IsFailure)
            {
                return BadRequest(updatedScriptTypeResult.ErrorMessage);
            }
            return Ok(_mapper.Map<ScriptTypeModel>(updatedScriptTypeResult.Value));
        }
    }
}