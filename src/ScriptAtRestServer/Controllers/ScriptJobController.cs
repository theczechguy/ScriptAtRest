﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Domains.ScriptScheduling.Models;
using ScriptAtRest.Server.Domains.ScriptSchedulingSrv;
using ScriptAtRestServer.IdentityAuth;
using System;
using System.Threading.Tasks;

namespace ScriptAtRestServer.Controllers
{
    [Produces("application/json")]
    [Authorize(Roles = UserRoles.Approved)]
    [ApiController]
    [Route("api/[controller]")]
    public class ScriptJobController : Controller
    {
        private readonly ILogger<ScriptJobController> _logger;
        private readonly IScriptSchedulingService _scriptSchedulingService;

        public ScriptJobController(
            ILogger<ScriptJobController> logger,
            IScriptSchedulingService scriptSchedulingService)
        {
            _logger = logger;
            _scriptSchedulingService = scriptSchedulingService;
        }

        /// <summary>
        /// Create a script job for a specified script id
        /// Body of the request can contain script parameters
        /// Script parameter values must be encoded using Base64 UTF8 encoding
        /// </summary>
        /// <param name="scheduleScriptJobModel"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<IActionResult> ScheduleScriptRun([FromBody] ScheduleScriptJobModel scheduleScriptJobModel)
        {
            _logger.LogInformation("Schedule script run for script id: {id}", scheduleScriptJobModel.ScriptId);
            var scriptJob = await _scriptSchedulingService.ScheduleScriptRun(scheduleScriptJobModel.ScriptId, scheduleScriptJobModel.ScriptParamModel, scheduleScriptJobModel.RequiredCapabilityName);
            if (!scriptJob.IsSuccess)
            {
                _logger.LogWarning(scriptJob.ErrorMessage);
                return BadRequest($"Unable to schedule scriptjob : {scriptJob.ErrorMessage}");
            }
            _logger.LogInformation("Script job scheduled : {ScriptJobId}", scriptJob.Value.Id);
            return Ok(scriptJob.Value);
        }

        /// <summary>
        /// Get details of a script job
        /// </summary>
        /// <param name="ScriptJobId"></param>
        /// <response code="200">Return a script job details</response>
        [HttpGet("{ScriptJobId}")]
        public async Task<IActionResult> GetScriptJob(Guid ScriptJobId)
        {
            _logger.LogInformation("Get scriptjob with id : {id}", ScriptJobId);
            var scriptJobResult = await _scriptSchedulingService.GetScriptJobById(ScriptJobId);
            if (scriptJobResult.IsSuccess)
            {
                return Ok(scriptJobResult.Value);
            }
            return BadRequest(scriptJobResult.ErrorMessage);
        }

        [HttpGet("list")]
        public async Task<IActionResult> ListRecentJobs()
        {
            _logger.LogInformation("List recent script jobs");
            var listResult = await _scriptSchedulingService.ListRecentJobs();
            if (listResult.IsSuccess)
            {
                return Ok(listResult.Value);
            }
            return BadRequest(listResult.ErrorMessage);
        }
    }
}