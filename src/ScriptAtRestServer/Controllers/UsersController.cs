﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Domains.User;
using ScriptAtRest.Server.Domains.User.Models;
using ScriptAtRest.Server.IdentityAuth.Models;
using ScriptAtRestServer.Helpers;
using ScriptAtRestServer.Helpers.Constants;
using ScriptAtRestServer.IdentityAuth;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScriptAtRestServer.Controllers
{
    [Produces("application/json")]
    [Authorize(Roles = UserRoles.Approved)]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ILogger<UsersController> _logger;
        protected readonly IConfiguration _configuration;
        private readonly IUserService _userService;

        public UsersController(
            IMapper Mapper,
            ILogger<UsersController> Logger,
            IConfiguration Configuration,
            IUserService UserService
            )
        {
            _mapper = Mapper;
            _logger = Logger;
            _configuration = Configuration;
            _userService = UserService;
        }

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="RegisterModel"></param>
        /// <returns>A newly created User</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If registration fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel RegisterModel)
        {
            _logger.LogInformation("Register new user with username : {username}", RegisterModel.Username);
            if (!_configuration.GetValue<bool>("UserManagement:AllowNewUserRegistration"))
            {
                _logger.LogWarning("User registration is disabled by configuration option 'AllowNewUserRegistration'");
                return Unauthorized("User registration is disabled by administrator");
            }
            var newUserResult = await _userService.CreateUser(RegisterModel, ScriptAtRest.Server.IdentityAuth.ApplicationUserType.Regular);
            if (!newUserResult.IsSuccess)
            {
                _logger.LogWarning("User registration failed : {err}", newUserResult.ErrorMessage);
                return BadRequest(newUserResult.ErrorMessage);
            }
            return Ok(_mapper.Map<UserModel>(newUserResult.Value));
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns>Confirmation message</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If registration fails</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpDelete]
        [Route("{UserName}")]
        public async Task<IActionResult> Delete(string UserName)
        {
            _logger.LogInformation("Delete user: {User}", UserName);
            var result = await _userService.DeleteUser(UserName, ScriptAtRest.Server.IdentityAuth.ApplicationUserType.Regular);
            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(new GenericResponse { Status = ResponseConstants.StatusOk, Message = $"User: {UserName} deleted" });
        }

        /// <summary>
        /// Get list of all users
        /// </summary>
        /// <returns>List of users</returns>
        /// <response code="200">If successfull</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            _logger.LogInformation("Get all users");
            var users = await _userService.GetAllUsers();
            if (!users.IsSuccess)
            {
                return BadRequest(users.ErrorMessage);
            }
            return Ok(_mapper.Map<List<UserModel>>(users.Value));
        }

        /// <summary>
        /// Find user by name
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns>User details</returns>
        /// <response code="200">If successfull</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet]
        [Route("{UserName}")]
        public async Task<IActionResult> GetUserByName(string UserName)
        {
            _logger.LogInformation("Get user with name : {user}", UserName);
            var user = await _userService.GetUserByName(UserName, ScriptAtRest.Server.IdentityAuth.ApplicationUserType.Regular);
            if (user.IsFailure)
            {
                return BadRequest(user.ErrorMessage);
            }
            return Ok(_mapper.Map<UserModel>(user.Value));
        }

        /// <summary>
        /// Get roles of specified user
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns>Returns list of roles the user is member of</returns>
        /// <response code="200">If successfull</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [HttpGet]
        [Route("{UserName}/roles")]
        public async Task<IActionResult> GetUserRoles(string UserName)
        {
            _logger.LogInformation("Get roles of user: {User}", UserName);
            var roles = await _userService.GetUserRoles(UserName);
            if (roles.IsFailure)
            {
                return BadRequest(roles.ErrorMessage);
            }
            UserWithRolesModel model = new UserWithRolesModel
            {
                Username = UserName,
                Roles = roles.Value
            };
            return Ok(model);
        }

        /// <summary>
        /// Approve user after registration
        /// </summary>
        /// <remarks>Only users with Admin role can run this request</remarks>
        /// <param name="UserName"></param>
        /// <returns>Confirmation message</returns>
        /// <response code="200">Returns user information</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        [Route("{UserName}/approve")]
        public async Task<IActionResult> ApproveUser(string UserName)
        {
            _logger.LogInformation("Approve user : {user}", UserName);
            var user = await _userService.ApproveUser(UserName);
            if (user.IsFailure)
            {
                return BadRequest(user.ErrorMessage);
            }
            return Ok(_mapper.Map<UserModel>(user.Value));
        }

        /// <summary>
        /// Grant access to a role
        /// </summary>
        /// <remarks>Only users with Admin role can run this request</remarks>
        /// <param name="UserName"></param>
        /// <param name="AddRoleModel"></param>
        /// <returns>Confirmation message</returns>
        /// <response code="200">If successfull</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        [Route("{UserName}/addrole")]
        public async Task<IActionResult> AddUserRole(string UserName, [FromBody] RoleModel AddRoleModel)
        {
            _logger.LogInformation("Add role: {role} to user: {user}", AddRoleModel.RoleName, UserName);
            var result = await _userService.AddUserToRole(UserName,
                                                          AddRoleModel.RoleName,
                                                          ScriptAtRest.Server.IdentityAuth.ApplicationUserType.Regular);
            if (result.IsFailure)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(new GenericResponse { Status = ResponseConstants.StatusOk, Message = "Role granted" });
        }

        /// <summary>
        /// Remove access to a role
        /// </summary>
        /// <remarks>Only users with Admin role can run this request</remarks>
        /// <param name="UserName"></param>
        /// <param name="RemoveRoleModel"></param>
        /// <returns>Confirmation message</returns>
        /// <response code="200">If successfull</response>
        /// <response code="400">If request failes</response>
        /// <response code="500">If application experiences fatal error</response>
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        [Route("{UserName}/removerole")]
        public async Task<IActionResult> RemoveUserRole(string UserName, [FromBody] RoleModel RemoveRoleModel)
        {
            _logger.LogInformation("Remove role: {role} from user: {user}", RemoveRoleModel.RoleName, UserName);
            var removeUserResult = await _userService.RemoveUserFromRole(UserName, RemoveRoleModel.RoleName);
            if (removeUserResult.IsFailure)
            {
                return BadRequest(removeUserResult.ErrorMessage);
            }
            return Ok(new GenericResponse { Status = ResponseConstants.StatusOk, Message = "Role membership removed" });
        }
    }
}