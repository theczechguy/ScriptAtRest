﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRestServer.IdentityAuth;

namespace ScriptAtRest.Server.Database
{
    public class DataContext : IdentityDbContext<ApplicationUser>
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options) { }

        public DbSet<ScriptEntity> Scripts { get; set; }
        public DbSet<ScriptTypeEntity> ScriptTypes { get; set; }
        public DbSet<ScriptJobEntity> ScriptJobs { get; set; }
        public DbSet<RefreshTokenEntity> RefreshTokens { get; set; }
        public DbSet<AgentEntity> Agents { get; set; }
        public DbSet<AgentCapabilityEntity> AgentCapabilities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // when script is deleted set scriptid of scriptjob to null
            modelBuilder.Entity<ScriptJobEntity>()
                        .HasOne(s => s.Script)
                        .WithMany()
                        .OnDelete(DeleteBehavior.SetNull);

            // when user is deleted delete all related refresh tokens
            modelBuilder.Entity<ApplicationUser>()
                        .HasMany(a => a.RefreshTokens)
                        .WithOne()
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ScriptEntity>()
                        .HasMany(s => s.ScriptHistory)
                        .WithOne()
                        .OnDelete(DeleteBehavior.Cascade);

            // Typically, this method is called only once when the first instance of a derived context is created.
            // https://docs.microsoft.com/en-us/dotnet/api/system.data.entity.dbcontext.onmodelcreating?view=entity-framework-6.2.0
            base.OnModelCreating(modelBuilder);
            SeedDefaultUser(modelBuilder);
            SeedDefaultRoles(modelBuilder);
            SeedAdminRoleAssignment(modelBuilder);
        }

        private void SeedAdminRoleAssignment(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "10000000-0000-0000-0000-000000000000",
                UserId = "00000000-0000-0000-0000-000000000001"
            });

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "20000000-0000-0000-0000-000000000000",
                UserId = "00000000-0000-0000-0000-000000000001"
            });
        }

        private void SeedDefaultUser(ModelBuilder modelBuilder)
        {
            ApplicationUser user = new ApplicationUser()
            {
                Id = "00000000-0000-0000-0000-000000000001",
                UserName = "Admin",
                NormalizedUserName = "ADMIN",
                Approved = true
            };

            PasswordHasher<ApplicationUser> hasher = new PasswordHasher<ApplicationUser>();
            user.PasswordHash = hasher.HashPassword(user, "ChangeMeNow1!");
            modelBuilder.Entity<ApplicationUser>().HasData(user);
        }

        private void SeedDefaultRoles(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "10000000-0000-0000-0000-000000000000", Name = "Admin", ConcurrencyStamp = "1", NormalizedName = "ADMIN" }
            );
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "20000000-0000-0000-0000-000000000000", Name = "Approved", ConcurrencyStamp = "1", NormalizedName = "APPROVED" }
            );
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "30000000-0000-0000-0000-000000000000", Name = "Agent", ConcurrencyStamp = "1", NormalizedName = "AGENT" }
            );
        }
    }
}