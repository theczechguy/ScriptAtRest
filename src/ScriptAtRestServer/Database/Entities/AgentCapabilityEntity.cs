﻿using System;
using System.Collections.Generic;

namespace ScriptAtRest.Server.Database.Entities
{
    public class AgentCapabilityEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<AgentEntity> Agents { get; set; }
    }
}