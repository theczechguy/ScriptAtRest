﻿using System;
using System.Collections.Generic;

namespace ScriptAtRest.Server.Database.Entities
{
    public class AgentEntity
    {
        public Guid Id { get; set; }
        public string UserDefinedName { get; set; }
        public string ConnectionStatus { get; set; }
        public string ConnectionId { get; set; }
        public DateTime LastConnected { get; set; }
        public string IdentityAgentName { get; set; }
        public Guid UserId { get; set; }
        public List<AgentCapabilityEntity> AgentCapabilities { get; set; }
        public List<Guid> AgentCapabilityIds { get; set; }
    }
}