﻿using System;
using System.Collections.Generic;

namespace ScriptAtRest.Server.Database.Entities
{
    public class ScriptEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptTypeEntity ScriptType { get; set; }
        public int Timeout { get; set; }
        public long LastModifiedTicksUtc { get; set; }
        public DateTime LastModifiedUtc { get; set; }
        public ICollection<ScriptHistoryEntity> ScriptHistory { get; set; }
    }
}