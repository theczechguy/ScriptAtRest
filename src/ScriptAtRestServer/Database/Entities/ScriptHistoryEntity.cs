﻿using System;

namespace ScriptAtRest.Server.Database.Entities
{
    public class ScriptHistoryEntity
    {
        public int Id { get; set; }
        public int ScriptEntityId { get; set; }
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptTypeEntity ScriptType { get; set; }
        public int Timeout { get; set; }
        public DateTime ModifiedUtc { get; set; }
    }
}