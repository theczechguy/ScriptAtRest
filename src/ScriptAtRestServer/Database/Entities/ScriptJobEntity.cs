﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Database.Entities
{
    public class ScriptJobEntity
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string JobStatus { get; set; }
        public string EncodedJobStandardOutput { get; set; }
        public string EncodedJobErrorOutput { get; set; }
        public int JobExitCode { get; set; }
        public string FailureDetails { get; set; }
        public Guid AssignedAgentId { get; set; }
        public ScriptEntity Script { get; set; }
        public Guid AgentCapabilityId { get; set; }
        public string ScriptRequestData { get; set; }
        public int RetryCount { get; set; }

        public ScriptJobEntity()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
        }
    }
}