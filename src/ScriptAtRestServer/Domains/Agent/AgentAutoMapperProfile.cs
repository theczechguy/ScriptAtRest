﻿using AutoMapper;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.AgentSrv.Models;

namespace ScriptAtRest.Server.Domains.AgentSrv
{
    public class AgentAutoMapperProfile : Profile
    {
        public AgentAutoMapperProfile()
        {
            CreateMap<AgentEntity, AgentModel>().ReverseMap();
        }
    }
}