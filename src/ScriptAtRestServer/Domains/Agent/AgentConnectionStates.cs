﻿namespace ScriptAtRest.Server.Domains.Agent
{
    public enum AgentConnectionState
    {
        Connected,
        Disconnected,
        InvalidatedByServerAfterRestart,
        AbortedByServer,
        Suspended
    }
}