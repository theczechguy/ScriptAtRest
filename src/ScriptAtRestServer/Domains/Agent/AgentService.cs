﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.Agent;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv;
using ScriptAtRest.Server.Domains.AgentSrv.Models;
using ScriptAtRest.Server.Domains.Token;
using ScriptAtRest.Server.Domains.User;
using ScriptAtRest.Server.Helpers;
using ScriptAtRest.Shared.Scripts;

using ScriptAtRestServer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.AgentSrv
{
    public interface IAgentService
    {
        Task<ServiceResult<AgentRegisteredModel>> RegisterAgent(string AgentName);

        Task<ServiceResult<AgentModel>> GetAgentByIdentityName(string IdentityName);

        Task<ServiceResult<AgentModel>> GetAgentById(Guid AgentId);

        Task<ServiceResult<AgentModel>> GetAnyAvailableAgent();

        Task<ServiceResult<AgentModel>> GetAvailableAgentWithCapabilityId(Guid CapabilityId);

        Task<ServiceResult<List<AgentModel>>> GetAllAgents();

        Task<ServiceResult<AgentModel>> UpdateAgentState(Guid AgentId, AgentConnectionState AgentConnectionState);

        Task<ServiceResult<AgentModel>> UpdateAgentOnConnect(Guid AgentId, string ConnectionId);

        Task<ServiceResult<AgentModel>> UpdateAgentOnDisconnect(Guid AgentId);

        Task<ServiceResult<AgentModel>> AssignCapabilityToAgent(Guid AgentId, Guid CapabilityId);

        Task<ServiceResult<AgentModel>> RemoveCapabilityFromAgent(Guid AgentId, Guid CapabilityId);

        Task<ServiceResult<NewTokensModel>> RenewTokensRequestedByAgent(string RefreshToken, Guid AgentId);

        Task<ServiceResult<string>> ResetTokensByAgentId(Guid AgentId);

        Task<ServiceResult<bool>> DeleteAgentById(Guid agentId);
    }

    public class AgentService : IAgentService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ILogger<AgentService> _logger;
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;
        private readonly IAgentCapabilityService _agentCapabilityService;
        private readonly IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> _hubContext;
        private readonly IConfiguration _configuration;

        public AgentService(
            DataContext dataContext,
            IMapper mapper,
            ILogger<AgentService> logger,
            IUserService userService,
            ITokenService tokenService,
            IAgentCapabilityService agentCapabilityService,
            IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> hubContext,
            IConfiguration configuration)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _logger = logger;
            _userService = userService;
            _tokenService = tokenService;
            _agentCapabilityService = agentCapabilityService;
            _hubContext = hubContext;
            _configuration = configuration;
        }

        public async Task<ServiceResult<List<AgentModel>>> GetAllAgents()
        {
            var allAgents = await _dataContext.Agents.ToListAsync();
            if (allAgents.Any())
            {
                return ServiceResult<List<AgentModel>>.Success(_mapper.Map<List<AgentModel>>(allAgents));
            }
            return ServiceResult<List<AgentModel>>.Failure("No agent found");
        }

        public async Task<ServiceResult<NewTokensModel>> RenewTokensRequestedByAgent(string RefreshToken, Guid AgentId)
        {
            var agent = await _getAgentEntityById(AgentId);
            var agentUser = await _userService.GetUserBynameWithRefreshToken(agent.IdentityAgentName);
            if (agentUser.IsFailure)
            {
                return ServiceResult<NewTokensModel>.Failure(agentUser.ErrorMessage);
            }
            var existingRefreshTokenResult = await _tokenService.GetValidRefreshToken(RefreshToken, agentUser.Value);
            if (!existingRefreshTokenResult.IsSuccess)
            {
                _logger.LogWarning("Agent: {agentid} provided invalid refresh token", AgentId);
                return null;
            }
            await _tokenService.RevokeRefreshTokenAsync(existingRefreshTokenResult.Value, agentUser.Value, null);
            var tokenResult = await _tokenService.GenerateTokensAsync(agentUser.Value.Id, null, int.Parse(_configuration["JWT:AgentTokenExpirationMinutes"]), int.Parse(_configuration["JWT:AgentRefreshTokenExpirationMinutes"]));
            return ServiceResult<NewTokensModel>.Success(_mapper.Map<NewTokensModel>(tokenResult.Value));
        }

        public async Task<ServiceResult<string>> ResetTokensByAgentId(Guid AgentId)
        {
            var agent = await _getAgentEntityById(AgentId);
            var agentUser = await _userService.GetUserBynameWithRefreshToken(agent.IdentityAgentName);
            if (agentUser.IsFailure)
            {
                return ServiceResult<string>.Failure(agentUser.ErrorMessage);
            }
            await _tokenService.RevokeAllRefreshTokensAsync(agentUser.Value, null);
            var tokenResult = await _tokenService.GenerateTokensAsync(agentUser.Value.Id, null, int.Parse(_configuration["JWT:AgentTokenExpirationMinutes"]), int.Parse(_configuration["JWT:AgentRefreshTokenExpirationMinutes"]));
            if (tokenResult.IsFailure)
            {
                return ServiceResult<string>.Failure(tokenResult.ErrorMessage);
            }
            return ServiceResult<string>.Success(tokenResult.Value.RefreshToken.Token);
        }

        /// <summary>
        /// Finds an available agent with the specified capability ID.
        /// </summary>
        /// <param name="CapabilityId">The ID of the capability to search for.</param>
        /// <returns>An <see cref="AgentModel"/> object representing the selected agent, or null if no agent is available.</returns>
        /// <remarks>
        /// This method searches the database for an agent with the specified capability ID and connection status "Connected". If multiple agents are found, a random agent is selected from the list.
        /// </remarks>
        public async Task<ServiceResult<AgentModel>> GetAvailableAgentWithCapabilityId(Guid CapabilityId)
        {
            _logger.LogDebug("Finding available agent with capability: {cap}", CapabilityId);

            var agents = await _dataContext.Agents
                .Where(a => a.ConnectionStatus == AgentConnectionState.Connected.ToString())
                .Where(a => a.AgentCapabilityIds.Contains(CapabilityId))
                .ToListAsync();

            if (agents == null || !agents.Any())
            {
                return ServiceResult<AgentModel>.Failure("No available agent found");
            }

            if (agents.Count > 1)
            {
                var randomNumber = RandomGenerators.GenerateRandomNumber(0, agents.Count);
                return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agents[randomNumber]));
            }
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agents.First()));
        }

        /// <summary>
        /// Gets any available agent that is currently connected.
        /// </summary>
        /// <returns>
        /// An <see cref="AgentModel"/> representing the available agent, or null if no agents are available.
        /// </returns>
        public async Task<ServiceResult<AgentModel>> GetAnyAvailableAgent()
        {
            _logger.LogDebug("Finding available agent");
            // should be improved to rotate agent selection when multiple are available
            var agents = await _dataContext.Agents
                .Where(a => a.ConnectionStatus == AgentConnectionState.Connected.ToString())
                .ToListAsync();

            if (agents == null || !agents.Any())
            {
                return ServiceResult<AgentModel>.Failure("No available agent found");
            }

            if (agents.Count > 1)
            {
                var randomNumber = RandomGenerators.GenerateRandomNumber(0, agents.Count);
                return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agents[randomNumber]));
            }
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agents.First()));
        }

        public async Task<ServiceResult<AgentModel>> UpdateAgentOnConnect(Guid AgentId, string ConnectionId)
        {
            _logger.LogDebug("Updating agent properties on connect for id: {id}", AgentId);
            var agentEntity = await _getAgentEntityById(AgentId);
            if (agentEntity is null)
            {
                return ServiceResult<AgentModel>.Failure($"Agent with id: {AgentId} not found!");
            }

            agentEntity.ConnectionStatus = AgentConnectionState.Connected.ToString();
            agentEntity.LastConnected = DateTime.UtcNow;
            agentEntity.ConnectionId = ConnectionId;
            await _dataContext.SaveChangesAsync();
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentEntity));
        }

        public async Task<ServiceResult<AgentModel>> GetAgentById(Guid AgentId)
        {
            _logger.LogDebug("Fetching agent by id: {id}", AgentId);
            var agentSearchResult = await _getAgentEntityById(AgentId);
            if (agentSearchResult == null)
            {
                return ServiceResult<AgentModel>.Failure("Agent not found");
            }
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentSearchResult));
        }

        public async Task<ServiceResult<AgentModel>> UpdateAgentOnDisconnect(Guid AgentId)
        {
            _logger.LogDebug("Updating agent properties on disconnect for id: {id}", AgentId);
            var agentEntity = await _getAgentEntityById(AgentId);
            if (agentEntity is null)
            {
                return ServiceResult<AgentModel>.Failure($"Agent with id: {AgentId} not found!");
            }
            agentEntity.ConnectionStatus = AgentConnectionState.Disconnected.ToString();
            await _dataContext.SaveChangesAsync();
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentEntity));
        }

        public async Task<ServiceResult<AgentModel>> UpdateAgentState(Guid AgentId, AgentConnectionState AgentConnectionState)
        {
            _logger.LogDebug("Updating agent state for agentid: {id} to {state}", AgentId, AgentConnectionState.ToString());
            var agentEntity = await _getAgentEntityById(AgentId);
            if (agentEntity is null)
            {
                return ServiceResult<AgentModel>.Failure($"Agent with id: {AgentId} not found!");
            }
            agentEntity.ConnectionStatus = AgentConnectionState.ToString();
            await _dataContext.SaveChangesAsync();
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentEntity));
        }

        public async Task<ServiceResult<AgentModel>> GetAgentByIdentityName(string IdentityName)
        {
            _logger.LogDebug("Find agent with identity name: {name}", IdentityName);
            if (IdentityName == null)
            {
                return ServiceResult<AgentModel>.Failure("Invalid identity");
            }
            var agent = await _dataContext.Agents.AsNoTracking().FirstOrDefaultAsync(a => a.IdentityAgentName == IdentityName);
            if (agent != null)
            {
                _logger.LogDebug("Found agent: {@agent}", agent);
                return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agent));
            }
            return ServiceResult<AgentModel>.Failure($"Agent with Identity: {IdentityName} not found");
        }

        public async Task<ServiceResult<AgentRegisteredModel>> RegisterAgent(string AgentName)
        {
            var newAgent = await _addAgentToDb(AgentName);
            if (!newAgent.IsSuccess)
            {
                return ServiceResult<AgentRegisteredModel>.Failure(newAgent.ErrorMessage);
            }

            var agentUser = await _userService.CreateAgentUser(newAgent.Value.IdentityAgentName);
            if (!agentUser.IsSuccess)
            {
                return ServiceResult<AgentRegisteredModel>.Failure(agentUser.ErrorMessage);
            }

            var tokenSet = await _tokenService.GenerateTokensAsync(agentUser.Value.Id, null, int.Parse(_configuration["JWT:AgentTokenExpirationMinutes"]), int.Parse(_configuration["JWT:AgentRefreshTokenExpirationMinutes"]));
            if (!tokenSet.IsSuccess)
            {
                return ServiceResult<AgentRegisteredModel>.Failure(tokenSet.ErrorMessage);
            }

            return ServiceResult<AgentRegisteredModel>.Success(new AgentRegisteredModel
            {
                AgentId = newAgent.Value.Id,
                AgentName = newAgent.Value.UserDefinedName,
                Token = tokenSet.Value.RefreshToken.Token
            });
        }

        public async Task<ServiceResult<AgentModel>> AssignCapabilityToAgent(Guid AgentId, Guid CapabilityId)
        {
            _logger.LogInformation("Assign capability: {capability} to agent: {agent}", CapabilityId, AgentId);

            var agentEntity = await _getAgentEntityById(AgentId) ?? throw new AppException("Agent not found");
            var agentCapabilityFindResult = await _agentCapabilityService.GetAgentCapabilityById(CapabilityId);
            if (!agentCapabilityFindResult.IsSuccess)
            {
                return ServiceResult<AgentModel>.Failure(agentCapabilityFindResult.ErrorMessage);
            }

            if (!agentEntity.AgentCapabilityIds?.Contains(CapabilityId) ?? true)
            {
                _logger.LogDebug("Agent does not have this capability yet, adding...");
                agentEntity.AgentCapabilityIds ??= new List<Guid>();
                agentEntity.AgentCapabilityIds.Add(CapabilityId);
                await _dataContext.SaveChangesAsync();
            }
            return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentEntity));
        }

        public async Task<ServiceResult<AgentModel>> RemoveCapabilityFromAgent(Guid AgentId, Guid CapabilityId)
        {
            _logger.LogInformation("Remove capability: {capability} from agent: {agent}", CapabilityId, AgentId);

            var agentEntity = await _getAgentEntityById(AgentId) ?? throw new AppException("Agent not found");
            if (!agentEntity.AgentCapabilityIds.Any())
            {
                return ServiceResult<AgentModel>.Failure("Agent has no capabilities assigned");
            }
            if (agentEntity.AgentCapabilityIds.Contains(CapabilityId))
            {
                var removed = agentEntity.AgentCapabilityIds.Remove(CapabilityId);
                if (removed)
                {
                    await _dataContext.SaveChangesAsync();
                }
                return ServiceResult<AgentModel>.Success(_mapper.Map<AgentModel>(agentEntity));
            }
            return ServiceResult<AgentModel>.Failure("Agent does not have specified capability");
        }

        private async Task<ServiceResult<AgentEntity>> _addAgentToDb(string AgentName)
        {
            var existingAgent = await _dataContext.Agents.AnyAsync(a => a.UserDefinedName == AgentName);
            if (existingAgent)
            {
                return ServiceResult<AgentEntity>.Failure($"Agent: {AgentName} already exists");
            }

            // if users already exists somehow, let put 4 random digits to username
            string identityAgentName = $"agent_{AgentName}";
            while ((await _userService.UserExists(identityAgentName, IdentityAuth.ApplicationUserType.Agent)).Value)
            {
                identityAgentName = $"agent_{AgentName}_{new Random().Next(1000, 10000)}";
            }
            var newAgent = new AgentEntity
            {
                UserDefinedName = AgentName,
                IdentityAgentName = identityAgentName
            };
            _dataContext.Agents.Add(newAgent);
            _logger.LogDebug("Adding new agent to db {@agent}", newAgent);
            await _dataContext.SaveChangesAsync();
            return ServiceResult<AgentEntity>.Success(newAgent);
        }

        private async Task<AgentEntity> _getAgentEntityById(Guid Id)
        {
            _logger.LogDebug("Get agent with id: {id}", Id);
            return await _dataContext.Agents.Include(a => a.AgentCapabilities).FirstOrDefaultAsync(a => a.Id == Id);
        }

        public async Task<ServiceResult<bool>> DeleteAgentById(Guid agentId)
        {
            // get agent
            _logger.LogDebug("Finding Agent entity");
            var agentEntity = await this._getAgentEntityById(agentId);
            if (agentEntity is null)
            {
                return ServiceResult<bool>.Failure("Agent not found");
            }
            // get user
            _logger.LogDebug("Finding Agent Identity");
            var agentIdentity = await _userService.GetUserByName(agentEntity.IdentityAgentName, IdentityAuth.ApplicationUserType.Agent);
            if (agentIdentity.IsFailure)
            {
                return ServiceResult<bool>.Failure("Agent identity not found");
            }

            // delete agent
            _logger.LogDebug("Deleting Agent entity");
            _dataContext.Agents.Remove(agentEntity);
            await _dataContext.SaveChangesAsync();

            // delete user
            _logger.LogDebug("Deleting Agent Identity");
            var deleteUserResult = await _userService.DeleteUser(agentIdentity.Value.UserName, IdentityAuth.ApplicationUserType.Agent);
            if (!deleteUserResult.IsSuccess)
            {
                return ServiceResult<bool>.Failure($"Error while deleting agent identity: {deleteUserResult.ErrorMessage}");
            }

            // kill any active connection for the agent
            try
            {
                var agentConnection = _hubContext.Clients.Client(agentEntity.ConnectionId);
                if (agentConnection is not null)
                {
                    _logger.LogDebug("Terminating agent connection: {connectionid}", agentEntity.ConnectionId);
                    await agentConnection.SendAsync(SignalRAgentMethods.TerminateConnectionMethod);
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Failed to terminate connection for agent: {connectionid}, error: {error}", agentEntity.ConnectionId, ex.Message);
            }
            return ServiceResult<bool>.Success(true);
        }
    }
}