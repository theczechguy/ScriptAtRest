﻿using System;

namespace ScriptAtRest.Server.Domains.AgentSrv.Models
{
    public class AgentRegisteredModel
    {
        public string AgentName { get; set; }
        public string Token { get; set; }
        public Guid AgentId { get; internal set; }
    }
}