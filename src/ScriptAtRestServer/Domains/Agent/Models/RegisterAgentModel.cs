﻿using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.AgentSrv.Models
{
    public class RegisterAgentModel
    {
        [Required]
        [RegularExpression(@"^\S*$", ErrorMessage = "Name must not contain spaces !")]
        public string AgentName { get; set; }
    }
}