﻿using AutoMapper;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv.Models;

namespace ScriptAtRest.Server.Domains.AgentCapabilitySrv
{
    public class AgentCapabilitiesAutoMapperProfile : Profile
    {
        public AgentCapabilitiesAutoMapperProfile()
        {
            CreateMap<AgentCapabilityModel, AgentCapabilityEntity>();
            CreateMap<AgentCapabilityEntity, AgentCapabilityModel>();
        }
    }
}