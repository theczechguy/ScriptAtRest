﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv.Models;
using ScriptAtRest.Server.Helpers;
using ScriptAtRestServer.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.AgentCapabilitySrv
{
    public interface IAgentCapabilityService
    {
        /// <summary>
        /// Creates a new agent capability with the specified name and description.
        /// </summary>
        /// <param name="CapabilityName">The name of the new capability.</param>
        /// <param name="Description">The description of the new capability.</param>
        /// <returns>The newly created agent capability.</returns>
        /// <exception cref="AppException">Thrown when an agent capability with the same name already exists.</exception>
        Task<ServiceResult<AgentCapabilityModel>> NewAgentCapability(string CapabilityName, string Description);

        /// <summary>
        /// Retrieves an agent capability by name.
        /// </summary>
        /// <param name="CapabilityName">The name of the agent capability to retrieve.</param>
        /// <returns>The agent capability with the specified name, or null if it is not found.</returns>
        Task<ServiceResult<AgentCapabilityModel>> GetAgentCapabilityByName(string CapabilityName);

        /// <summary>
        /// Retrieves an agent capability by name.
        /// </summary>
        /// <param name="CapabilityId">The name of the agent capability to retrieve.</param>
        /// <returns>The agent capability with the specified name, or null if it is not found.</returns>
        Task<ServiceResult<AgentCapabilityModel>> GetAgentCapabilityById(Guid CapabilityId);

        /// <summary>
        /// Removes an agent capability by name.
        /// </summary>
        /// <param name="CapabilityName">The name of the agent capability to remove.</param>
        /// <returns>A boolean indicating whether the capability was found and removed (true) or not found (false).</returns>
        Task<ServiceResult<bool>> RemoveAgentCapabilityByName(string CapabilityName);

        /// <summary>
        /// List all registered capabilities
        /// </summary>
        /// <returns>A list of registered capabilities</returns>
        Task<ServiceResult<List<AgentCapabilityModel>>> ListAllCapabilities();
    }

    public class AgentCapabilityService : IAgentCapabilityService
    {
        private readonly DataContext _dataContext;
        private readonly ILogger<AgentCapabilityService> _logger;
        private readonly IMapper _mapper;

        public AgentCapabilityService(DataContext dataContext, ILogger<AgentCapabilityService> logger, IMapper mapper)
        {
            _dataContext = dataContext;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AgentCapabilityModel>> NewAgentCapability(string CapabilityName, string Description)
        {
            _logger.LogInformation("Registering new agent capability: {name}", CapabilityName);
            var capabilityExists = await _getCapabilityByName(CapabilityName, false);
            if (capabilityExists != null)
            {
                return ServiceResult<AgentCapabilityModel>.Failure("Capability with specified name already exists");
            }
            var newCapability = _dataContext.AgentCapabilities.Add(new AgentCapabilityEntity
            {
                Description = Description,
                Name = CapabilityName
            });
            await _dataContext.SaveChangesAsync();
            return ServiceResult<AgentCapabilityModel>.Success(_mapper.Map<AgentCapabilityModel>(newCapability.Entity));
        }

        public async Task<ServiceResult<AgentCapabilityModel>> GetAgentCapabilityByName(string CapabilityName)
        {
            _logger.LogInformation("Get agent capability by name: {name}", CapabilityName);
            var capability = await _getCapabilityByName(CapabilityName, false);
            if (capability is null)
            {
                return ServiceResult<AgentCapabilityModel>.Failure("Capability not found");
            }
            return ServiceResult<AgentCapabilityModel>.Success(_mapper.Map<AgentCapabilityModel>(capability));
        }

        public async Task<ServiceResult<AgentCapabilityModel>> GetAgentCapabilityById(Guid CapabilityId)
        {
            _logger.LogInformation("Get agent capability by id: {name}", CapabilityId);
            var capability = await _getCapabilityById(CapabilityId, false);
            if (capability is null)
            {
                return ServiceResult<AgentCapabilityModel>.Failure("Capability not found");
            }
            return ServiceResult<AgentCapabilityModel>.Success(_mapper.Map<AgentCapabilityModel>(capability));
        }

        public async Task<ServiceResult<bool>> RemoveAgentCapabilityByName(string CapabilityName)
        {
            _logger.LogInformation("Remove agent capability by name: {name}", CapabilityName);
            var capabilityExists = await _getCapabilityByName(CapabilityName, true);
            if (capabilityExists == null)
            {
                _logger.LogInformation("Capability not found");
                return ServiceResult<bool>.Failure("Capability not found");
            }
            _dataContext.AgentCapabilities.Remove(capabilityExists);
            await _dataContext.SaveChangesAsync();
            return ServiceResult<bool>.Success(true);
        }

        /// <summary>
        /// Gets an agent capability by name from the database.
        /// </summary>
        /// <param name="CapabilityName">The name of the agent capability to retrieve.</param>
        /// <param name="trackEntity">A boolean indicating whether to track the entity or not. If true, the entity will be tracked by the EF DbContext.</param>
        /// <returns>The agent capability with the specified name, or null if it is not found.</returns>
        /// <remarks>
        /// If <paramref name="trackEntity"/> is set to false, the method will return the agent capability without tracking it in the EF DbContext. This can improve performance in scenarios where the entity is read-only and will not be modified.
        /// </remarks>
        private async Task<AgentCapabilityEntity> _getCapabilityByName(string CapabilityName, bool trackEntity)
        {
            _logger.LogDebug("Get agent capability by name: {name}", CapabilityName);
            var query = _dataContext.AgentCapabilities.AsQueryable();
            if (!trackEntity)
            {
                query = query.AsNoTracking();
            }
            var capability = await query.FirstOrDefaultAsync(c => c.Name == CapabilityName);
            if (capability == null)
            {
                _logger.LogDebug("Capability not found");
                return null;
            }
            _logger.LogDebug("Agent capability found: {@capability}", capability);
            return capability;
        }

        /// <summary>
        /// Gets an agent capability by name from the database.
        /// </summary>
        /// <param name="CapabilityId">The name of the agent capability to retrieve.</param>
        /// <param name="trackEntity">A boolean indicating whether to track the entity or not. If true, the entity will be tracked by the EF DbContext.</param>
        /// <returns>The agent capability with the specified name, or null if it is not found.</returns>
        /// <remarks>
        /// If <paramref name="trackEntity"/> is set to false, the method will return the agent capability without tracking it in the EF DbContext. This can improve performance in scenarios where the entity is read-only and will not be modified.
        /// </remarks>
        private async Task<AgentCapabilityEntity> _getCapabilityById(Guid CapabilityId, bool trackEntity)
        {
            _logger.LogDebug("Get agent capability by id: {id}", CapabilityId);
            var query = _dataContext.AgentCapabilities.AsQueryable();
            if (!trackEntity)
            {
                query = query.AsNoTracking();
            }
            var capability = await query.FirstOrDefaultAsync(c => c.Id == CapabilityId);
            if (capability == null)
            {
                _logger.LogDebug("Capability not found");
                return null;
            }
            _logger.LogDebug("Agent capability found: {@capability}", capability);
            return capability;
        }

        public async Task<ServiceResult<List<AgentCapabilityModel>>> ListAllCapabilities()
        {
            var capabilities = await _dataContext.AgentCapabilities.ToListAsync();
            if (capabilities is null)
            {
                return ServiceResult<List<AgentCapabilityModel>>.Failure("No capabilities found");
            }
            var mappedCapabilities = _mapper.Map<List<AgentCapabilityModel>>(capabilities);
            return ServiceResult<List<AgentCapabilityModel>>.Success(mappedCapabilities);
        }
    }
}