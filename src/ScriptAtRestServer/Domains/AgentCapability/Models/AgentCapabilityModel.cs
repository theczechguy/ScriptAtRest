﻿using System;

namespace ScriptAtRest.Server.Domains.AgentCapabilitySrv.Models
{
    public class AgentCapabilityModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}