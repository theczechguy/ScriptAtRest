﻿using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.AgentCapabilitySrv.Models
{
    public class RegisterAgentCapabilityModel
    {
        [Required]
        [RegularExpression(@"^\S*$", ErrorMessage = "Name must not contain spaces !")]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}