﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Domains.Agent;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.AgentInitializerSrv
{
    public class AgentInitializerService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<AgentInitializerService> _logger;

        public AgentInitializerService(ILogger<AgentInitializerService> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                DataContext dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                foreach (var agent in dataContext.Agents)
                {
                    _logger.LogWarning("Invalidating state of agent after app restart : {agent}", agent.IdentityAgentName);
                    agent.ConnectionStatus = AgentConnectionState.InvalidatedByServerAfterRestart.ToString();
                }
                await dataContext.SaveChangesAsync(cancellationToken);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}