using System;
using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class RegisterScriptModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        public string EncodedContent { get; set; }

        [Required]
        [Range(1, 3600, ErrorMessage = "Value must be between {1} and {2}")]
        public int Timeout { get; set; }
    }
}