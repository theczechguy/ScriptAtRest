﻿using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class RegisterScriptTypeModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Runner { get; set; }

        [Required]
        public string FileExtension { get; set; }

        public string ScriptArgument { get; set; }
    }
}