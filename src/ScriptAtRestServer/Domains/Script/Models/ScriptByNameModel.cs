﻿namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class ScriptByNameModel
    {
        public string Name { get; set; }
    }
}