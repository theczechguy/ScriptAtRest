using ScriptAtRest.Server.Database.Entities;
using System;
using System.Collections.Generic;

namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class ScriptModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptTypeEntity ScriptType { get; set; }
        public int Timeout { get; set; }
        public DateTime LastModifiedUtc { get; set; }
        public virtual ICollection<ScriptHistoryEntity> ScriptHistory { get; set; }
    }
}