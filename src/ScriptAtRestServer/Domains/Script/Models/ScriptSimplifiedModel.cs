﻿namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class ScriptSimplifiedModel
    {
        public int id { get; set; }
        public string Name { get; set; }
    }
}