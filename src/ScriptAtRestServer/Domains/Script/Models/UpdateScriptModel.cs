﻿namespace ScriptAtRest.Server.Domains.Script.Models
{
    public class UpdateScriptModel
    {
        public string Name { get; set; }

        public int Type { get; set; }

        public string EncodedContent { get; set; }

        public int Timeout { get; set; }
    }
}