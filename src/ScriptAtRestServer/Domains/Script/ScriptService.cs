﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.Script.Models;
using ScriptAtRest.Server.Helpers;
using ScriptAtRestServer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.Script
{
    public interface IScriptService
    {
        Task<ServiceResult<ScriptModel>> CreateAsync(RegisterScriptModel Model);

        Task<ServiceResult<ScriptTypeModel>> CreateTypeAsync(string Name, string Runner, string FileExtension, string ScriptArgument);

        Task<ServiceResult<bool>> DeleteAsync(int id);

        Task<ServiceResult<bool>> DeleteTypeAsync(int Id);

        IEnumerable<ScriptEntity> GetAll();

        IEnumerable<ScriptTypeEntity> GetAllTypes();

        Task<ServiceResult<ScriptEntity>> GetByIdAsync(int Id);

        Task<ServiceResult<ScriptEntity>> GetScriptByNameAsync(string name);

        Task<ServiceResult<ScriptTypeEntity>> GetTypeByIdAsync(int Id);

        Task<ServiceResult<ScriptTypeEntity>> UpdateTypeById(int Id, ScriptTypeEntity UpdatedType);

        Task<ServiceResult<ScriptEntity>> UpdateScriptByIdAsync(int Id, UpdateScriptModel UpdatedScript);
    }

    public class ScriptService : IScriptService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ScriptService(DataContext Context, IMapper mapper)
        {
            _context = Context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<ScriptModel>> CreateAsync(RegisterScriptModel Model)
        {
            if (await _context.Scripts.AnyAsync(x => x.Name == Model.Name))
            {
                return ServiceResult<ScriptModel>.Failure("Scriptname is already taken");
            }

            ScriptTypeEntity scriptType = await _context.ScriptTypes.FindAsync(Model.Type);
            if (scriptType is null)
            {
                return ServiceResult<ScriptModel>.Failure("Script type not found");
            }
            var modified = DateTime.UtcNow;
            var newScript = new ScriptEntity
            {
                EncodedContent = Model.EncodedContent,
                Name = Model.Name,
                Timeout = Model.Timeout,
                ScriptType = scriptType,
                LastModifiedUtc = modified,
            };

            _ = _context.Scripts.Add(newScript);
            _ = await _context.SaveChangesAsync();

            newScript.ScriptHistory = new List<ScriptHistoryEntity>
            {
                new ScriptHistoryEntity
                {
                    EncodedContent = Model.EncodedContent,
                    Name = Model.Name,
                    Timeout = Model.Timeout,
                    ModifiedUtc = modified,
                    ScriptType = scriptType,
                }
            };

            await _context.SaveChangesAsync();

            return ServiceResult<ScriptModel>.Success(_mapper.Map<ScriptModel>(newScript));
        }

        public async Task<ServiceResult<ScriptTypeModel>> CreateTypeAsync(string Name, string Runner, string FileExtension, string ScriptArgument)
        {
            if (await _context.ScriptTypes.AnyAsync(x => x.Name == Name))
            {
                return ServiceResult<ScriptTypeModel>.Failure("Script type name is already taken");
            }

            var newScriptType = new ScriptTypeEntity
            {
                FileExtension = FileExtension,
                ScriptArgument = ScriptArgument,
                Name = Name,
                Runner = Runner
            };

            _context.ScriptTypes.Add(newScriptType);
            _ = await _context.SaveChangesAsync();

            return ServiceResult<ScriptTypeModel>.Success(_mapper.Map<ScriptTypeModel>(newScriptType));
        }

        public async Task<ServiceResult<bool>> DeleteAsync(int id)
        {
            ScriptEntity script = await _context.Scripts.FindAsync(id);
            if (script is null)
            {
                return ServiceResult<bool>.Failure("Script not found");
            }
            _ = _context.Scripts.Remove(script);
            _ = await _context.SaveChangesAsync();
            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<bool>> DeleteTypeAsync(int Id)
        {
            ScriptTypeEntity type = await _context.ScriptTypes.FindAsync(Id);
            if (type is null)
            {
                return ServiceResult<bool>.Failure("Script type not found");
            }
            _ = _context.ScriptTypes.Remove(type);
            _ = await _context.SaveChangesAsync();
            return ServiceResult<bool>.Success(true);
        }

        public IEnumerable<ScriptEntity> GetAll() => _context.Scripts;

        public IEnumerable<ScriptTypeEntity> GetAllTypes()
        {
            return _context.ScriptTypes;
        }

        public async Task<ServiceResult<ScriptEntity>> GetByIdAsync(int Id)
        {
            ScriptEntity script = await _context.Scripts
                .Include(x => x.ScriptHistory)
                .Include(z => z.ScriptType)
                .FirstOrDefaultAsync(y => y.Id == Id);
            if (script is null)
            {
                return ServiceResult<ScriptEntity>.Failure("Script not found");
            }
            return ServiceResult<ScriptEntity>.Success(script);
        }

        public async Task<ServiceResult<ScriptEntity>> GetScriptByNameAsync(string name)
        {
            ScriptEntity script = await _context.Scripts
                .Include(x => x.ScriptHistory)
                .Include(z => z.ScriptType)
                .Where(s => s.Name == name)
                .SingleOrDefaultAsync() ?? throw new AppException($"Script with name {name} not found !");
            return ServiceResult<ScriptEntity>.Success(script);
        }

        public async Task<ServiceResult<ScriptTypeEntity>> GetTypeByIdAsync(int Id)
        {
            ScriptTypeEntity type = await _context.ScriptTypes.FindAsync(Id);
            if (type is null)
            {
                return ServiceResult<ScriptTypeEntity>.Failure("Type not found");
            }
            return ServiceResult<ScriptTypeEntity>.Success(type);
        }

        public async Task<ServiceResult<ScriptEntity>> UpdateScriptByIdAsync(int Id, UpdateScriptModel UpdatedScript)
        {
            bool scriptHasChanged = false;
            ScriptEntity current = await _context.Scripts
                .Include(x => x.ScriptType)
                .Include(x => x.ScriptHistory)
                .FirstOrDefaultAsync(s => s.Id == Id);
            if (current is null)
            {
                return ServiceResult<ScriptEntity>.Failure($"Script with id {Id} not found");
            }

            if (!string.IsNullOrWhiteSpace(UpdatedScript.EncodedContent) && current.EncodedContent != UpdatedScript.EncodedContent)
            {
                scriptHasChanged = true;
                current.EncodedContent = UpdatedScript.EncodedContent;
            }

            if (!string.IsNullOrWhiteSpace(UpdatedScript.Name) && current.Name != UpdatedScript.Name)
            {
                if (await _context.Scripts.AnyAsync(x => x.Name == UpdatedScript.Name))
                {
                    return ServiceResult<ScriptEntity>.Failure("New script name is already taken");
                }
                scriptHasChanged = true;
                current.Name = UpdatedScript.Name;
            }

            if (!(UpdatedScript.Type == 0) && current.ScriptType.Id != UpdatedScript.Type)
            {
                scriptHasChanged = true;
                ScriptTypeEntity newType = await _context.ScriptTypes.FirstOrDefaultAsync(x => x.Id == UpdatedScript.Type);
                current.ScriptType = newType;
            }

            if (!(UpdatedScript.Timeout == 0) && current.Timeout != UpdatedScript.Timeout)
            {
                scriptHasChanged = true;
                current.Timeout = UpdatedScript.Timeout;
            }

            if (scriptHasChanged)
            {
                current.LastModifiedUtc = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                current.ScriptHistory ??= new List<ScriptHistoryEntity>();
                current.ScriptHistory.Add(new ScriptHistoryEntity
                {
                    EncodedContent = current.EncodedContent,
                    Name = current.Name,
                    Timeout = current.Timeout,
                    ModifiedUtc = current.LastModifiedUtc,
                    ScriptType = current.ScriptType
                });

                _context.Scripts.Update(current);
                _ = await _context.SaveChangesAsync();
                return ServiceResult<ScriptEntity>.Success(current);
            }
            else
            {
                return ServiceResult<ScriptEntity>.Failure("No changes were detected");
            }
        }

        public async Task<ServiceResult<ScriptTypeEntity>> UpdateTypeById(int Id, ScriptTypeEntity UpdatedType)
        {
            bool typeHasChanged = false;

            ScriptTypeEntity currentType = await _context.ScriptTypes.FindAsync(Id);
            if (currentType is null)
            {
                return ServiceResult<ScriptTypeEntity>.Failure($"Script type with id {Id} not found");
            }

            if (!string.IsNullOrWhiteSpace(UpdatedType.FileExtension) && currentType.FileExtension != UpdatedType.FileExtension)
            {
                typeHasChanged = true;
                currentType.FileExtension = UpdatedType.FileExtension;
            }

            if (!string.IsNullOrWhiteSpace(UpdatedType.Runner) && currentType.Runner != UpdatedType.Runner)
            {
                typeHasChanged = true;
                currentType.Runner = UpdatedType.Runner;
            }

            if (!string.IsNullOrWhiteSpace(UpdatedType.ScriptArgument) && currentType.ScriptArgument != UpdatedType.ScriptArgument)
            {
                typeHasChanged = true;
                currentType.ScriptArgument = UpdatedType.ScriptArgument;
            }

            if (!string.IsNullOrWhiteSpace(UpdatedType.Name) && currentType.Name != UpdatedType.Name)
            {
                if (await _context.ScriptTypes.AnyAsync(x => x.Name == UpdatedType.Name))
                {
                    return ServiceResult<ScriptTypeEntity>.Failure("New script type name is already taken !");
                }
                typeHasChanged = true;
                currentType.Name = UpdatedType.Name;
            }

            if (typeHasChanged)
            {
                _context.ScriptTypes.Update(currentType);
                _ = await _context.SaveChangesAsync();
                return ServiceResult<ScriptTypeEntity>.Success(currentType);
            }
            else
            {
                return ServiceResult<ScriptTypeEntity>.Failure("No changes detected");
            }
        }
    }
}