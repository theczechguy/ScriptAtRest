﻿using ScriptAtRest.Shared.Scripts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.ScriptScheduling.Models
{
    public class ScheduleScriptJobModel
    {
        [Required]
        public List<ScriptParamModel> ScriptParamModel { get; set; }

        public string RequiredCapabilityName { get; set; }

        [Required]
        public int ScriptId { get; set; }
    }
}