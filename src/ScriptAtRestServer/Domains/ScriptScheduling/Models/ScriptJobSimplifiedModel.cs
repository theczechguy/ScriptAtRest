﻿using System;

namespace ScriptAtRest.Server.Domains.ScriptScheduling.Models
{
    public class ScriptJobSimplifiedModel
    {
        public string Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string JobStatus { get; set; }
    }
}