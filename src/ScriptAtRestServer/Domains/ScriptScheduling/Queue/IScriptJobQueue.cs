﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.ScriptScheduling.Queue
{
    public interface IScriptJobQueue
    {
        ValueTask QueueAsync(Guid item);

        ValueTask<Guid> DequeueAsync(CancellationToken cancellationToken);
    }
}