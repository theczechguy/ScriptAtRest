﻿using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.ScriptScheduling.Queue
{
    public class ScriptJobQueue : IScriptJobQueue
    {
        private readonly Channel<Guid> _queue;

        public ScriptJobQueue()
        {
            BoundedChannelOptions options = new(50)
            {
                FullMode = BoundedChannelFullMode.Wait
            };
            _queue = Channel.CreateBounded<Guid>(options);
        }

        public async ValueTask<Guid> DequeueAsync(CancellationToken cancellationToken)
        {
            return await _queue.Reader.ReadAsync(cancellationToken);
        }

        public async ValueTask QueueAsync(Guid scriptJobId)
        {
            if (scriptJobId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(scriptJobId));
            }
            await _queue.Writer.WriteAsync(scriptJobId);
        }
    }
}