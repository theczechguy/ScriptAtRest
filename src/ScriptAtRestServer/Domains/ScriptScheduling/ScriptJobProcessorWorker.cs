﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.AgentSrv;
using ScriptAtRest.Server.Domains.AgentSrv.Models;
using ScriptAtRest.Server.Domains.ScriptScheduling.Queue;
using ScriptAtRest.Server.Helpers;
using ScriptAtRest.Shared.Scripts;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.ScriptScheduling
{
    public class ScriptJobProcessorWorker : BackgroundService
    {
        private readonly IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> _hubContext;
        private readonly ILogger<ScriptJobProcessorWorker> _logger;
        private readonly IServiceProvider _serviceProvider;

        public ScriptJobProcessorWorker(ILogger<ScriptJobProcessorWorker> logger,
                                         IServiceProvider serviceProvider,
                                         IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> hubContext)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
            _hubContext = hubContext;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    using (IServiceScope scope = _serviceProvider.CreateScope())
                    {
                        DataContext dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                        IAgentService agentService = scope.ServiceProvider.GetRequiredService<IAgentService>();
                        IScriptJobQueue scriptJobQueue = scope.ServiceProvider.GetRequiredService<IScriptJobQueue>();

                        _logger.LogDebug("[JobProcessor] Waiting for scriptjob item in queue");
                        Guid? workItem =
                            await scriptJobQueue.DequeueAsync(stoppingToken);
                        _logger.LogDebug("[JobProcessor] Dequeued job from queue");

                        var scriptJob = await dataContext.ScriptJobs.Where(j => j.Id == workItem).FirstOrDefaultAsync();
                        if (scriptJob == null)
                        {
                            _logger.LogWarning("[JobProcessor] Unable to find scriptjob : {ScriptJobId}", scriptJob.Id);
                            continue;
                        }
                        _logger.LogInformation("[JobProcessor] Processing scriptjob: {ScriptJobId}", scriptJob.Id);
                        _logger.LogDebug("[JobProcessor] Identifying suitable agent for script job {ScriptJobId}", scriptJob.Id);
                        var agentResult = await _getAgentForScriptJob(scriptJob.AgentCapabilityId, agentService);
                        if (agentResult.IsFailure)
                        {
                            _logger.LogWarning("[JobProcessor] No available agent found for job {ScriptJobId}", scriptJob.Id);
                            var newJobState = ScriptJobStates.NoAgentAvailable;
                            if (scriptJob.RetryCount >= 4)
                            {
                                newJobState = ScriptJobStates.TooManyRetries;
                            }
                            await UpdateScriptJobStatus(scriptJob, newJobState, dataContext, stoppingToken);
                            continue;
                        }

                        // if agent found send scriptjob to agent
                        _logger.LogInformation("[JobProcessor] Sending {method} request to agent: {AgentName} | connectionid: {AgentConnectionId}",
                                                SignalRAgentMethods.RunScriptMethod,
                                                agentResult.Value.UserDefinedName,
                                                agentResult.Value.ConnectionId);
                        var scriptRequestData = JsonSerializer.Deserialize<ScriptRequestData>(scriptJob.ScriptRequestData);
                        // this should be wrapped and failure of sending request handled properly
                        await _hubContext.Clients.Client(agentResult.Value.ConnectionId)
                                                    .SendAsync(SignalRAgentMethods.RunScriptMethod, scriptRequestData);

                        await UpdateScriptJobWithAgent(
                            scriptJob,
                            agentResult.Value.Id,
                            ScriptJobStates.SentToAgent,
                            dataContext,
                            stoppingToken);
                    }
                }
            }
            catch (OperationCanceledException)
            {
                // Prevent throwing if stoppingToken was signaled
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "[JobProcessor] Error occurred processing script job");
            }
        }

        /// <summary>
        /// Identifies a suitable agent for the specified script entity based on its agent capability.
        /// If script requires agent with capability it will look only for those with matching capability
        /// If there is not requirement for agept capability then any connected agent is randomly selected
        /// </summary>
        /// <param name="CapabilityId">Optional Capability ID</param>
        /// <param name="agentService">Agent service</param>
        /// <returns>An agent model representing the suitable agent, or null if no suitable agent is found.</returns>
        private async Task<ServiceResult<AgentModel>> _getAgentForScriptJob(Guid CapabilityId, IAgentService agentService)
        {
            if (CapabilityId == Guid.Empty)
            {
                _logger.LogDebug("[JobProcessor] Script job does not require any specific capability");
                return await agentService.GetAnyAvailableAgent();
            }
            _logger.LogDebug("[JobProcessor] Script job requires agent with specific capability: {cap}", CapabilityId);
            return await agentService.GetAvailableAgentWithCapabilityId(CapabilityId);
        }

        private async Task UpdateScriptJobStatus(ScriptJobEntity scriptjob, string status, DataContext dataContext, CancellationToken stoppingToken)
        {
            scriptjob.JobStatus = status;
            scriptjob.LastUpdatedOn = DateTime.UtcNow;
            scriptjob.RetryCount = scriptjob.RetryCount + 1;
            await dataContext.SaveChangesAsync(stoppingToken);
        }

        private async Task UpdateScriptJobWithAgent(ScriptJobEntity scriptjob, Guid agentId, string status, DataContext dataContext, CancellationToken stoppingToken)
        {
            scriptjob.JobStatus = status;
            scriptjob.AssignedAgentId = agentId;
            scriptjob.LastUpdatedOn = DateTime.UtcNow;
            await dataContext.SaveChangesAsync(stoppingToken);
        }
    }
}