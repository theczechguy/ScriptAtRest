﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.ScriptScheduling.Queue;
using ScriptAtRest.Shared.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.ScriptScheduling
{
    public class ScriptJobRerunService : BackgroundService
    {
        private readonly ILogger<ScriptJobProcessorWorker> _logger;
        private readonly IServiceProvider _serviceProvider;

        public ScriptJobRerunService(ILogger<ScriptJobProcessorWorker> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using (IServiceScope scope = _serviceProvider.CreateScope())
                    {
                        DataContext dataContext = scope.ServiceProvider.GetService<DataContext>();
                        IScriptJobQueue scriptJobQueue = scope.ServiceProvider.GetRequiredService<IScriptJobQueue>();

                        // search for jobs in serverqueu or jobs where no agent available was found where last attempt was performed no sooner than 10 seconds before and retry count is less than 5
                        List<ScriptJobEntity> scriptJobs = await dataContext.ScriptJobs
                            .Where(j =>
                                j.JobStatus == ScriptJobStates.NoAgentAvailable
                                && j.LastUpdatedOn <= DateTime.UtcNow.AddSeconds(-10))
                            .OrderBy(j => j.CreatedOn)
                            .ThenBy(j => j.LastUpdatedOn)
                            .Take(10)
                            .ToListAsync(cancellationToken: stoppingToken);

                        foreach (var job in scriptJobs)
                        {
                            _logger.LogInformation("Resubmiting scriptjob: {ScriptJobId}", job.Id);
                            await scriptJobQueue.QueueAsync(job.Id);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Fatal failure in script job rerun service");
                }
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}