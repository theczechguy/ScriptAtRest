﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv;
using ScriptAtRest.Server.Domains.AgentSrv;
using ScriptAtRest.Server.Domains.Script;
using ScriptAtRest.Server.Domains.ScriptScheduling.Models;
using ScriptAtRest.Server.Domains.ScriptScheduling.Queue;
using ScriptAtRest.Server.Helpers;
using ScriptAtRest.Shared.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.ScriptSchedulingSrv
{
    public interface IScriptSchedulingService
    {
        Task<ServiceResult<ScriptJobModel>> ScheduleScriptRun(int ScriptId, List<ScriptParamModel> scriptParamModels, string requiredCapabilityName);

        Task<ServiceResult<ScriptJobModel>> GetScriptJobById(Guid ScriptJobId);

        Task<ServiceResult<List<ScriptJobModel>>> ListRecentJobs();
    }

    public class ScriptSchedulingService : IScriptSchedulingService
    {
        private readonly ILogger<ScriptSchedulingService> _logger;
        private readonly DataContext _dataContext;
        private readonly IScriptService _scriptService;
        private readonly IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> _hubContext;
        private readonly IAgentService _agentService;
        private readonly IMapper _mapper;
        private readonly IAgentCapabilityService _agentCapabilityService;
        private readonly IServiceProvider _serviceProvider;

        public ScriptSchedulingService(
            ILogger<ScriptSchedulingService> logger,
            DataContext dataContext,
            IScriptService scriptService,
            IHubContext<ScriptAtRestServer.SignalRHub.AgentHub> hubContext,
            IAgentService agentService,
            IMapper mapper,
            IAgentCapabilityService agentCapabilityService,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _dataContext = dataContext;
            _scriptService = scriptService;
            _hubContext = hubContext;
            _agentService = agentService;
            _mapper = mapper;
            _agentCapabilityService = agentCapabilityService;
            _serviceProvider = serviceProvider;
        }

        public async Task<ServiceResult<ScriptJobModel>> GetScriptJobById(Guid ScriptJobId)
        {
            var scriptJob = await _dataContext.ScriptJobs.Include(s => s.Script).FirstOrDefaultAsync(s => s.Id == ScriptJobId);
            if (scriptJob is null)
            {
                return ServiceResult<ScriptJobModel>.Failure("Scriptjob not found");
            }
            return ServiceResult<ScriptJobModel>.Success(_mapper.Map<ScriptJobModel>(scriptJob));
        }

        public async Task<ServiceResult<ScriptJobModel>> ScheduleScriptRun(int ScriptId, List<ScriptParamModel> scriptParamModels, string requiredCapabilityName)
        {
            // get script
            var script = await _scriptService.GetByIdAsync(ScriptId);
            if (script.IsFailure)
            {
                return ServiceResult<ScriptJobModel>.Failure(script.ErrorMessage);
            }

            Guid agentCapabilityId = Guid.Empty;
            if (!string.IsNullOrWhiteSpace(requiredCapabilityName))
            {
                var capabilityResult = await this._getAgentCapabilityByName(requiredCapabilityName);
                if (capabilityResult.IsSuccess)
                {
                    agentCapabilityId = capabilityResult.Value;
                }
                else
                {
                    return ServiceResult<ScriptJobModel>.Failure(capabilityResult.ErrorMessage);
                }
            }

            //create scriptjob
            ScriptJobEntity serverScriptJob = await _createScriptJob(script.Value, agentCapabilityId);

            var scriptDataTransferObject = new ScriptRequestData
            {
                ScriptEncodedContent = script.Value.EncodedContent,
                ScriptArguments = script.Value.ScriptType.ScriptArgument,
                ScriptFileExtension = script.Value.ScriptType.FileExtension,
                ScriptExecutable = script.Value.ScriptType.Runner,
                ScriptTimeoutSeconds = script.Value.Timeout,
                ServerScriptJobId = serverScriptJob.Id,
                ShouldDeleteFileAfterExecution = true,
                ScriptParamModel = scriptParamModels
            };
            serverScriptJob.ScriptRequestData = JsonSerializer.Serialize(scriptDataTransferObject);
            await _dataContext.SaveChangesAsync();

            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                _logger.LogDebug("Adding new job {ScriptJobId} to queue", serverScriptJob.Id);
                IScriptJobQueue scriptJobQueue = scope.ServiceProvider.GetRequiredService<IScriptJobQueue>();
                await scriptJobQueue.QueueAsync(serverScriptJob.Id);
            }

            return ServiceResult<ScriptJobModel>.Success(_mapper.Map<ScriptJobModel>(serverScriptJob));
        }

        public async Task<ServiceResult<List<ScriptJobModel>>> ListRecentJobs()
        {
            var jobs = await _dataContext.ScriptJobs.Include(s => s.Script).OrderByDescending(j => j.CreatedOn).Take(20).ToListAsync();
            if (jobs.Any())
            {
                return ServiceResult<List<ScriptJobModel>>.Success(_mapper.Map<List<ScriptJobModel>>(jobs));
            }
            return ServiceResult<List<ScriptJobModel>>.Failure("No jobs found");
        }

        private async Task<ScriptJobEntity> _createScriptJob(ScriptEntity Script, Guid AgentCapabilityId)
        {
            var entityEntry = _dataContext.ScriptJobs.Add(new ScriptJobEntity
            {
                JobStatus = ScriptJobStates.InServerQueue,
                Script = Script,
                AgentCapabilityId = AgentCapabilityId
            });
            await _dataContext.SaveChangesAsync();
            return entityEntry.Entity;
        }

        private async Task<ServiceResult<Guid>> _getAgentCapabilityByName(string capabilityName)
        {
            var result = await _agentCapabilityService.GetAgentCapabilityByName(capabilityName);
            if (result.IsFailure)
            {
                return ServiceResult<Guid>.Failure(result.ErrorMessage);
            }
            return ServiceResult<Guid>.Success(result.Value.Id);
        }
    }
}