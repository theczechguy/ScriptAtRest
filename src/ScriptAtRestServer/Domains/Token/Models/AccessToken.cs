﻿using System;

namespace ScriptAtRest.Server.Domains.Token.Models
{
    public class AccessToken
    {
        public string Token { get; set; }
        public DateTime ExpiryOn { get; set; }
    }
}