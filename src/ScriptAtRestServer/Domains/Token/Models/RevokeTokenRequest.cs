﻿namespace ScriptAtRest.Server.Domains.Token.Models
{
    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }
}