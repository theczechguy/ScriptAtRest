﻿using ScriptAtRest.Server.Database.Entities;

namespace ScriptAtRest.Server.Domains.Token.Models
{
    public class TokenResult
    {
        public RefreshTokenEntity RefreshToken { get; set; }
        public AccessToken Token { get; set; }
    }
}