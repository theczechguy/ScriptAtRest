﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.Token.Models;
using ScriptAtRest.Server.Helpers;
using ScriptAtRestServer.IdentityAuth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.Token
{
    public interface ITokenService
    {
        /// <summary>
        /// Generates access and refresh tokens for the specified <paramref name="AppUser"/> and sets the refresh token as an HttpOnly cookie in the <paramref name="HttpContext"/> response.
        /// </summary>
        /// <param name="AppUser">The application user for whom to generate tokens.</param>
        /// <param name="HttpContext">The HTTP context.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the generated tokens.</returns>
        Task<ServiceResult<TokenResult>> GenerateTokensAsync(String UserId, HttpContext HttpContext, int AccessTokenExpirationMinutes, int RefreshTokenExpirationMinutes);

        /// <summary>
        /// Retrieves a valid refresh token based on the specified <paramref name="Token"/> and <paramref name="User"/>.
        /// </summary>
        /// <param name="Token">The refresh token to validate.</param>
        /// <param name="User">The associated user.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the valid refresh token.</returns>
        Task<ServiceResult<RefreshTokenEntity>> GetValidRefreshToken(string Token, ApplicationUser User);

        /// <summary>
        /// Revokes the specified <paramref name="RefreshToken"/> for the given <paramref name="User"/> and logs the revocation IP.
        /// </summary>
        /// <param name="RefreshToken">The refresh token to revoke.</param>
        /// <param name="User">The associated user.</param>
        /// <param name="HttpContext">The HTTP context.</param>
        /// <returns>A task that represents the asynchronous operation. The task result indicates whether the revocation was successful.</returns>
        Task<ServiceResult<bool>> RevokeRefreshTokenAsync(RefreshTokenEntity RefreshToken, ApplicationUser User, HttpContext HttpContext);

        /// <summary>
        /// Revokes all refresh tokens associated with the specified <paramref name="User"/> and logs the revocation IP.
        /// </summary>
        /// <param name="User">The user for whom to revoke refresh tokens.</param>
        /// <param name="HttpContext">The HTTP context.</param>
        /// <returns>A task that represents the asynchronous operation. The task result indicates whether the revocation was successful.</returns>
        Task<ServiceResult<bool>> RevokeAllRefreshTokensAsync(ApplicationUser User, HttpContext HttpContext);

        /// <summary>
        /// Validates the provided <paramref name="Username"/> and <paramref name="Password"/> and returns the corresponding application user.
        /// </summary>
        /// <param name="Username">The username to validate.</param>
        /// <param name="Password">The password to validate.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the validated application user.</returns>
        Task<ServiceResult<ApplicationUser>> ValidateUser(string Username, string Password);

        Task<ServiceResult<ApplicationUser>> IsProvidedRefreshTokenValid(string RefreshToken);
    }

    public class TokenService : ITokenService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<TokenService> _logger;
        protected readonly IConfiguration _configuration;
        private readonly DataContext _context;

        public TokenService(
            UserManager<ApplicationUser> UserManager,
            ILogger<TokenService> Logger,
            IConfiguration Configuration,
            DataContext Context
            )
        {
            _userManager = UserManager;
            _logger = Logger;
            _configuration = Configuration;
            _context = Context;
        }

        //public async Task<ServiceResult<TokenResult>> GenerateTokensAsync(ApplicationUser AppUser, HttpContext HttpContext)
        //{
        //    AccessToken accessToken = await _generateAccessTokenAsync(AppUser);

        //    string ipAddress = HttpContext?.Connection.RemoteIpAddress?.ToString() ?? "127.0.0.1";
        //    RefreshTokenEntity refreshToken = _generateRefreshToken(AppUser.Id, AppUser.UserName, ipAddress);

        //    if (HttpContext != null)
        //    {
        //        // Set Refresh Token Cookie
        //        var cookieOptions = new CookieOptions
        //        {
        //            HttpOnly = true,
        //            Expires = DateTime.UtcNow.AddDays(1)
        //        };
        //        HttpContext.Response.Cookies.Append("refreshToken", refreshToken.Token, cookieOptions);
        //    }

        //    // Save refresh token to database
        //    AppUser.RefreshTokens ??= new List<RefreshTokenEntity>();

        //    AppUser.RefreshTokens.Add(refreshToken);
        //    await _userManager.UpdateAsync(AppUser);
        //    return ServiceResult<TokenResult>.Success(new TokenResult { Token = accessToken, RefreshToken = refreshToken });
        //}

        public async Task<ServiceResult<TokenResult>> GenerateTokensAsync(String UserId, HttpContext HttpContext, int AccessTokenExpirationMinutes, int RefreshTokenExpirationMinutes)
        {
            var appUser = await _userManager.FindByIdAsync(UserId.ToString());
            if (appUser == null)
            {
                return ServiceResult<TokenResult>.Failure("Identity not found");
            }
            AccessToken accessToken = await _generateAccessTokenAsync(appUser, AccessTokenExpirationMinutes);

            string ipAddress = HttpContext?.Connection.RemoteIpAddress?.ToString() ?? "127.0.0.1";
            RefreshTokenEntity refreshToken = _generateRefreshToken(appUser.Id, appUser.UserName, ipAddress, RefreshTokenExpirationMinutes);

            if (HttpContext != null)
            {
                // Set Refresh Token Cookie
                var cookieOptions = new CookieOptions
                {
                    HttpOnly = true,
                    Expires = DateTime.UtcNow.AddDays(1)
                };
                HttpContext.Response.Cookies.Append("refreshToken", refreshToken.Token, cookieOptions);
            }

            // Save refresh token to database
            appUser.RefreshTokens ??= new List<RefreshTokenEntity>();

            appUser.RefreshTokens.Add(refreshToken);
            await _userManager.UpdateAsync(appUser);
            _logger.LogDebug("Access token expiration: {expiration}", accessToken.ExpiryOn);
            _logger.LogDebug("Refresh token expiration: {expiration}", refreshToken.ExpiryOn);
            return ServiceResult<TokenResult>.Success(new TokenResult { Token = accessToken, RefreshToken = refreshToken });
        }

        public async Task<ServiceResult<RefreshTokenEntity>> GetValidRefreshToken(string Token, ApplicationUser User)
        {
            if (User == null)
            {
                return ServiceResult<RefreshTokenEntity>.Failure("User not specified");
            }
            if (User.RefreshTokens == null)
            {
                return ServiceResult<RefreshTokenEntity>.Failure("User not specified");
            }
            RefreshTokenEntity existingToken = User.RefreshTokens.FirstOrDefault(x => x.Token == Token);
            if (existingToken == null)
            {
                return ServiceResult<RefreshTokenEntity>.Failure("Token not found");
            }
            if (IsRefreshTokenValid(existingToken))
            {
                return ServiceResult<RefreshTokenEntity>.Success(existingToken);
            }
            return ServiceResult<RefreshTokenEntity>.Failure("Token is invalid");
        }

        public async Task<ServiceResult<bool>> RevokeRefreshTokenAsync(RefreshTokenEntity RefreshToken, ApplicationUser User, HttpContext HttpContext)
        {
            if (RefreshToken.RevokedByIp is not null)
            {
                return ServiceResult<bool>.Success(true);
            }

            string ipAddress = HttpContext?.Connection.RemoteIpAddress?.ToString() ?? "127.0.0.1";
            RefreshToken.RevokedByIp = ipAddress;
            RefreshToken.RevokedOn = DateTime.UtcNow;
            _ = _context.Update(User);
            await _context.SaveChangesAsync();
            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<bool>> RevokeAllRefreshTokensAsync(ApplicationUser User, HttpContext HttpContext)
        {
            User.RefreshTokens = await _context.RefreshTokens
                .Where(t => t.UserId == User.Id && t.RevokedByIp == null).ToListAsync();

            if (User.RefreshTokens == null)
            {
                return ServiceResult<bool>.Failure("User has not refresh tokens");
            }

            foreach (var currentToken in User.RefreshTokens)
            {
                await RevokeRefreshTokenAsync(currentToken, User, HttpContext);
            }
            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<ApplicationUser>> ValidateUser(string Username, string Password)
        {
            var identityUser = await _userManager.FindByNameAsync(Username);
            if (identityUser is not null)
            {
                var verifyResult = _userManager.PasswordHasher.VerifyHashedPassword(identityUser, identityUser.PasswordHash, Password);
                if (verifyResult == PasswordVerificationResult.Failed)
                {
                    return ServiceResult<ApplicationUser>.Failure("Password verification failed");
                }
                return ServiceResult<ApplicationUser>.Success(identityUser);
            }
            _logger.LogWarning("User: {user} not found", Username);
            return ServiceResult<ApplicationUser>.Failure("User not found");
        }

        private static bool IsRefreshTokenValid(RefreshTokenEntity ExistingToken)
        {
            // Is token already revoked, then return false
            if (ExistingToken.RevokedByIp != null && ExistingToken.RevokedOn != DateTime.MinValue)
            {
                return false;
            }
            // Token already expired, then return false
            if (ExistingToken.ExpiryOn <= DateTime.UtcNow)
            {
                return false;
            }
            return true;
        }

        private async Task<AccessToken> _generateAccessTokenAsync(ApplicationUser AppUser, int ExpirationMinutes)
        {
            _logger.LogDebug("Generate access token");
            _logger.LogDebug("Fetching user roles");
            IList<string> userRoles = await _userManager.GetRolesAsync(AppUser);
            var authClaims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, AppUser.UserName),
                        new Claim(ClaimTypes.NameIdentifier, AppUser.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };
            _logger.LogDebug("Adding roles to authentication claims");
            if (userRoles != null)
            {
                foreach (var userRole in userRoles)
                {
                    _logger.LogDebug("[+] {role}", userRole);
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }
            }
            //var expiryInMinutes = int.Parse(_configuration["JWT:TokenExpirationMinutes"]);
            var expiresOn = DateTime.UtcNow.AddMinutes(ExpirationMinutes);
            var accessToken = this._generateJwtToken(authClaims, expiresOn);
            return new AccessToken
            {
                Token = accessToken,
                ExpiryOn = expiresOn
            };
        }

        private RefreshTokenEntity _generateRefreshToken(string userId, string username, string ipaddress, int ExpirationMinutes)
        {
            var expiresOn = DateTime.UtcNow.AddMinutes(ExpirationMinutes);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.NameIdentifier, userId),
            };
            var token = this._generateJwtToken(claims, expiresOn);

            return (new RefreshTokenEntity
            {
                Token = token,
                ExpiryOn = expiresOn,
                CreatedOn = DateTime.UtcNow,
                CreatedByIp = "127.0.0.1",
                UserId = userId.ToString()
            });
        }

        /// <summary>
        /// Generates a JWT token.
        /// </summary>
        /// <param name="claims">A list of Claims to be included in the JWT token.</param>
        /// <param name="expires">The expiration date and time for the JWT token.</param>
        /// <returns>A JWT token as a string.</returns>
        /// <remarks>
        /// The JWT token is signed with a secret key and includes the provided claims.
        /// The issuer, audience, and notBefore values for the token are retrieved from the application's configuration.
        /// The method also logs the token's expiration date and time for debugging purposes.
        /// </remarks>
        private string _generateJwtToken(List<Claim> claims, DateTime expires)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: expires,
                claims: claims,
                signingCredentials: credentials,
                notBefore: DateTime.UtcNow
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<ServiceResult<ApplicationUser>> IsProvidedRefreshTokenValid(string RefreshToken)
        {
            var userWithTokens = await _context.Users.AsNoTracking().Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(y => y.Token == RefreshToken && y.UserId == x.Id));

            var existingToken = await _context.RefreshTokens.FirstOrDefaultAsync(t => t.Token == RefreshToken);

            if (userWithTokens is null)
            {
                return ServiceResult<ApplicationUser>.Failure("Token is not valid");
            }

            var isValid = IsRefreshTokenValid(existingToken);
            if (isValid)
            {
                return ServiceResult<ApplicationUser>.Success(userWithTokens);
            }
            return ServiceResult<ApplicationUser>.Failure("Token is not valid");
        }
    }
}