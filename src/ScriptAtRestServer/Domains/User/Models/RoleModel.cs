﻿using System.ComponentModel.DataAnnotations;

namespace ScriptAtRest.Server.Domains.User.Models
{
    public class RoleModel
    {
        [Required]
        public string RoleName { get; set; }
    }
}