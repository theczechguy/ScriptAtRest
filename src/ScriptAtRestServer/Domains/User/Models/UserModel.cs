namespace ScriptAtRest.Server.Domains.User.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public bool Approved { get; set; }
    }
}