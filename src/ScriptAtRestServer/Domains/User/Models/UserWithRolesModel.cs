using System.Collections.Generic;

namespace ScriptAtRest.Server.Domains.User.Models
{
    public class UserWithRolesModel
    {
        public string Username { get; set; }
        public IList<string> Roles { get; set; }
    }
}