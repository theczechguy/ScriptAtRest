﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Domains.Token;
using ScriptAtRest.Server.Helpers;
using ScriptAtRest.Server.IdentityAuth;
using ScriptAtRest.Server.IdentityAuth.Models;
using ScriptAtRestServer.IdentityAuth;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Domains.User
{
    public interface IUserService
    {
        Task<ServiceResult<ApplicationUser>> CreateUser(RegisterModel Model, ApplicationUserType UserType);

        Task<ServiceResult<bool>> DeleteUser(string UserName, ApplicationUserType UserType);

        Task<ServiceResult<List<ApplicationUser>>> GetAllUsers();

        Task<ServiceResult<ApplicationUser>> GetUserByName(string UserName, ApplicationUserType UserType);

        Task<ServiceResult<IList<string>>> GetUserRoles(string UserName);

        Task<ServiceResult<bool>> AddUserToRole(string UserName, string RoleName, ApplicationUserType U);

        Task<ServiceResult<ApplicationUser>> ApproveUser(string UserName);

        Task<ServiceResult<IdentityResult>> RemoveUserFromRole(string UserName, string RoleName);

        Task<ServiceResult<ApplicationUser>> CreateAgentUser(string UserName);

        Task<ServiceResult<bool>> UserExists(string UserName, ApplicationUserType UserType);

        Task<ServiceResult<ApplicationUser>> GetUserBynameWithRefreshToken(string UserName);
    }

    public class UserService : IUserService
    {
        protected readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<TokenService> _logger;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly DataContext _dataContext;

        public UserService
        (
            UserManager<ApplicationUser> UserManager,
            ILogger<TokenService> Logger,
            IConfiguration Configuration,
            RoleManager<IdentityRole> RoleManager,
            DataContext dataContext
        )
        {
            _userManager = UserManager;
            _logger = Logger;
            _configuration = Configuration;
            _roleManager = RoleManager;
            _dataContext = dataContext;
        }

        /// <summary>
        /// Retrieves a user with the specified username and includes their refresh tokens.
        /// </summary>
        /// <param name="UserName">
        /// The username of the user to retrieve.
        /// </param>
        /// <returns>
        /// Returns a <see cref="ApplicationUser"/> object representing the user with the specified username,
        /// including any associated refresh tokens.
        /// </returns>
        /// <remarks>
        /// This method retrieves a user with the specified username from the data context, and includes
        /// any associated refresh tokens in the result. The result is returned as an <see cref="ApplicationUser"/>
        /// object. If no user is found with the specified username, the method returns null.
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// Thrown if the <paramref name="UserName"/> parameter is null or empty.
        /// </exception>
        public async Task<ServiceResult<ApplicationUser>> GetUserBynameWithRefreshToken(string UserName)
        {
            var user = await _dataContext.Users.Include(r => r.RefreshTokens).FirstOrDefaultAsync(u => u.UserName == UserName);
            if (user is null)
            {
                return ServiceResult<ApplicationUser>.Failure("User not found");
            }
            return ServiceResult<ApplicationUser>.Success(user);
        }

        public async Task<ServiceResult<bool>> AddUserToRole(string UserName, string RoleName, ApplicationUserType UserType)
        {
            var user = await GetUserIfExists(UserName, UserType);
            if (user is null)
            {
                return ServiceResult<bool>.Failure("User not found");
            }
            var role = await RoleExists(RoleName);
            if (role is null)
            {
                return ServiceResult<bool>.Failure("Role not found");
            }
            _logger.LogDebug("Adding user: {user} to role: {role}", UserName, RoleName);
            var identityResult = await _userManager.AddToRoleAsync(user, role.Name);
            if (identityResult.Succeeded)
            {
                return ServiceResult<bool>.Success(true);
            }
            return ServiceResult<bool>.Failure(identityResult.Errors.ToString());
        }

        public async Task<ServiceResult<ApplicationUser>> ApproveUser(string UserName)
        {
            var user = await GetUserIfExists(UserName, ApplicationUserType.Regular);
            if (user is null)
            {
                return ServiceResult<ApplicationUser>.Failure("User not found");
            }
            var addRoleResult = await AddUserToRole(user.UserName, UserRoles.Approved, ApplicationUserType.Regular);
            if (addRoleResult.IsFailure)
            {
                return ServiceResult<ApplicationUser>.Failure(addRoleResult.ErrorMessage);
            }
            user.Approved = true;
            _logger.LogDebug("Updating user: {user}", UserName);
            await _userManager.UpdateAsync(user);
            return ServiceResult<ApplicationUser>.Success(user);
        }

        public async Task<ServiceResult<ApplicationUser>> CreateUser(RegisterModel Model, ApplicationUserType UserType)
        {
            _logger.LogDebug("Verifying if user already exists");
            var userExists = await _userManager.FindByNameAsync(Model.Username);
            if (userExists != null)
            {
                return ServiceResult<ApplicationUser>.Failure("User name is already taken");
            }

            ApplicationUser newUser = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = Model.Username,
                Approved = false,
                UserType = UserType
            };
            _logger.LogDebug("Creating new user account {@user}", newUser);
            var createResult = await _userManager.CreateAsync(newUser, Model.Password);
            if (!createResult.Succeeded)
            {
                return ServiceResult<ApplicationUser>.Failure($"Unable to create, please verify user details and try again - {string.Join(", ", createResult.Errors.Select(e => e.Description))}");
                //return ServiceResult<ApplicationUser>.Failure($"Unable to create, please verify user details and try again - {createResult.Errors}.");
            }
            return ServiceResult<ApplicationUser>.Success(await _userManager.FindByNameAsync(Model.Username));
        }

        public async Task<ServiceResult<bool>> DeleteUser(string UserName, ApplicationUserType UserType)
        {
            var user = await GetUserIfExists(UserName, UserType);
            if (user is null)
            {
                return ServiceResult<bool>.Failure("User not found");
            }
            _logger.LogDebug("Deleting user: {user}", UserName);
            await _userManager.DeleteAsync(user);
            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<List<ApplicationUser>>> GetAllUsers()
        {
            _logger.LogDebug("Fetching all users");
            var allUsers = await _userManager.Users.Where(u => u.UserType == ApplicationUserType.Regular).ToListAsync();
            if (allUsers.Count == 0)
            {
                return ServiceResult<List<ApplicationUser>>.Failure("No users found");
            }
            return ServiceResult<List<ApplicationUser>>.Success(allUsers);
        }

        public async Task<ServiceResult<ApplicationUser>> GetUserByName(string UserName, ApplicationUserType UserType)
        {
            var userExistsResult = await GetUserIfExists(UserName, UserType);
            if (userExistsResult is null)
            {
                return ServiceResult<ApplicationUser>.Failure("User not found");
            }
            return ServiceResult<ApplicationUser>.Success(userExistsResult);
        }

        public async Task<ServiceResult<IList<string>>> GetUserRoles(string UserName)
        {
            _logger.LogDebug("Get all roles of user: {user}", UserName);
            var user = await GetUserIfExists(UserName, ApplicationUserType.Regular);
            if (user is null)
            {
                return ServiceResult<IList<string>>.Failure("User not found");
            }
            return ServiceResult<IList<string>>.Success(await _userManager.GetRolesAsync(user));
        }

        private async Task<ApplicationUser> GetUserIfExists(string UserName, ApplicationUserType UserType)
        {
            _logger.LogDebug("Verify if user: {user} with usertype :{usertype} exists", UserName, UserType);
            ApplicationUser user = await _dataContext.Users.FirstOrDefaultAsync(u => u.UserName == UserName && u.UserType == UserType);
            return user;
        }

        private async Task<IdentityRole> RoleExists(string RoleName)
        {
            _logger.LogDebug("Verify if role: {role} exists", RoleName);
            return await _roleManager.FindByNameAsync(RoleName);
        }

        public async Task<ServiceResult<IdentityResult>> RemoveUserFromRole(string UserName, string RoleName)
        {
            var user = await GetUserIfExists(UserName, ApplicationUserType.Regular);
            if (user is null)
            {
                return ServiceResult<IdentityResult>.Failure("User not found");
            }
            var role = await RoleExists(RoleName);
            if (role is null)
            {
                return ServiceResult<IdentityResult>.Failure("Role not found");
            }

            _logger.LogDebug("Removing user: {user} from role: {role}", UserName, RoleName);
            return ServiceResult<IdentityResult>.Success(await _userManager.RemoveFromRoleAsync(user, role.Name));
        }

        public async Task<ServiceResult<ApplicationUser>> CreateAgentUser(string AgentName)
        {
            var newUserModel = new RegisterModel
            {
                Username = AgentName,
                Password = _generateRandomPassword()
            };
            _logger.LogDebug("Creating user for agent connection : {agentuser}", newUserModel.Username);
            var newUserResult = await CreateUser(newUserModel, ApplicationUserType.Agent);
            if (!newUserResult.IsSuccess)
            {
                return ServiceResult<ApplicationUser>.Failure(newUserResult.ErrorMessage);
            }
            var addUserToRoleResult = await AddUserToRole(newUserResult.Value.UserName, UserRoles.Agent, ApplicationUserType.Agent);
            if (addUserToRoleResult.IsFailure)
            {
                return ServiceResult<ApplicationUser>.Failure(addUserToRoleResult.ErrorMessage);
            }
            return ServiceResult<ApplicationUser>.Success(newUserResult.Value);
        }

        private static string _generateRandomPassword(int Length = 12)
        {
            const string lowerCaseChars = "abcdefghijklmnopqrstuvwxyz";
            const string upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string digitChars = "0123456789";
            const string specialChars = "!@#$%^&*_-+=";

            // Combine all character sets
            var allChars = lowerCaseChars + upperCaseChars + digitChars + specialChars;
            var random = new Random();

            // Ensure that the password contains at least one character from each character set
            var password = new char[Length];
            password[0] = lowerCaseChars[random.Next(lowerCaseChars.Length)];
            password[1] = upperCaseChars[random.Next(upperCaseChars.Length)];
            password[2] = digitChars[random.Next(digitChars.Length)];
            password[3] = specialChars[random.Next(specialChars.Length)];

            // Fill the rest of the password with random characters from the combined character set
            for (int i = 4; i < Length; i++)
            {
                password[i] = allChars[random.Next(allChars.Length)];
            }

            // Shuffle the password characters
            var shuffledPassword = password.OrderBy(c => random.Next()).ToArray();

            return new string(shuffledPassword);
        }

        public async Task<ServiceResult<bool>> UserExists(string UserName, ApplicationUserType UserType)
        {
            var user = await GetUserByName(UserName, UserType);
            if (user.IsFailure)
            {
                return ServiceResult<bool>.Success(false);
            }
            return ServiceResult<bool>.Success(true);
        }
    }
}