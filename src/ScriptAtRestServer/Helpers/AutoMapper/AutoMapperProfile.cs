using AutoMapper;
using Microsoft.AspNetCore.Identity;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.Domains.Script.Models;
using ScriptAtRest.Server.Domains.ScriptScheduling.Models;
using ScriptAtRest.Server.Domains.Token.Models;
using ScriptAtRest.Server.Domains.User.Models;
using ScriptAtRest.Shared.Scripts;
using ScriptAtRestServer.IdentityAuth;
using System;

namespace WebApi.Helpers.AutoMapper;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<long, DateTime>().ConvertUsing<TicksToDateTimeConverter>();

        CreateMap<IdentityUser, ApplicationUser>();
        CreateMap<ApplicationUser, UserModel>();

        CreateMap<ScriptEntity, ScriptModel>();
        CreateMap<RegisterScriptModel, ScriptEntity>();
        CreateMap<UpdateScriptModel, ScriptEntity>();

        CreateMap<ScriptTypeEntity, ScriptTypeModel>();
        CreateMap<RegisterScriptTypeModel, ScriptTypeEntity>();
        CreateMap<UpdateScriptTypeModel, ScriptTypeEntity>();

        CreateMap<ScriptEntity, ScriptSimplifiedModel>();

        CreateMap<TokenResult, NewTokensModel>()
            .ForMember(dest => dest.Token, opt => opt.MapFrom(src => src.Token.Token))
            .ForMember(dest => dest.TokenExpiration, opt => opt.MapFrom(src => src.Token.ExpiryOn))
            .ForMember(dest => dest.RefreshToken, opt => opt.MapFrom(src => src.RefreshToken.Token))
            .ForMember(dest => dest.RefreshTokenExpiration, opt => opt.MapFrom(src => src.RefreshToken.ExpiryOn));

        CreateMap<ScriptJobEntity, ScriptJobModel>()
            .ForMember(dest => dest.ScriptId, opt => opt.MapFrom(src => src.Script.Id));
        CreateMap<ScriptJobEntity, ScriptJobSimplifiedModel>();
    }
}

public class TicksToDateTimeConverter : ITypeConverter<long, DateTime>
{
    public DateTime Convert(long source, DateTime destination, ResolutionContext context)
    {
        return new DateTime(source); // interpret long as Ticks
    }
}