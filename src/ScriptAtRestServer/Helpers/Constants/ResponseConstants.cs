﻿namespace ScriptAtRestServer.Helpers.Constants
{
    public class ResponseConstants
    {
        public const string StatusOk = "Successful";
        public const string StatusFail = "Failed";
        public const string TokenMissing = "Refresh token is missing";
    }
}