﻿namespace ScriptAtRestServer.Helpers
{
    public class GenericResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}