﻿using System;
using System.Security.Cryptography;

namespace ScriptAtRest.Server.Helpers
{
    public class RandomGenerators
    {
        public static int GenerateRandomNumber(int minValue, int maxValue)
        {
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            var randomBytes = new byte[4];
            randomNumberGenerator.GetBytes(randomBytes);
            var randomInt = BitConverter.ToInt32(randomBytes, 0);
            return Math.Abs(randomInt % (maxValue - minValue)) + minValue;
        }
    }
}