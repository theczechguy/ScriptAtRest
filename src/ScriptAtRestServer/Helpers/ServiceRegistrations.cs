﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using ScriptAtRest.Server.AgentHub;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Domains.AgentCapabilitySrv;
using ScriptAtRest.Server.Domains.AgentInitializerSrv;
using ScriptAtRest.Server.Domains.AgentSrv;
using ScriptAtRest.Server.Domains.Script;
using ScriptAtRest.Server.Domains.ScriptScheduling;
using ScriptAtRest.Server.Domains.ScriptScheduling.Queue;
using ScriptAtRest.Server.Domains.ScriptSchedulingSrv;
using ScriptAtRest.Server.Domains.Token;
using ScriptAtRest.Server.Domains.User;
using ScriptAtRestServer.IdentityAuth;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;

using System.Text;
using System.Threading.Tasks;

namespace ScriptAtRest.Server.Helpers
{
    public static class ServiceRegistrations
    {
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options => options.UseNpgsql(configuration.GetConnectionString("PgSql")));
        }

        public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSignalR();

            services.AddHostedService<AgentInitializerService>();
            services.AddHostedService<ScriptJobProcessorWorker>();
            services.AddHostedService<ScriptJobRerunService>();
            services.AddScoped<IScriptService, ScriptService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IScriptSchedulingService, ScriptSchedulingService>();
            services.AddScoped<IAgentCapabilityService, AgentCapabilityService>();
            services.AddSingleton<IScriptJobQueue, ScriptJobQueue>();

            services.AddSingleton<AgentSynchronizationService>();
        }

        public static async Task ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            var authenticationBuilder = services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer("local", options =>
            {
                options.IncludeErrorDetails = true;
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateLifetime = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = configuration["JWT:ValidAudience"],
                    ValidIssuer = configuration["JWT:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"])),
                    ClockSkew = TimeSpan.Zero
                };
            })
            .AddPolicyScheme(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.ForwardDefaultSelector = context =>
                {
                    if (!configuration.GetSection("AzureAd").Exists())
                    {
                        return "local"; // if azuread section is not in the config return local instantly
                    }
                    var jwtHandler = new JwtSecurityTokenHandler();
                    var authorizationHeader = context.Request.Headers[HeaderNames.Authorization].ToString();
                    if (!string.IsNullOrEmpty(authorizationHeader) && authorizationHeader.StartsWith("Bearer "))
                    {
                        var token = authorizationHeader.Substring("Bearer ".Length).Trim();
                        if (jwtHandler.CanReadToken(token))
                        {
                            var parsed = jwtHandler.ReadToken(token);
                            var tokenIssuer = parsed.Issuer;
                            if (tokenIssuer.StartsWith("https://login.microsoftonline.com/"))
                            {
                                return "azuread";
                            }
                        }
                    }
                    return "local"; // if unknown return "local"
                };
            });

            // Check if AzureAd configuration section exists
            var azureAdSection = configuration.GetSection("AzureAd");
            if (azureAdSection.Exists())
            {
                authenticationBuilder.AddMicrosoftIdentityWebApi(azureAdSection, "azuread");
            }
            else
            {
                Log.Logger.Warning("AzureAd configuration block is not present, the authentication method will not be available");
            }
        }

        public static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "ScriptAtRest API",
                    Description = "Documentation of ScriptAtRest API",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "Michal Zezula"
                    }
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id="Bearer"
                            }
                        },
                        new string[]{}
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }
    }
}