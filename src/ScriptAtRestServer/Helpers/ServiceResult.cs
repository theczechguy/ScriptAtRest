﻿namespace ScriptAtRest.Server.Helpers
{
    public class ServiceResult<T>
    {
        public bool IsSuccess { get; private set; }
        public bool IsFailure { get; private set; }
        public T Value { get; private set; }
        public string ErrorMessage { get; private set; }

        public static ServiceResult<T> Success(T Value)
        {
            return new ServiceResult<T> { IsSuccess = true, IsFailure = false, Value = Value };
        }

        public static ServiceResult<T> Failure(string errorMessage)
        {
            return new ServiceResult<T> { IsSuccess = false, IsFailure = true, ErrorMessage = errorMessage };
        }
    }
}