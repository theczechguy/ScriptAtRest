﻿using Microsoft.AspNetCore.Identity;
using ScriptAtRest.Server.Database.Entities;
using ScriptAtRest.Server.IdentityAuth;
using System.Collections.Generic;

namespace ScriptAtRestServer.IdentityAuth
{
    public class ApplicationUser : IdentityUser
    {
        public bool Approved { get; set; }
        public List<RefreshTokenEntity> RefreshTokens { get; set; }
        public ApplicationUserType UserType { get; set; }
    }
}