﻿namespace ScriptAtRest.Server.IdentityAuth
{
    public enum ApplicationUserType
    {
        Regular,
        Agent
    }
}