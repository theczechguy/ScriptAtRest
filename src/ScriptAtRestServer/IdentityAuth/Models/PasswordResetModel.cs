﻿namespace ScriptAtRest.Server.IdentityAuth.Models
{
    public class PasswordResetModel
    {
        public string Password { get; set; }
    }
}