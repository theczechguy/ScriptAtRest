﻿namespace ScriptAtRestServer.IdentityAuth
{
    public class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
        public const string Approved = "Approved";
        public const string Agent = "Agent";
    }
}