﻿using Microsoft.AspNetCore.Http;
using Serilog.Context;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriptAtRestServer.RequestLogContextMiddleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class RequestLogContextMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestLogContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext HttpContext)
        {
            string authHeader = HttpContext.Request.Headers["Authorization"];
            string username = "unknown";
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                int seperatorIndex = usernamePassword.IndexOf(':');
                username = usernamePassword.Substring(0, seperatorIndex);
            }

            if (authHeader != null && authHeader.StartsWith("Bearer"))
            {
                var encodedToken = authHeader.Substring("Bearer ".Length).Trim();
                var handler = new JwtSecurityTokenHandler();
                var token = handler.ReadToken(encodedToken) as JwtSecurityToken;
                if (token.Issuer.Contains("https://login.microsoftonline.com")) // if the token is issued by MS idp
                {
                    username = token.Claims.Where(c => c.Type == "name").FirstOrDefault().Value;
                }
                else
                {
                    username = token.Claims.First().Value;
                }
            }

            using (LogContext.PushProperty("Username", username))
            {
                return _next.Invoke(HttpContext);
            }
        }
    }
}