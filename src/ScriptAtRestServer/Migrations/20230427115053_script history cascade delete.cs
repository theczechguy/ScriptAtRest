﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scripthistorycascadedelete : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScriptHistoryEntity_Scripts_ScriptEntityId",
                table: "ScriptHistoryEntity");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "48bb0fc6-69d2-47b7-9566-65f0bc036442", "AQAAAAIAAYagAAAAEB1JU3In8w79ZxPU/1MDj0TAGPL4umTJZDKTDJlPtwoPZ5kGu3oC5WshwApCwti4kw==", "aa135833-7150-4eaa-9507-d71f3be8f2ea" });

            migrationBuilder.AddForeignKey(
                name: "FK_ScriptHistoryEntity_Scripts_ScriptEntityId",
                table: "ScriptHistoryEntity",
                column: "ScriptEntityId",
                principalTable: "Scripts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScriptHistoryEntity_Scripts_ScriptEntityId",
                table: "ScriptHistoryEntity");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2b91f549-521b-4a6f-b08b-2dce13e71ea5", "AQAAAAIAAYagAAAAEOwd536bR9xYROUS1OR2wEMDRndE/XCUuel61HCACHhbgx2jpQL5/iDljW0sFqaxfQ==", "eb677149-9d2e-4a91-80f4-6bbc3b963d9c" });

            migrationBuilder.AddForeignKey(
                name: "FK_ScriptHistoryEntity_Scripts_ScriptEntityId",
                table: "ScriptHistoryEntity",
                column: "ScriptEntityId",
                principalTable: "Scripts",
                principalColumn: "Id");
        }
    }
}