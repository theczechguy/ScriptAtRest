﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class addUserTypetoApplicationUserentity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "AspNetUsers",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp", "UserType" },
                values: new object[] { "50822d40-831b-4ae0-967f-83ecbcacde0c", "AQAAAAIAAYagAAAAEHLZ08Wb/xsKYu8+oMhOCgWX8uglTONCmAO5jzwW8htuF/d8iT2d7a0+G5Np5qXcFA==", "b2bb42e4-a625-489c-9ebf-188bdadff14f", 0 });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserType",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "48bb0fc6-69d2-47b7-9566-65f0bc036442", "AQAAAAIAAYagAAAAEB1JU3In8w79ZxPU/1MDj0TAGPL4umTJZDKTDJlPtwoPZ5kGu3oC5WshwApCwti4kw==", "aa135833-7150-4eaa-9507-d71f3be8f2ea" });
        }
    }
}