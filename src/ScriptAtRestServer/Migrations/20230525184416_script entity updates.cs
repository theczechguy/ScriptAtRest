﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scriptentityupdates : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScriptId",
                table: "ScriptHistoryEntity");

            migrationBuilder.AlterColumn<int>(
                name: "ScriptEntityId",
                table: "ScriptHistoryEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "74dc86d4-450d-4584-95dc-b0c2582c6445", "AQAAAAIAAYagAAAAEOWtzPu7W0cVy07ZOB/pbPRwEgd3p1qk6zP0GvESx/G9IGlhXdt+7yIOZf2iq+6ptA==", "10184add-04b5-4a2d-9ee8-66690bd80321" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ScriptEntityId",
                table: "ScriptHistoryEntity",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "ScriptId",
                table: "ScriptHistoryEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "50822d40-831b-4ae0-967f-83ecbcacde0c", "AQAAAAIAAYagAAAAEHLZ08Wb/xsKYu8+oMhOCgWX8uglTONCmAO5jzwW8htuF/d8iT2d7a0+G5Np5qXcFA==", "b2bb42e4-a625-489c-9ebf-188bdadff14f" });
        }
    }
}