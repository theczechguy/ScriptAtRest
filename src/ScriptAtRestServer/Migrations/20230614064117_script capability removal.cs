﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scriptcapabilityremoval : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Scripts_AgentCapabilities_AgentCapabilityId",
                table: "Scripts");

            migrationBuilder.DropIndex(
                name: "IX_Scripts_AgentCapabilityId",
                table: "Scripts");

            migrationBuilder.DropColumn(
                name: "AgentCapabilityId",
                table: "Scripts");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6723ee00-443f-4910-86f4-70b01de5e42a", "AQAAAAIAAYagAAAAEP9V+iRGfvkYFLngRhn/+rS4ughKPctC2WxJ4sJvDRP8988Kjkt6UaKcdZVvgGReMQ==", "3c9f867d-f109-45b2-9518-e2226705ae5d" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AgentCapabilityId",
                table: "Scripts",
                type: "uuid",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "74dc86d4-450d-4584-95dc-b0c2582c6445", "AQAAAAIAAYagAAAAEOWtzPu7W0cVy07ZOB/pbPRwEgd3p1qk6zP0GvESx/G9IGlhXdt+7yIOZf2iq+6ptA==", "10184add-04b5-4a2d-9ee8-66690bd80321" });

            migrationBuilder.CreateIndex(
                name: "IX_Scripts_AgentCapabilityId",
                table: "Scripts",
                column: "AgentCapabilityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Scripts_AgentCapabilities_AgentCapabilityId",
                table: "Scripts",
                column: "AgentCapabilityId",
                principalTable: "AgentCapabilities",
                principalColumn: "Id");
        }
    }
}