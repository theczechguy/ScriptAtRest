﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scripjobenhancements : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AgentCapabilityId",
                table: "ScriptJobs",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdatedOn",
                table: "ScriptJobs",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "251b087f-62cc-47d6-bece-34c60d172add", "AQAAAAIAAYagAAAAEND2OdP9rq5wFJCphbinTDDYVJSiuxDSt+eb0cgGod1fWlLZOb3dkPbRWZaelZ8iHg==", "9895f0cf-9ada-422f-8f50-75057086cae5" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgentCapabilityId",
                table: "ScriptJobs");

            migrationBuilder.DropColumn(
                name: "LastUpdatedOn",
                table: "ScriptJobs");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6723ee00-443f-4910-86f4-70b01de5e42a", "AQAAAAIAAYagAAAAEP9V+iRGfvkYFLngRhn/+rS4ughKPctC2WxJ4sJvDRP8988Kjkt6UaKcdZVvgGReMQ==", "3c9f867d-f109-45b2-9518-e2226705ae5d" });
        }
    }
}