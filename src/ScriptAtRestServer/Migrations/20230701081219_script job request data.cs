﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scriptjobrequestdata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ScriptRequestData",
                table: "ScriptJobs",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b851c182-d110-4278-8148-437db0ea5b99", "AQAAAAIAAYagAAAAED2BaiT6Iog+0xlmu/imuAg29b7aBkvrLR8Wu8eSgIKA2bFj+2K3CxJLNOjIrxWluQ==", "754bb84a-9ad1-4828-a0cf-94449c0acab1" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScriptRequestData",
                table: "ScriptJobs");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "251b087f-62cc-47d6-bece-34c60d172add", "AQAAAAIAAYagAAAAEND2OdP9rq5wFJCphbinTDDYVJSiuxDSt+eb0cgGod1fWlLZOb3dkPbRWZaelZ8iHg==", "9895f0cf-9ada-422f-8f50-75057086cae5" });
        }
    }
}