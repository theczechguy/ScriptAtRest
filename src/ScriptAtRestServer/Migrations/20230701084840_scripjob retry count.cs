﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScriptAtRest.Server.Migrations
{
    /// <inheritdoc />
    public partial class scripjobretrycount : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RetryCount",
                table: "ScriptJobs",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "81e311e5-fea7-4b1c-8534-708d92f64f3f", "AQAAAAIAAYagAAAAEKq8lWKRyIOEkNDUcawPieSMxfkHc7LoSXy/+JCJVhKyxH8ocjwGCATbHu3M85dsmg==", "acd23e27-9ab5-483b-a535-2925f6e585b1" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RetryCount",
                table: "ScriptJobs");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "00000000-0000-0000-0000-000000000001",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b851c182-d110-4278-8148-437db0ea5b99", "AQAAAAIAAYagAAAAED2BaiT6Iog+0xlmu/imuAg29b7aBkvrLR8Wu8eSgIKA2bFj+2K3CxJLNOjIrxWluQ==", "754bb84a-9ad1-4828-a0cf-94449c0acab1" });
        }
    }
}