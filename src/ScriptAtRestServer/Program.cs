using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using ScriptAtRest.Server.Database;

using ScriptAtRest.Server.Helpers;
using ScriptAtRestServer.IdentityAuth;
using ScriptAtRestServer.Middleware;
using ScriptAtRestServer.RequestLogContextMiddleware;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;

// Builder configuration
var builder = WebApplication.CreateBuilder(args);

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.WithProperty("AppPath", Environment.ProcessPath)
    .CreateLogger();

builder.Host.UseSerilog();

var appExecutionMethod = builder.Configuration.GetValue<string>("General:AppExecutionMethod");
if (appExecutionMethod != "standalone")
{
    var pathToContentRoot = Path.GetDirectoryName(Environment.ProcessPath);
    Directory.SetCurrentDirectory(pathToContentRoot);
}
switch (appExecutionMethod)
{
    case "standalone":
        Log.Information("Starting as standalone");
        break;

    case "winservice":
        Log.Information("Starting as winservice");
        builder.Host.UseWindowsService();
        break;

    case "systemd":
        Log.Information("Starting as systemd service");
        builder.Host.UseSystemd();
        break;

    default:
        throw new Exception(string.Format("Appexecutionmethod configuration item contains unknown value : {0}", appExecutionMethod));
}
Log.Information($"Version: {Assembly.GetExecutingAssembly().GetName().Version}");

// ConfigureServices
builder.Services.AddControllers(x => x.AllowEmptyInputInBodyModelBinding = true);

builder.Services.ConfigureSqlContext(builder.Configuration);
builder.Services.ConfigureServices(builder.Configuration);
await builder.Services.ConfigureAuthentication(builder.Configuration);

builder.Services.ConfigureSwagger(builder.Configuration);

//IdentityModelEventSource.ShowPII = true; // only when debuging

// App configuration

var app = builder.Build();

// Configure
app.MapHub<ScriptAtRestServer.SignalRHub.AgentHub>("/agenthub");

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "ScriptAtRest API V1");
});

app.MapControllers();

// global cors policy
app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());
app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

// this specific order is important for the logging to properly work
app.UseMiddleware<ExceptionMiddleware>();
app.UseMiddleware<RequestLogContextMiddleware>();
app.UseSerilogRequestLogging();

try
{
    using (var scope = app.Services.CreateScope())
    {
        var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();
        dbContext.Database.Migrate();
    }
    app.Run();
}
catch (Exception ex)
{
    Console.WriteLine($"Unhandled fatal exception :{ex}");
    Environment.Exit(1);
}