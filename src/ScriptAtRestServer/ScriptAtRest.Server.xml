<?xml version="1.0"?>
<doc>
    <assembly>
        <name>ScriptAtRest.Server</name>
    </assembly>
    <members>
        <member name="M:ScriptAtRestServer.Controllers.AgentController.ResetAgentTokensById(System.Guid)">
            <summary>
            Manually reset tokens for specified agent id
            </summary>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.AgentController.GetAllAgents">
            <summary>
            List all registered agents
            </summary>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.RegisterAsync(ScriptAtRest.Server.Domains.Script.Models.RegisterScriptModel)">
             <summary>
             Register new script
             </summary>
             <param name="Model"></param>
             <remarks>
             # Sample request
             ```
                 POST /scripts/register
                 {
                    "Name": "Script name",
                    "Type": 1,
                    "EncodedContent": "aGVsbG8=",
                    "Timeout": 60
                 }
             ```
             # Parameter requirements
             - Name must be unique
             - Script type must exist before script registration
             - Encoded content is BASE64 UTF8 encoded string
             - Timeout is in seconds

             # Encoding script content
             - ***Powershell***
                 ```
                 $Text = "script content"
                 [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($Text))
                 ```
             - ***Python***
                 ```
                 import base64
                 base64.b64encode(bytes('script content', 'utf-8'))
                 ```
             </remarks>
             <returns>A newly created script</returns>
             <response code="200">Returns the newly created item</response>
             <response code="400">If registration fails</response>
             <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.GetAll">
            <summary>
            Get all registered scripts
            </summary>
            <returns>A list of all scripts</returns>
            <response code="200">Returns a list of all scripts</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.GetByIdAsync(System.Int32)">
            <summary>
            Get script by id
            </summary>
            <param name="Id"></param>
            <returns>Script</returns>
            <response code="200">Returns script</response>
            <response code="400">If script id cannot be found</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.GetScriptByNameAsync(ScriptAtRest.Server.Domains.Script.Models.ScriptByNameModel)">
            <summary>
            Get script by name
            </summary>
            <param name="RequestModel"></param>
            <response code="200">Returns script</response>
            <response code="400">If script name cannot be found</response>
            <response code="500">If application experiences fatal error</response>
            <returns></returns>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.DeleteAsync(System.Int32)">
            <summary>
            Delete script by id
            </summary>
            <param name="Id"></param>
            <returns>Script</returns>
            <response code="200">Returns empty response</response>
            <response code="400">If script id cannot be found</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.UpdateScriptAsync(ScriptAtRest.Server.Domains.Script.Models.UpdateScriptModel,System.Int32)">
            <summary>
            Update script definition
            </summary>
            <remarks>
            # Sample request
            - this request will update script with id ***1*** with new script content and new timeout value
                ```
                    PUT /scripts/1
                    {
                        "EncodedContent": "aGVsbG8=",
                        "Timeout": 59
                    }
                ```
            # Remarks
            - there is no need to specify all fields, only put in the request things that you want to change
            - encodedContent is generated the same way as when you register new scripts. IF you need help read docu for the scripts/register action
            </remarks>
            <param name="Model"></param>
            <param name="Id"></param>
            <returns>Updated script</returns>
            <response code="200">Returns updated definition</response>
            <response code="400">If update fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.GetAllScriptTypes">
            <summary>
            Get list of registered script types
            </summary>
            <returns>A list of script types</returns>
            <response code="200">Returns a list of all script types</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.GetScripTypeById(System.Int32)">
            <summary>
            Get script type by id
            </summary>
            <param name="Id"></param>
            <returns>Script type</returns>
            <response code="200">Returns script type</response>
            <response code="400">If script type id not found</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.RegisterScriptType(ScriptAtRest.Server.Domains.Script.Models.RegisterScriptTypeModel)">
            <summary>
            Register script type
            </summary>
            <param name="Model"></param>
            <returns></returns>
            <response code="200">Returns script type</response>
            <response code="400">If script type id not found</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.DeleteScriptTypeAsync(System.Int32)">
            <summary>
            Delete script type by id
            </summary>
            <param name="Id"></param>
            <returns></returns>
            <response code="200">Empty response</response>
            <response code="400">If script type id not found</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptController.UpdateScriptTypeAsync(ScriptAtRest.Server.Domains.Script.Models.UpdateScriptTypeModel,System.Int32)">
            <summary>
            Update script type definition
            </summary>
            <param name="Model"></param>
            <param name="Id"></param>
            <returns>Updated script type</returns>
            <response code="200">Returns updated script type definition</response>
            <response code="400">If script type update fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptJobController.ScheduleScriptRun(ScriptAtRest.Server.Domains.ScriptScheduling.Models.ScheduleScriptJobModel)">
            <summary>
            Create a script job for a specified script id
            Body of the request can contain script parameters
            Script parameter values must be encoded using Base64 UTF8 encoding
            </summary>
            <param name="scheduleScriptJobModel"></param>
            <returns></returns>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.ScriptJobController.GetScriptJob(System.Guid)">
            <summary>
            Get details of a script job
            </summary>
            <param name="ScriptJobId"></param>
            <response code="200">Return a script job details</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.Register(ScriptAtRest.Server.IdentityAuth.Models.RegisterModel)">
            <summary>
            Register new user
            </summary>
            <param name="RegisterModel"></param>
            <returns>A newly created User</returns>
            <response code="200">Returns the newly created item</response>
            <response code="400">If registration fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.Delete(System.String)">
            <summary>
            Delete user
            </summary>
            <param name="UserName"></param>
            <returns>Confirmation message</returns>
            <response code="200">Returns the newly created item</response>
            <response code="400">If registration fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.GetAll">
            <summary>
            Get list of all users
            </summary>
            <returns>List of users</returns>
            <response code="200">If successfull</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.GetUserByName(System.String)">
            <summary>
            Find user by name
            </summary>
            <param name="UserName"></param>
            <returns>User details</returns>
            <response code="200">If successfull</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.GetUserRoles(System.String)">
            <summary>
            Get roles of specified user
            </summary>
            <param name="UserName"></param>
            <returns>Returns list of roles the user is member of</returns>
            <response code="200">If successfull</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.ApproveUser(System.String)">
            <summary>
            Approve user after registration
            </summary>
            <remarks>Only users with Admin role can run this request</remarks>
            <param name="UserName"></param>
            <returns>Confirmation message</returns>
            <response code="200">Returns user information</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.AddUserRole(System.String,ScriptAtRest.Server.Domains.User.Models.RoleModel)">
            <summary>
            Grant access to a role
            </summary>
            <remarks>Only users with Admin role can run this request</remarks>
            <param name="UserName"></param>
            <param name="AddRoleModel"></param>
            <returns>Confirmation message</returns>
            <response code="200">If successfull</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:ScriptAtRestServer.Controllers.UsersController.RemoveUserRole(System.String,ScriptAtRest.Server.Domains.User.Models.RoleModel)">
            <summary>
            Remove access to a role
            </summary>
            <remarks>Only users with Admin role can run this request</remarks>
            <param name="UserName"></param>
            <param name="RemoveRoleModel"></param>
            <returns>Confirmation message</returns>
            <response code="200">If successfull</response>
            <response code="400">If request failes</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:JwtAuthentication.Controllers.AuthenticationController.Login(ScriptAtRest.Server.IdentityAuth.Models.LoginModel)">
            <summary>
            Generate access token for user
            </summary>
            <param name="LoginModel"></param>
            <returns>Access token</returns>
            <response code="200">Returns new access token</response>
            <response code="400">Request fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:JwtAuthentication.Controllers.AuthenticationController.RefreshToken(ScriptAtRest.Shared.Scripts.RefreshTokenRequest)">
            <summary>
            Refresh access and refresh token
            </summary>
            <remarks>Refresh token can be provided via cookie or body</remarks>
            <returns>New refresh and access token</returns>
            <response code="200">Returns new tokens</response>
            <response code="400">Request fails</response>
            <response code="500">If application experiences fatal error</response>
        </member>
        <member name="M:JwtAuthentication.Controllers.AuthenticationController.ResetUserPassword(System.String,ScriptAtRest.Server.IdentityAuth.Models.PasswordResetModel)">
            <summary>
            Set new password for a user
            </summary>
            <remarks>Only users with Admin role can run this request</remarks>
            <param name="UserName"></param>
            <param name="NewPasswordModel"></param>
            <returns>Confirmation message</returns>
        </member>
        <member name="M:JwtAuthentication.Controllers.AuthenticationController.RevokeToken(ScriptAtRest.Server.Domains.Token.Models.RevokeTokenRequest)">
            <summary>
            Revoke existing refresh token
            </summary>
            <param name="RequestModel"></param>
            <returns>Confirmation message</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.IAgentCapabilityService.NewAgentCapability(System.String,System.String)">
            <summary>
            Creates a new agent capability with the specified name and description.
            </summary>
            <param name="CapabilityName">The name of the new capability.</param>
            <param name="Description">The description of the new capability.</param>
            <returns>The newly created agent capability.</returns>
            <exception cref="T:ScriptAtRestServer.Helpers.AppException">Thrown when an agent capability with the same name already exists.</exception>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.IAgentCapabilityService.GetAgentCapabilityByName(System.String)">
            <summary>
            Retrieves an agent capability by name.
            </summary>
            <param name="CapabilityName">The name of the agent capability to retrieve.</param>
            <returns>The agent capability with the specified name, or null if it is not found.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.IAgentCapabilityService.GetAgentCapabilityById(System.Guid)">
            <summary>
            Retrieves an agent capability by name.
            </summary>
            <param name="CapabilityId">The name of the agent capability to retrieve.</param>
            <returns>The agent capability with the specified name, or null if it is not found.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.IAgentCapabilityService.RemoveAgentCapabilityByName(System.String)">
            <summary>
            Removes an agent capability by name.
            </summary>
            <param name="CapabilityName">The name of the agent capability to remove.</param>
            <returns>A boolean indicating whether the capability was found and removed (true) or not found (false).</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.IAgentCapabilityService.ListAllCapabilities">
            <summary>
            List all registered capabilities
            </summary>
            <returns>A list of registered capabilities</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.AgentCapabilityService._getCapabilityByName(System.String,System.Boolean)">
            <summary>
            Gets an agent capability by name from the database.
            </summary>
            <param name="CapabilityName">The name of the agent capability to retrieve.</param>
            <param name="trackEntity">A boolean indicating whether to track the entity or not. If true, the entity will be tracked by the EF DbContext.</param>
            <returns>The agent capability with the specified name, or null if it is not found.</returns>
            <remarks>
            If <paramref name="trackEntity" /> is set to false, the method will return the agent capability without tracking it in the EF DbContext. This can improve performance in scenarios where the entity is read-only and will not be modified.
            </remarks>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentCapabilitySrv.AgentCapabilityService._getCapabilityById(System.Guid,System.Boolean)">
            <summary>
            Gets an agent capability by name from the database.
            </summary>
            <param name="CapabilityId">The name of the agent capability to retrieve.</param>
            <param name="trackEntity">A boolean indicating whether to track the entity or not. If true, the entity will be tracked by the EF DbContext.</param>
            <returns>The agent capability with the specified name, or null if it is not found.</returns>
            <remarks>
            If <paramref name="trackEntity" /> is set to false, the method will return the agent capability without tracking it in the EF DbContext. This can improve performance in scenarios where the entity is read-only and will not be modified.
            </remarks>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentSrv.AgentService.GetAvailableAgentWithCapabilityId(System.Guid)">
            <summary>
            Finds an available agent with the specified capability ID.
            </summary>
            <param name="CapabilityId">The ID of the capability to search for.</param>
            <returns>An <see cref="T:ScriptAtRest.Server.Domains.AgentSrv.Models.AgentModel" /> object representing the selected agent, or null if no agent is available.</returns>
            <remarks>
            This method searches the database for an agent with the specified capability ID and connection status "Connected". If multiple agents are found, a random agent is selected from the list.
            </remarks>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.AgentSrv.AgentService.GetAnyAvailableAgent">
            <summary>
            Gets any available agent that is currently connected.
            </summary>
            <returns>
            An <see cref="T:ScriptAtRest.Server.Domains.AgentSrv.Models.AgentModel" /> representing the available agent, or null if no agents are available.
            </returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.ScriptScheduling.ScriptJobProcessorService._getAgentForScriptJob(System.Guid,ScriptAtRest.Server.Domains.AgentSrv.IAgentService)">
            <summary>
            Identifies a suitable agent for the specified script entity based on its agent capability.
            If script requires agent with capability it will look only for those with matching capability
            If there is not requirement for agept capability then any connected agent is randomly selected
            </summary>
            <param name="CapabilityId">Optional Capability ID</param>
            <param name="agentService">Agent service</param>
            <returns>An agent model representing the suitable agent, or null if no suitable agent is found.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.ITokenService.GenerateTokensAsync(System.String,Microsoft.AspNetCore.Http.HttpContext,System.Int32,System.Int32)">
            <summary>
            Generates access and refresh tokens for the specified <paramref name="AppUser" /> and sets the refresh token as an HttpOnly cookie in the <paramref name="HttpContext" /> response.
            </summary>
            <param name="AppUser">The application user for whom to generate tokens.</param>
            <param name="HttpContext">The HTTP context.</param>
            <returns>A task that represents the asynchronous operation. The task result contains the generated tokens.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.ITokenService.GetValidRefreshToken(System.String,ScriptAtRestServer.IdentityAuth.ApplicationUser)">
            <summary>
            Retrieves a valid refresh token based on the specified <paramref name="Token" /> and <paramref name="User" />.
            </summary>
            <param name="Token">The refresh token to validate.</param>
            <param name="User">The associated user.</param>
            <returns>A task that represents the asynchronous operation. The task result contains the valid refresh token.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.ITokenService.RevokeRefreshTokenAsync(ScriptAtRest.Server.Database.Entities.RefreshTokenEntity,ScriptAtRestServer.IdentityAuth.ApplicationUser,Microsoft.AspNetCore.Http.HttpContext)">
            <summary>
            Revokes the specified <paramref name="RefreshToken" /> for the given <paramref name="User" /> and logs the revocation IP.
            </summary>
            <param name="RefreshToken">The refresh token to revoke.</param>
            <param name="User">The associated user.</param>
            <param name="HttpContext">The HTTP context.</param>
            <returns>A task that represents the asynchronous operation. The task result indicates whether the revocation was successful.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.ITokenService.RevokeAllRefreshTokensAsync(ScriptAtRestServer.IdentityAuth.ApplicationUser,Microsoft.AspNetCore.Http.HttpContext)">
            <summary>
            Revokes all refresh tokens associated with the specified <paramref name="User" /> and logs the revocation IP.
            </summary>
            <param name="User">The user for whom to revoke refresh tokens.</param>
            <param name="HttpContext">The HTTP context.</param>
            <returns>A task that represents the asynchronous operation. The task result indicates whether the revocation was successful.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.ITokenService.ValidateUser(System.String,System.String)">
            <summary>
            Validates the provided <paramref name="Username" /> and <paramref name="Password" /> and returns the corresponding application user.
            </summary>
            <param name="Username">The username to validate.</param>
            <param name="Password">The password to validate.</param>
            <returns>A task that represents the asynchronous operation. The task result contains the validated application user.</returns>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.Token.TokenService._generateJwtToken(System.Collections.Generic.List{System.Security.Claims.Claim},System.DateTime)">
            <summary>
            Generates a JWT token.
            </summary>
            <param name="claims">A list of Claims to be included in the JWT token.</param>
            <param name="expires">The expiration date and time for the JWT token.</param>
            <returns>A JWT token as a string.</returns>
            <remarks>
            The JWT token is signed with a secret key and includes the provided claims.
            The issuer, audience, and notBefore values for the token are retrieved from the application's configuration.
            The method also logs the token's expiration date and time for debugging purposes.
            </remarks>
        </member>
        <member name="M:ScriptAtRest.Server.Domains.User.UserService.GetUserBynameWithRefreshToken(System.String)">
            <summary>
            Retrieves a user with the specified username and includes their refresh tokens.
            </summary>
            <param name="UserName">
            The username of the user to retrieve.
            </param>
            <returns>
            Returns a <see cref="T:ScriptAtRestServer.IdentityAuth.ApplicationUser" /> object representing the user with the specified username,
            including any associated refresh tokens.
            </returns>
            <remarks>
            This method retrieves a user with the specified username from the data context, and includes
            any associated refresh tokens in the result. The result is returned as an <see cref="T:ScriptAtRestServer.IdentityAuth.ApplicationUser" />
            object. If no user is found with the specified username, the method returns null.
            </remarks>
            <exception cref="T:System.ArgumentNullException">
            Thrown if the <paramref name="UserName" /> parameter is null or empty.
            </exception>
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.init">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.init.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.init.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.init.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scripthistorycascadedelete">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripthistorycascadedelete.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripthistorycascadedelete.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripthistorycascadedelete.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.addUserTypetoApplicationUserentity">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.addUserTypetoApplicationUserentity.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.addUserTypetoApplicationUserentity.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.addUserTypetoApplicationUserentity.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scriptentityupdates">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptentityupdates.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptentityupdates.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptentityupdates.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scriptcapabilityremoval">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptcapabilityremoval.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptcapabilityremoval.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptcapabilityremoval.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scripjobenhancements">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobenhancements.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobenhancements.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobenhancements.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scriptjobrequestdata">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptjobrequestdata.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptjobrequestdata.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scriptjobrequestdata.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
        <member name="T:ScriptAtRest.Server.Migrations.scripjobretrycount">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobretrycount.Up(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobretrycount.Down(Microsoft.EntityFrameworkCore.Migrations.MigrationBuilder)">
            <inheritdoc />
        </member>
        <member name="M:ScriptAtRest.Server.Migrations.scripjobretrycount.BuildTargetModel(Microsoft.EntityFrameworkCore.ModelBuilder)">
            <inheritdoc />
        </member>
    </members>
</doc>