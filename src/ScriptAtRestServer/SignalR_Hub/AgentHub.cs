﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using ScriptAtRest.Server.AgentHub;
using ScriptAtRest.Server.Database;
using ScriptAtRest.Server.Domains.Agent;
using ScriptAtRest.Server.Domains.AgentSrv;
using ScriptAtRest.Shared.Scripts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ScriptAtRestServer.SignalRHub
{
    [Authorize]
    public class AgentHub : Hub
    {
        private readonly ILogger<AgentHub> _logger;
        private readonly IAgentService _agentService;
        private readonly DataContext _dataContext;
        private readonly AgentSynchronizationService _agentSynchronizationService;

        public AgentHub(ILogger<AgentHub> logger, IAgentService agentService, DataContext dataContext, AgentSynchronizationService agentSynchronizationService)
        {
            _logger = logger;
            _agentService = agentService;
            _dataContext = dataContext;
            _agentSynchronizationService = agentSynchronizationService;
        }

        public override async Task<Task> OnConnectedAsync()
        {
            _logger.LogInformation("Agent connecting");
            string agentIdentity = this.Context.User.Identity.Name;
            SemaphoreSlim semaphore = null;
            if (agentIdentity is not null)
            {
                semaphore = _agentSynchronizationService.AgentSemaphores.GetOrAdd(agentIdentity, new SemaphoreSlim(1, 1));
                await semaphore.WaitAsync();
            }
            try
            {
                var existingAgentRecord = await _agentService.GetAgentByIdentityName(agentIdentity);

                if (existingAgentRecord.IsFailure)
                {
                    _logger.LogWarning("Agent with unknown identity is attempting to connect with hub. Aborting connectionid: {id}", this.Context.ConnectionId);
                    this.Context.Abort();
                    _logger.LogWarning("Connection aborted");
                    return base.OnConnectedAsync();
                };

                await _agentService.UpdateAgentOnConnect(existingAgentRecord.Value.Id, this.Context.ConnectionId);

                _logger.LogInformation("Agent connected: {@AgentDetails}", (
                    AgentConnectionId: this.Context.ConnectionId,
                    AgentIdentity: agentIdentity,
                    AgentName: existingAgentRecord.Value.UserDefinedName
                ));
            }
            finally
            {
                semaphore.Release();
            }
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            _logger.LogInformation("Agent disconnecting");
            string agentIdentity = this.Context.User.Identity.Name;
            SemaphoreSlim semaphore;
            if (agentIdentity is not null)
            {
                semaphore = _agentSynchronizationService.AgentSemaphores.GetOrAdd(agentIdentity, new SemaphoreSlim(1, 1));
                await semaphore.WaitAsync();
                var existingAgentRecord = await _agentService.GetAgentByIdentityName(agentIdentity);
                if (!existingAgentRecord.IsSuccess)
                {
                    _logger.LogWarning("Agent with unrecognized identity disconnected from hub. Connectionid: {id} Identity: {identityName}", this.Context.ConnectionId, agentIdentity);
                }
                else
                {
                    try
                    {
                        await _agentService.UpdateAgentOnDisconnect(existingAgentRecord.Value.Id);
                    }
                    finally
                    {
                        _ = _agentSynchronizationService.AgentSemaphores.TryRemove(agentIdentity, out _);
                        _logger.LogInformation("Agent disconnected: {@AgentDetails}", (
                            AgentConnectionId: this.Context.ConnectionId,
                            AgentIdentity: agentIdentity
                        ));
                        semaphore?.Release();
                    }
                }
            }
            else
            {
                _logger.LogWarning("Agent with unknown identity disconnected from hub. Connectionid: {id}", this.Context.ConnectionId);
            }
        }

        public async Task ScriptRunAck(string ServerScriptJobId)
        {
            var ScriptJobId = Guid.Parse(ServerScriptJobId);
            _logger.LogInformation("Received scriptjob update: [{state}] for job: {job}", "ACK", ScriptJobId);
            var scriptjob = await _dataContext.ScriptJobs.FindAsync(ScriptJobId);
            if (scriptjob != null)
            {
                //_logger.LogDebug("ScriptJob Found: {@job}", scriptjob);
                scriptjob.JobStatus = ScriptJobStates.Acknowledged;
                await _dataContext.SaveChangesAsync();
            }
            else
            {
                _logger.LogWarning("Script job not found in DB !");
            }
        }

        public async Task ScriptRunUpdateState(string ServerScriptJobid, string ScriptJobState)
        {
            var ScriptJobId = Guid.Parse(ServerScriptJobid);
            _logger.LogInformation("Received scriptjob update: [{state}] for job: {jobid}", ScriptJobState, ScriptJobId);
            var scriptjob = await _dataContext.ScriptJobs.FindAsync(ScriptJobId);
            if (scriptjob != null)
            {
                _logger.LogDebug("ScriptJob Found: {@jobh}", scriptjob);
                scriptjob.JobStatus = ScriptJobState;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task ScriptRunFatalFailure(string ServerScriptJobId, string ExceptionMessage)
        {
            var ScriptJobId = Guid.Parse(ServerScriptJobId);
            _logger.LogWarning("Scriptjob: {jobid} failed when running on remote agent: {agent}", ScriptJobId, this.Context.User.Identity.Name);
            var scriptjob = await _dataContext.ScriptJobs.FindAsync(ScriptJobId);
            if (scriptjob != null)
            {
                _logger.LogDebug("ScriptJob Found: {@jobh}", scriptjob);
                scriptjob.JobStatus = ScriptJobStates.Failed;
                scriptjob.FailureDetails = ExceptionMessage;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task ScriptRunFinished(ScriptRunResultData scriptRunResultData)
        {
            _logger.LogInformation("Received scriptjob update: [{state}] for job: {jobid}", "COMPLETE", scriptRunResultData.ServerScriptJobId);
            var scriptjob = await _dataContext.ScriptJobs.FindAsync(scriptRunResultData.ServerScriptJobId);
            if (scriptjob != null)
            {
                _logger.LogDebug("ScriptJob Found: {@jobh}", scriptjob);
                scriptjob.JobStatus = ScriptJobStates.Completed;
                scriptjob.EncodedJobStandardOutput = scriptRunResultData.EncodedStandardOutput;
                scriptjob.EncodedJobErrorOutput = scriptRunResultData.EncodedErrorOutput;
                scriptjob.JobExitCode = scriptRunResultData.ScriptExitCode;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task<NewTokensModel> RenewTokens(string RefreshToken)
        {
            var agent = await _agentService.GetAgentByIdentityName(this.Context.User.Identity.Name);
            if (!agent.IsSuccess)
            {
                return null;
            }
            _logger.LogDebug("Agent: {id} requested refresh of tokens", agent.Value.Id);
            var newTokens = await _agentService.RenewTokensRequestedByAgent(RefreshToken, agent.Value.Id);
            if (!newTokens.IsSuccess)
            {
                return null;
            }
            return newTokens.Value;
        }

        public async Task SuspendAgent(string Reason)
        {
            var agentName = this.Context.User.Identity.Name;
            _logger.LogWarning("Received agent suspension request from : {AgentName}, reason: {reason}", agentName, Reason);
            var agent = await _agentService.GetAgentByIdentityName(agentName);
            await _agentService.UpdateAgentState(agent.Value.Id, AgentConnectionState.Suspended);
            _logger.LogWarning("Agent: {AgentName} will not process any further requests", agentName);
        }
    }
}