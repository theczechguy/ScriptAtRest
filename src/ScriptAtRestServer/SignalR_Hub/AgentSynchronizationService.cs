﻿using System.Collections.Concurrent;
using System.Threading;

namespace ScriptAtRest.Server.AgentHub
{
    public class AgentSynchronizationService
    {
        public ConcurrentDictionary<string, SemaphoreSlim> AgentSemaphores { get; } = new ConcurrentDictionary<string, SemaphoreSlim>();
    }
}