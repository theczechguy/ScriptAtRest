﻿using Microsoft.EntityFrameworkCore;
using ScriptAtRest.Server.Database;

namespace ScriptAtRestServerTests
{
    internal class DatabaseFactory
    {
        public static DataContext PrepareSqLiteInMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("DataSource=:memory")
                .Options;

            DataContext context = new DataContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            return context;
        }
    }
}