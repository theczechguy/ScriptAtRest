﻿//using ScriptAtRestServer.Entities;
//using ScriptAtRestServer.Helpers;
//using ScriptAtRestServer.Models.Scripts;
//using ScriptAtRestServer.Services;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Xunit;

//[assembly: CollectionBehavior(DisableTestParallelization = true)]

//namespace ScriptAtRestServerTests
//{
//    public class ScriptServiceTests
//    {
//        private static readonly ScriptType _newType = new ScriptType { Name = "TestType", Runner = "pwsh.exe", FileExtension = "ps.1", ScriptArgument = "-f" };
//        private static readonly RegisterScriptModel _newScript = new RegisterScriptModel { Name = "TestScript", Type = 1, EncodedContent = "dGVzdA==", Timeout = 10 };

//        //Script type tests
//        [Fact]
//        public async Task ScriptService_CreateTypeAsync()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT
//            ScriptType registeredType = await service.CreateTypeAsync(_newType);

//            //ASSERT
//            Assert.Equal(1, registeredType.Id);
//            Assert.Equal(_newType.Name, registeredType.Name);
//            Assert.Equal(_newType.Runner, registeredType.Runner);
//            Assert.Equal(_newType.ScriptArgument, registeredType.ScriptArgument);
//            Assert.Equal(_newType.FileExtension, registeredType.FileExtension);
//        }

//        [Fact]
//        public async Task ScriptService_CreateTypeAsync_throw_name_taken()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            await service.CreateTypeAsync(_newType);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.CreateTypeAsync(_newType));
//            Assert.Equal("Script type name is already taken", exception.Message);
//        }

//        [Fact]
//        public async Task ScriptService_DeleteTypeAsync_return_true()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType registeredType = await service.CreateTypeAsync(_newType);

//            //ACT
//            bool result = await service.DeleteTypeAsync(registeredType.Id);

//            //ASSERT
//            Assert.True(result);
//        }

//        [Fact]
//        public async Task ScriptService_DeleteTypeAsync_throw_id_not_found()
//        {
//            //ARRANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.DeleteTypeAsync(1));
//            Assert.Equal("Script type with requested id not found", exception.Message);
//        }

//        [Fact]
//        public async Task ScriptService_GetAllTypes_return_two_scripttypes()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            ScriptType type1 = new ScriptType { Name = "TestType1", Runner = "pwsh.exe", FileExtension = "ps.1", ScriptArgument = "-f" };
//            ScriptType type2 = new ScriptType { Name = "TestType2", Runner = "cmd.exe", FileExtension = "cmd", ScriptArgument = "/c" };

//            await service.CreateTypeAsync(type1);
//            await service.CreateTypeAsync(type2);

//            //ACT
//            var allTypes = service.GetAllTypes();

//            //ASSERT
//            Assert.Equal(2, allTypes.Count());
//            Assert.Contains(type1, allTypes);
//            Assert.Contains(type2, allTypes);
//        }

//        [Fact]
//        public async Task ScriptService_GetTypeByIdAsync_return_scripttype()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType registeredType = await service.CreateTypeAsync(_newType);

//            //ACT
//            ScriptType resultGetType = await service.GetTypeByIdAsync(registeredType.Id);

//            //ASSERT
//            Assert.Equal(registeredType, resultGetType);
//        }

//        [Theory]
//        [InlineData(1)]
//        [InlineData(2)]
//        [InlineData(3)]
//        [InlineData(-9)]
//        [InlineData(9876)]
//        public async Task ScriptService_GetTypeByIdAsync_throw_id_not_found(int TypeId)
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.GetTypeByIdAsync(TypeId));
//            Assert.Equal("Script type with specified id not found", exception.Message);
//        }

//        public static IEnumerable<object[]> ScriptTypeUpdateData => new List<object[]>
//            {
//                new object[] {
//                    new ScriptType {FileExtension = "test1" , Name = "test1" , Runner = "test1" ,ScriptArgument = "test1" },
//                    new ScriptType {FileExtension = "test1" , Name = "test1" , Runner = "test1" ,ScriptArgument = "test1"  }
//                },
//               new object[] {
//                    new ScriptType {FileExtension = "test2" , Name = "test2" , Runner = "test2" ,ScriptArgument = "test2" },
//                    new ScriptType {FileExtension = "test2" , Name = "test2" , Runner = "test2" ,ScriptArgument = "test2"  }
//                }
//            };

//        [Theory]
//        [MemberData(nameof(ScriptTypeUpdateData))]
//        public async Task ScriptService_UpdateTypeById_return_updated_type(ScriptType UpdateTo, ScriptType Expected)
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType registeredType = await service.CreateTypeAsync(_newType);

//            //ACT
//            ScriptType updateResult = await service.UpdateTypeById(registeredType.Id, UpdateTo);

//            //ASSERT
//            Assert.Equal(Expected.FileExtension, updateResult.FileExtension);
//            Assert.Equal(Expected.Name, updateResult.Name);
//            Assert.Equal(Expected.Runner, updateResult.Runner);
//            Assert.Equal(Expected.ScriptArgument, updateResult.ScriptArgument);
//        }

//        // Script tests
//        public static IEnumerable<object[]> ScriptUpdateData => new List<object[]>
//        {
//            new object[] {
//                new UpdateScriptModel { Name = "new name" },
//                new Script { Name = "new name", EncodedContent = "dGVzdA==", Timeout = 10 },
//            },
//            new object[] {
//                new UpdateScriptModel { Timeout = 666 },
//                new Script { Name = "TestScript", EncodedContent = "dGVzdA==", Timeout = 666 }
//            },
//            new object[] {
//                new UpdateScriptModel { Type = 2 },
//                new Script { Name = "TestScript", ScriptType = _newType, EncodedContent = "dGVzdA==", Timeout = 666 }
//            },
//            new object[] {
//                new UpdateScriptModel { EncodedContent = "ZGlmZmVyZW50IGNvbnRlbnQ=" },
//                new Script { Name = "TestScript", EncodedContent = "different content", Timeout = 666 }
//            },
//            new object[] {
//                new UpdateScriptModel { Name = "TestScript1", EncodedContent = "dGVzdDEyMw==", Timeout = 10  },
//                new Script { Name = "TestScript1", EncodedContent = "dGVzdDEyMw==", Timeout = 10 }
//            }
//        };

//        [Theory]
//        [MemberData(nameof(ScriptUpdateData))]
//        public async Task ScriptService_UpdateScriptAsync_return_updated_script(UpdateScriptModel UpdateTo, Script Expected)
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            await service.CreateTypeAsync(_newType);
//            await service.CreateTypeAsync(new ScriptType { Name = "TestType2", Runner = "pwsh.exe", FileExtension = "ps.1", ScriptArgument = "-f" });

//            Script registeredScript = await service.CreateAsync(_newScript);

//            //ACT
//            Script updateResult = await service.UpdateScriptByIdAsync(registeredScript.Id, UpdateTo);

//            //ASSERT
//            //Assert.Equal(Expected.Content, updateResult.Content);
//            Assert.Equal(Expected.Name, updateResult.Name);
//            //Assert.Equal(Expected.Timeout , updateResult.Timeout);
//            //Assert.Equal(Expected.Type , updateResult.ScriptType.Id);
//        }

//        [Fact]
//        public async Task ScriptService_CreateAsync_return_new_script()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType scriptType = await service.CreateTypeAsync(new ScriptType { Name = "test", Runner = "test", ScriptArgument = "test", FileExtension = "test" });

//            //ACT
//            Script newScriptResult = await service.CreateAsync(_newScript);

//            //ASSERT
//            Assert.NotNull(newScriptResult);
//            Assert.Equal(1, newScriptResult.Id);
//            Assert.Equal(_newScript.Name, newScriptResult.Name);
//            Assert.Equal(_newScript.Type, newScriptResult.ScriptType.Id);
//            Assert.Equal(_newScript.EncodedContent, newScriptResult.EncodedContent);
//        }

//        [Fact]
//        public async Task ScriptService_CreateAsync_throw_name_taken()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType scriptType = await service.CreateTypeAsync(new ScriptType { Name = "test", Runner = "test", ScriptArgument = "test", FileExtension = "test" });
//            await service.CreateAsync(_newScript);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.CreateAsync(_newScript));
//            Assert.Equal("Scriptname is already taken", exception.Message);
//        }

//        [Fact]
//        public async Task ScriptService_CreateAsync_throw_type_not_registered()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.CreateAsync(_newScript));
//            Assert.Equal("Script type is not registered", exception.Message);
//        }

//        [Fact]
//        public async Task ScriptService_DeleteAsync_return_true()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            ScriptType scriptType = await service.CreateTypeAsync(new ScriptType { Name = "test", Runner = "test", ScriptArgument = "test", FileExtension = "test" });
//            Script registeredScript = await service.CreateAsync(_newScript);

//            //ACT
//            bool result = await service.DeleteTypeAsync(registeredScript.Id);

//            //ASSERT
//            Assert.True(result);
//        }

//        [Fact]
//        public async Task ScriptService_DeleteAsync_throw_id_not_found()
//        {
//            //ARRANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.DeleteAsync(1));
//            Assert.Equal("Script with requested id not found", exception.Message);
//        }

//        [Fact]
//        public async Task ScriptService_GetAll_return_two_scripts()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            await service.CreateTypeAsync(new ScriptType { Name = "test", Runner = "test", ScriptArgument = "test", FileExtension = "test" });

//            RegisterScriptModel script1 = new RegisterScriptModel { Name = "TestScript1", Type = 1, EncodedContent = "dGVzdDE=", Timeout = 10 };
//            RegisterScriptModel script2 = new RegisterScriptModel { Name = "TestScript2", Type = 1, EncodedContent = "dGVzdDI=", Timeout = 10 };
//            await service.CreateAsync(script1);
//            await service.CreateAsync(script2);

//            //ACT
//            var allScripts = service.GetAll();

//            //ASSERT
//            Assert.Equal(2, allScripts.Count());
//        }

//        [Fact]
//        public async Task ScriptService_GetScriptByNameAsync_return_script()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            await service.CreateTypeAsync(_newType);
//            Script registeredScript = await service.CreateAsync(_newScript);

//            //ACT
//            Script resultGetScript = await service.GetScriptByNameAsync(registeredScript.Name);

//            //ASSERT
//            Assert.Equal(registeredScript, resultGetScript);
//        }

//        [Theory]
//        [InlineData("I dont exist")]
//        [InlineData("I also dont exist")]
//        [InlineData("Strange, neither do I")]
//        public async Task ScriptService_GetScriptByNameAsync_throw_name_not_found(string ScriptName)
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.GetScriptByNameAsync(ScriptName));
//        }

//        [Fact]
//        public async Task ScriptService_GetByIdAsync_return_script()
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);
//            await service.CreateTypeAsync(_newType);
//            Script registeredScript = await service.CreateAsync(_newScript);

//            //ACT
//            Script resultGetScript = await service.GetByIdAsync(registeredScript.Id);

//            //ASSERT
//            Assert.Equal(registeredScript, resultGetScript);
//        }

//        [Theory]
//        [InlineData(1)]
//        [InlineData(2)]
//        [InlineData(3)]
//        [InlineData(-9)]
//        [InlineData(9876)]
//        public async Task ScriptService_GetByIdAsync_throw_id_not_found(int ScriptId)
//        {
//            //ARANGE
//            DataContext context = DatabaseFactory.PrepareSqLiteInMemoryDatabase();
//            var service = new ScriptService(context);

//            //ACT & ASSERT
//            AppException exception = await Assert.ThrowsAsync<AppException>(() => service.GetByIdAsync(ScriptId));
//            Assert.Equal($"Script with id {ScriptId} not found !", exception.Message);
//        }
//    }
//}